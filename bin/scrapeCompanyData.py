from lxml import html
import requests
import sys
ric = sys.argv[1]
page = requests.get('https://au.finance.yahoo.com/q/ks?s=' + ric)
tree = html.fromstring(page.content)

title = tree.xpath('//h2/text()')[-1]
names = tree.xpath('//td[@class="yfnc_tablehead1"]/text()')
values = tree.xpath('//span[@id="yfs_j10_' + ric.lower() + '"]/text()')
values += tree.xpath('//td[@class="yfnc_tabledata1"]/text()')
names[:] = [n for n in names if not (n == ":")]
print('{')
print('"Name": "' + title + '",')
for n, v in zip(names, values):
    print('"' + n + '": "' + v + '",')
print('}')

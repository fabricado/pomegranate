<h3 class='releaseNotesTitle'>Event Study Version 1.0</h3>
<p>Date - 6 April 2016</p>
<div>
    <ul>
        <li>Completely implemented event-study spec v1.5</li>
	<li>Known Bugs:
	<ul>
		<li>Windows build will not print elapsed time in logfile</li>
	</ul>
	</li>
    </ul>
</div>

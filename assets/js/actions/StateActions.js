/**
 * StateDispatcher is used to dispatch an action to the StateStore
 *
 * PomegranateConstants are predefined constants for the pomegranate app
 */
var StateDispatcher = require('../dispatchers/StateDispatcher');
var DataDispatcher = require('../dispatchers/DataDispatcher');
var PomegranateConstants = require('../constants/PomegranateConstants');
var ServerInterface = require('../modules/ServerInterface');
var StateStore = require('../stores/StateStore');
var $ = require('jquery');
var assign = require('object-assign');
var _ = require('lodash');

/**
 * Defines actions that can be created by react components
 * Each action causes a change in the store
 */
var StateActions = {

    /**
     * This action should be called on startup
     */
    initialise: function () {
        ServerInterface.getLoginStatus().then(function onResolve (result) {
            if (result.isLoggedIn) {
                StateDispatcher.dispatch({
                    actionType: PomegranateConstants.STATE_LOGIN_SUCCESS,
                    username: result.username
                });
            }
        }, function onReject (err) {
            console.log(err);
        });
        /*
        var token = ServerInterface.getActiveSessionFromServer().then(function onResolve(result) {
            //Promise succeeded
            //only set token/get state if we actually had a valid active session
            if (result.token) {
                StateActions.setStateToken(result.token);
                StateActions.getStateFromServer(result.token);
            } else {
                //no active session, so make a new one
                ServerInterface.getTokenFromServer()
                    .then(function onResolve(result) {
                        //new session ok
                        StateActions.setStateToken(result.token);
                    }, function onReject(err) {
                        //new session not ok
                        console.log("Could not make new session");
                    });
            }
            console.log("Token get: ", result.token);
        }, function onReject(error) {
            //error getting active session
            console.log(error);
        });
        */
    },

    /**
     * This action should be called when the Filter Variable Form is unmounted
     */
    setEventFilterState: function (eventFilterState) {
        StateDispatcher.dispatch({
            actionType: PomegranateConstants.STATE_SET_EVENT_FILTER,
            newState: eventFilterState
        });
    },

    /**
     * This action should be called when the Event Study Graph is unmounted
     */
    setEventStudyGraphState: function (eventStudyGraphState) {
        StateDispatcher.dispatch({
            actionType: PomegranateConstants.STATE_SET_EVENT_STUDY_GRAPH,
            newState: eventStudyGraphState
        });
    },

    /**
     * This action should be called whenever the states are updated
     */
    sendStateToServer: function() {
        /*
        var _state = {
            EventFilterState: StateStore.getEventFilterState(),
            EventStudyGraphState: StateStore.getEventStudyGraphState()
        };
        var _token = StateStore.getToken();

        //stringify state now to prevent numbers being turned into strings
        var data = {state: JSON.stringify(_state), token: _token};

        ServerInterface.sendStateToServer(data).then(function onResolve(result) {
            //Promise succeeded
            console.log("State Uploaded!");
        }, function onReject(error) {
            //error getting token
            console.log(error);
        });
        */
    },

    loginSuccess: function (name) {
        StateDispatcher.dispatch({
            actionType: PomegranateConstants.STATE_LOGIN_SUCCESS,
            username: name
        });
    },

    logoutSuccess: function () {
        StateDispatcher.dispatch({
            actionType: PomegranateConstants.STATE_LOGOUT_SUCCESS,
        });
        DataDispatcher.dispatch({
            actionType: PomegranateConstants.STATE_LOGOUT_SUCCESS
        });
    },

    /**
     * This action should be called when the session is changed, or on refresh
     */
    getStateFromServer: function (token) {
        /*
        console.log("getStateFromServer");
        StateDispatcher.dispatch(assign({actionType: PomegranateConstants.STATE_SET_TOKEN, newToken: token}));

        ServerInterface.getStateFromServer(token).then(function onResolve(result) {
            //Promise succeeded
            console.log(result);
            if (!result.error) {
                console.log("All good in the hood");
                StateDispatcher.dispatch(assign({actionType: PomegranateConstants.STATE_SET_ALL, states: result}));
            } else {
                console.log("Error: ", result.error);
            }
        }, function onReject(error) {
            //error getting state from server
            console.log(error);
        });
        */
    },

    /**
     * This action should be called when new files are uploaded
     */
    purge: function() {
        StateDispatcher.dispatch({
            actionType: PomegranateConstants.STATE_PURGE
        });
    }
};

module.exports = StateActions;

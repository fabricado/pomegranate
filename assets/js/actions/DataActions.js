/**
 * DataDispatcher is used to dispatch an action to the DataStore
 *
 * PomegranateConstants are predefined constants for the pomegranate app
 */
var DataDispatcher = require('../dispatchers/DataDispatcher');
var PomegranateConstants = require('../constants/PomegranateConstants');
var ServerInterface = require('../modules/ServerInterface');
var DataStore = require('../stores/DataStore');
var $ = require('jquery');
var assign = require('object-assign');
var _ = require('lodash');
var moment = require('moment');

/**
 * Defines actions that can be created by react components
 * Each action causes a change in the store
 */
var DataActions = {
    // **************************************************************
    // *
    // * ACTIONS RELATING TO FILES
    // **************************************************************

    /**
     * This action should be dispatched when a successful file upload is done to the server
     */
    onFileOK: function () {
        ServerInterface.getFileAnalyses().then(function onResolve(result) {
            //Promise succeeded
            console.log("Ship");
            console.log(result);
            console.log("Shape");
            DataDispatcher.dispatch(assign({actionType: PomegranateConstants.FILE_OK}, result));
        }, function onReject(error) {
            //Promise failed
            DataDispatcher.dispatch(assign({actionType: PomegranateConstants.SERVER_FILE_DATA_ERROR}, error));
            console.log(error);
        });
    },

    /**
     * This action should be called when the app has just loaded, and wants the store to update itself
     */
    initialise: function () {
        ServerInterface.getFileAnalyses().then(function onResolve(result) {
            //Promise succeeded
            DataDispatcher.dispatch(assign({actionType: PomegranateConstants.STORE_INITIALISE}, result));
        }, function onReject(error) {
            //Promise failed
            DataDispatcher.dispatch(assign({actionType: PomegranateConstants.SERVER_FILE_DATA_ERROR}, {error: error}));
            console.log(error);
        });
        ServerInterface.getServerRICList().then(function onResolve(result) {
            //Promise succeeded
            console.log("Server RICS good");
            DataDispatcher.dispatch({actionType: PomegranateConstants.GOT_SERVER_RICS, RICs: result});
        }, function onReject(error) {
            //Promise failed
            console.log(error);
        });
    },

    /**
     * This action should be dispatched when a file upload to the server is unsuccessful
     */
    onFileError: function () {
        DataDispatcher.dispatch({
            actionType: PomegranateConstants.FILE_ERROR
        });
    },


    // **************************************************************
    // *
    // * ACTIONS RELATING TO DATA
    // **************************************************************

    /**
     * This action should be dispatched when the store needs to update its events variable
     */
    getEvents: function () {
        ServerInterface.getAllEvents().then(function onResolve(result) {
            DataDispatcher.dispatch({
                actionType: PomegranateConstants.DATA_GET_EVENTS_OK,
                data: result
            });
        }, function onReject(error) {
            DataDispatcher.dispatch({
                actionType: PomegranateConstants.DATA_GET_EVENTS_ERROR,
                error: error
            });
        });
    },

    /**
     * This action should be dispatched when the store needs to update its lifetime stock info
     * @param rics A list of RICs to get data for
     */
    getLifetimeData: function (rics) {
        var keysInStore = _.keys(DataStore.getAllLifetimeData());
        var ricsToProcess = _.difference(rics,keysInStore);
        ServerInterface.getStockLifetimeData(ricsToProcess).then(function onResolve(result) {
            DataDispatcher.dispatch({
                actionType: PomegranateConstants.DATA_GET_RIC_LIFETIME_OK,
                data: result
            });
        }, function onReject(error) {
            DataDispatcher.dispatch({
                actionType: PomegranateConstants.DATA_GET_RIC_LIFETIME_ERROR,
                error: error
            });
        });
    },

    /**
     * This action should be called when the store is required to update its cumulative return values
     * with respect to the given input
     * @param windowBounds: @type {lowerBound: Integer, upperBound: Integer}
     * @param variableFilters @type {VariableName: {lowerWindow: Double, upperWindow: Double}}
     * @param mode
     * @param rel
     */
    getCumulativeReturnData: function (windowBounds, variableFilters,mode = "",rel) {
        ServerInterface.getCumulativeReturns(windowBounds, variableFilters,mode, rel).then(function onResolve(result) {
            DataDispatcher.dispatch({
                actionType: PomegranateConstants.DATA_GET_CUMULATIVE_RETURNS_OK,
                request: result.request,
                data: result.results
            });
        }, function onReject(error) {
            DataDispatcher.dispatch({
                actionType: PomegranateConstants.DATA_GET_CUMULATIVE_RETURNS_ERROR,
                error: error
            });
        });
    },

    /**
     * @param windowBounds: @type {lowerBound: Integer, upperBound: Integer}
     */
    getSingleEventCumulativeReturnData: function (windowBounds,mode, rel) {
        ServerInterface.getSingleEventCumulativeReturns(windowBounds,mode, rel).then(function onResolve(result) {
            DataDispatcher.dispatch({
                actionType: PomegranateConstants.DATA_GET_SINGLE_EVENT_CUMULATIVE_RETURNS_OK,
                request: result.request,
                data: result.results
            });
        }, function onReject (err) {
            DataDispatcher.dispatch({
                actionType: PomegranateConstants.DATA_GET_SINGLE_EVENT_CUMULATIVE_RETURNS_ERROR,
                error:err
            });
        });
    },

    getNews: function (rics) {
        ServerInterface.getNewsFromServer(rics).then(function onResolve(result) {
            DataDispatcher.dispatch({
                actionType: PomegranateConstants.DATA_NEWS_GET_OK,
                news: JSON.parse(result)
            });
        }, function onReject (err) {
            console.log("news bad");
        });
    },

    getPortfolioStocks: function () {
        ServerInterface.getPortfolioStocks().then(function onResolve(result) {
            DataDispatcher.dispatch({
                actionType: PomegranateConstants.DATA_GET_PORTFOLIO_STOCKS_OK,
                data: result
            });
        }, function onReject (err) {
            console.log(err);
        })
    },

    addNewEvent: function (newEvent) {
        ServerInterface.postNewEvent(newEvent).then(function onResolve(result) {
            DataDispatcher.dispatch({
                actionType: PomegranateConstants.ON_FILE_OK
            });
            ServerInterface.getFileAnalyses().then(function onResolve(result) {
                //Promise succeeded
                DataDispatcher.dispatch(assign({actionType: PomegranateConstants.FILE_OK}, result));
            }, function onReject(error) {
                DataDispatcher.dispatch(assign({actionType: PomegranateConstants.SERVER_FILE_DATA_ERROR}, error));
                console.log(error);
            });
        }, function onReject (err) {
            console.log(err);
        });
    },

    onLogout: function () {
        DataDispatcher.dispatch({actionType: PomegranateConstants.STATE_LOGOUT_SUCCESS});
    },

    getExchangeRateData: function (currency) {
        ServerInterface.getExchangeRateData(currency).then(function onResolve(result) {
            result["FXR" + currency].startDate = moment(result["FXR" + currency].startDate, "DD-MMM-YYYY"); //convert date from string to moment
            result["FXR" + currency].period = "monthly";
            result["FXR" + currency].jump = 1;
            DataDispatcher.dispatch({
                actionType: PomegranateConstants.DATA_GET_EXTERNAL_FACTOR,
                data: result
            });
        }, function onReject (err) {
            console.log("ex rate bad");
        });
    },

    getInterestRateData: function (country) {
        ServerInterface.getInterestRateData(country).then(function onResolve(result) {
            result[country].startDate = moment(result[country].startDate, "DD-MMM-YYYY"); //convert date from string to moment
            DataDispatcher.dispatch({
                actionType: PomegranateConstants.DATA_GET_EXTERNAL_FACTOR,
                data: result
            });
        }, function onReject (err) {
            console.log("interest rate bad");
        });
    },

    getCompanyProfile: function (RIC) {
        ServerInterface.getCompanyProfile(RIC).then(function onResolve(result) {
            DataDispatcher.dispatch({
                actionType: PomegranateConstants.GOT_COMPANY_PROFILE,
                newProfile: result
            });
            ServerInterface.getServerStockLifetime(RIC).then(function onResolve(result) {
                console.log("FLAG FLAG FLAG", result);
                DataDispatcher.dispatch({
                    actionType: PomegranateConstants.GOT_COMPANY_LIFETIME,
                    lifetime: result
                });
            }, function onReject (err) {
                console.log(err);
            });
        }, function onReject (err) {
            console.log(err);
        });
    },

    getUserStockLifetime: function (RIC) {
        ServerInterface.getUserStockLifetime(RIC).then(function onResolve(result) {
            DataDispatcher.dispatch({
                actionType: PomegranateConstants.GOT_USER_STOCK_LIFETIME,
                lifetime: result
            });
        }, function onReject (err) {
            console.log(err);
        });
    },

    addStockToPortfolio: function (stock) {
        ServerInterface.addStockToPortfolio(stock).then(function onResolve(result) {
            console.log("resolving");
            DataDispatcher.dispatch({
                actionType: PomegranateConstants.ADD_STOCK_TO_PORTFOLIO,
                newStock: stock
            });
        }, function onReject (err) {
            console.log(err);
        });
    }
};

module.exports = DataActions;

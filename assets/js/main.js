/**
 * This is the main entry point into the application
 */

var $ = require('jquery');
var React = require('react');
var ReactDOM = require('react-dom');

$(document).ready(function () {
    /**
     * We check which page we are on and run only the necessary modules
     */
    if (typeof ON_HOMEPAGE !== 'undefined') {
        var HomePage = require('./modules/HomePage.js');
        HomePage.init();
    } else if (typeof ON_DOCUMENTATION !== 'undefined') {
        //do nothing
    } else if (typeof ON_RELEASES !== 'undefined') {
        var Releases = require('./modules/Releases');
        Releases.init();
    } else if (typeof ON_POMEGRANATE_HOMEPAGE !== 'undefined') {
        var PomegranateApp = require('./components/PomegranateApp');
        ReactDOM.render(<PomegranateApp />, document.getElementById('react-pomegranateApp-container'));
    } else if (typeof ON_CONTACTS !== 'undefined') {
        var ContactPage = require('./modules/ContactPage');
        ContactPage.init();
    }
});

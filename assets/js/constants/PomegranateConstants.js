var keyMirror = require('keymirror');

/**
 * Defines constants for the pomegranate app
 */
module.exports = keyMirror({
    // feature tabs
    TAB_LIFETIME: null,
    TAB_EVENT_STUDY: null,
    TAB_UPLOAD: null,
    TAB_SINGLE_EVENT_STUDY: null,
    TAB_MULTI_EVENT_STUDY: null,
    TAB_NEWS: null,
    TAB_ADD_EVENTS: null,
    TAB_PORTFOLIO: null,
    TAB_FACTOR_STUDY: null,
    TAB_STOCK_STUDY: null,
    TAB_ANALYSIS: null,

    // actions relating to file operations
    FILE_OK: null,
    FILE_ERROR: null,
    SERVER_FILE_DATA_ERROR: null,

    // actions relating to data
    DATA_GET_CUMULATIVE_RETURNS_OK: null,
    DATA_GET_CUMULATIVE_RETURNS_ERROR: null,
    DATA_GET_SINGLE_EVENT_CUMULATIVE_RETURNS_OK: null,
    DATA_GET_SINGLE_EVENT_CUMULATIVEXTERNAL_DATA_CHANGEE_RETURNS_ERROR: null,
    DATA_GET_EVENTS_OK: null,
    DATA_GET_EVENTS_ERROR: null,
    DATA_GET_RIC_LIFETIME_OK: null,
    DATA_GET_RIC_LIFETIME_ERROR: null,
    DATA_NEWS_GET_OK: null,
    DATA_GET_PORTFOLIO_STOCKS_OK: null,
    DATA_GET_EXTERNAL_FACTOR: null,
    DATA_GET_RANDOM_STOCKS_OK: null,
    GOT_SERVER_RICS: null,
    GOT_COMPANY_PROFILE: null,
    GOT_COMPANY_LIFETIME: null,
    GOT_USER_STOCK_LIFETIME: null,
    ADD_STOCK_TO_PORTFOLIO: null,

    // actions relating to state
    STATE_SET_EVENT_FILTER: null,
    STATE_SET_EVENT_STUDY_GRAPH: null,
    STATE_LOGIN_SUCCESS: null,
    STATE_LOGOUT_SUCCESS: null,
    STATE_GET_FROM_SERVER: null,
    STATE_PURGE: null,

    //server lifecycle constants
    STORE_INITIALISE: null
});

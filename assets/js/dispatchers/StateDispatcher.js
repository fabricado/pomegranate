/**
 * The dispatcher is a flux object that can be used to dispatch actions
 * The dispatcher will dispatch actions to anyone who is registered to the dispatcher
 *
 * To register to the dispatcher, call .register(callback)
 * The callback will be called with the action in the parameters
 */
var Dispatcher = require('flux').Dispatcher;

/**
 * Make a new simple dispatcher
 */
var StateDispatcher = new Dispatcher();
module.exports = StateDispatcher;

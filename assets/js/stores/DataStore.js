/**
 * DataDispatcher is used to dispatch an action to the DataStore
 *
 * EventEmitter is a node object that can be used to broadcast events to registered entities
 *
 * assign is a utility that allows one to combine two or more objects. Overlapping attributes will be overridden
 * by each subsequent object
 *
 * PomegranateConstants are predefined constants for the pomegranate app
 */

var DataDispatcher = require('../dispatchers/DataDispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var PomegranateConstants = require('../constants/PomegranateConstants');
var _ = require('lodash');

/**
 * Variable to store the names of the current files in the server
 * @type {{stockFile: String, eventFile: String}}
 */
var serverFiles = {
    stockFile: null,
    eventFile: null
};

/**
 * Variable to store the current RICs in serverFiles.stockFile
 * @type {Array}
 */
var userRICs = [];
var serverRICs = [];

var companyProfile = {};

/**
 * Variable to store the range for each event variable in serverFiles.eventFile
 * @type {{VarName: {lowerBound: Number, upperBound: Number}}}
 */
var eventVariables = {};

/**
 * Variable to store all event information in serverFiles.eventFile
 * @type {[{ric: String,date: Date, variables: {VarName: Number}}]}
 */
var events = [];

/**
 * Variable to store the lifetime data of each RIC in serverFiles.stockFile
 * NOTE: The startDate is a Moment object
 * @type {{RIC: {startDate: Moment, values: []}}}
 */
var stockLifetimeData = {};

//lifetime data from server supplied stocks
var serverStockLifetimeData = {};

/**
 * Variable to store the request and result of the latest cumulative return call
 * @type {{req: {lowerBound: Number, upperBound: Number,
  * eventVariableFilters: {VarName: {lowerBound: Number, upperBound: Number}}}, data: {}}}
 */
var cumulativeReturnData = {
    req: {},
    data: {}
};

/**
 * Variable storing external factor data (eg, exchange rates, interest rates)
 */
var externalFactorData = {};

var news = [];

var portfolioStocks = [];

/**
 * A variable for a file change event
 * @type {string}
 */
var FILE_DATA_CHANGE = 'file_changed';
var CUMULATIVE_RETURN_CHANGE = 'cumulative_return_changed';
var EVENT_DATA_CHANGE = 'events_changed';
var STOCK_LIFETIME_DATA_CHANGE = 'stock_lifetime_data_changed';
var NEWS_CHANGE = 'news_changed';
var PORTFOLIO_STOCK_CHANGE = "portfolio stock changed";
var EXTERNAL_DATA_GET = "external data changed";
var COMPANY_PROFILE_CHANGE = 'company profile change';
var COMPANY_LIFETIME_GET = 'company lifetime get';
var USER_LIFETIME_GET = 'user lifetime get';

function updateServerFiles(newServerFiles) {
    serverFiles = assign(serverFiles, newServerFiles);
}

function updateUserRICs(newRICs) {
    userRICs = assign(userRICs, newRICs);
}

function updateServerRICs(newRICs) {
    serverRICs = assign(serverRICs, newRICs);
}

function updateCompanyProfile(newProfile) {
    companyProfile = newProfile;
}

function updateEventVariables(newEventVariables) {
    eventVariables = newEventVariables;
}

function updateEvents(newEvents) {
    events = newEvents;
}

function updateStockLifetimeData(newStockLifetimeData) {
    stockLifetimeData = assign(stockLifetimeData, newStockLifetimeData);
}

function updateServerStockLifetimeData(newStockLifetimeData) {
        console.log("Come and listen to this tale");
        console.log(newStockLifetimeData);
    serverStockLifetimeData = assign(serverStockLifetimeData, newStockLifetimeData);
}

function updateCumulativeReturns(req, data) {
    cumulativeReturnData.req = req;
    cumulativeReturnData.data = data;
}

function updateNews(newNews) {
    news = newNews;
}

function updatePortfolioStocks(newStocks) {
    portfolioStocks = newStocks;
}

function addPortfolioStock(newStock) {
    portfolioStocks.push(newStock);
}

function updateExternalFactorData(newData) {
    externalFactorData = newData;
}

/**
 *
 */
var DataStore = assign({}, EventEmitter.prototype, {
    /**
     * Returns the current file names in server
     * @returns {{stockFile: null, eventFile: null}}
     */
    getFiles: function () {
        return serverFiles;
    },

    /**
     * Returns the current list of RICs in the server stockFile
     * @returns {Array}
     */
    getUserRICs: function () {
        return userRICs;
    },

    /**
     * Returns the current list of RICs in the server stockFile
     * @returns {Array}
     */
    getServerRICs: function () {
        return serverRICs;
    },

    getCompanyProfile: function () {
        return companyProfile;
    },

    /**
     * Returns the current list of event variables and their range
     * @returns {{VarName: {lowerBound: Number, upperBound: Number}}}
     */
    getEventVariables: function () {
        return eventVariables;
    },

    /**
     * Returns the current list of all events in the server eventFIle
     * @returns {{RIC: String, date: Date, variables: {VarName: Number}}[]}
     */
    getEvents: function () {
        return events;
    },

    /**
     * Returns the current lifetime data of each RIC given in rics
     * @param rics
     * @returns {{RIC: {startDate: Date, values: []}}}
     */
    getStockLifetimeData: function (rics) {
        return _.pick(stockLifetimeData, rics);
    },

    getAllLifetimeData: function () {
        return stockLifetimeData;
    },

    getServerStockLifetimeData: function (RIC) {
        console.log("Gettysburg address");
        return _.pick(serverStockLifetimeData, [RIC]);
    },

    /**
     * Returns the request and result of the latest cumulative return call
     * @returns {{req: {lowerBound: Number, upperBound: Number,
      * eventVariableFilters: {VarName: {lowerBound: Number, upperBound: Number}}}, data: {}}}
     */
    getCumulativeReturnData: function () {
        return cumulativeReturnData;
    },

    getNews: function () {
        return news;
    },

    getPortfolioStocks: function () {
        return portfolioStocks;
    },

    getExternalFactorData: function () {
        return externalFactorData;
    },

    getRICs: function () {
        return portfolioStocks.map(function (s) {return s.ric});
    },

    /**
     * Sends out a file change event
     * Runs all callbacks that registered for the file change
     */
    emitFileChange: function () {
        this.emit(FILE_DATA_CHANGE);
    },

    /**
     * Allows an entity to register for a file change event
     * @param callback
     */
    registerFileChangeListener: function (callback) {
        this.on(FILE_DATA_CHANGE, callback);
    },

    /**
     * Allows an entity to un-register from a file change event
     * @param callback
     */
    removeFileChangeListener: function (callback) {
        this.removeListener(FILE_DATA_CHANGE, callback);
    },

    emitCumulativeReturnChange: function () {
        this.emit(CUMULATIVE_RETURN_CHANGE);
    },

    registerCumulativeReturnChangeListener: function (callback) {
        this.on(CUMULATIVE_RETURN_CHANGE, callback);
    },

    removeCumulativeReturnChangeListener: function (callback) {
        this.removeListener(CUMULATIVE_RETURN_CHANGE, callback);
    },

    emitEventsChange: function () {
        this.emit(EVENT_DATA_CHANGE);
    },

    registerEventChangeListener: function (callback) {
        this.on(EVENT_DATA_CHANGE, callback);
    },

    removeEventChangeListener: function (callback) {
        this.removeListener(EVENT_DATA_CHANGE, callback);
    },

    emitStockLifetimeDataChange: function () {
        this.emit(STOCK_LIFETIME_DATA_CHANGE);
    },

    registerStockLifetimeDataChangeListener: function (callback) {
        this.on(STOCK_LIFETIME_DATA_CHANGE, callback);
    },

    removeStockLifetimeDataChangeListener: function (callback) {
        this.removeListener(STOCK_LIFETIME_DATA_CHANGE, callback);
    },

    emitNewsChange: function () {
        this.emit(NEWS_CHANGE);
    },

    registerNewsChangeListener: function (callback) {
        this.on(NEWS_CHANGE, callback);
    },

    removeNewsChangeListener: function (callback) {
        this.removeListener(NEWS_CHANGE, callback);
    },

    emitPortfolioStockChange: function () {
        this.emit(PORTFOLIO_STOCK_CHANGE);
    },

    registerPortfolioStockChangeListener: function (callback) {
        this.on(PORTFOLIO_STOCK_CHANGE, callback);
    },

    removePortfolioStockChangeListener: function (callback) {
        this.removeListener(PORTFOLIO_STOCK_CHANGE, callback);
    },


    emitExternalFactorGet: function () {
        console.log("BEACON");
        this.emit(EXTERNAL_DATA_GET);
    },

    registerExternalFactorGetListener: function (callback) {
        this.on(EXTERNAL_DATA_GET, callback);
    },

    removeExternalFactorGetListener: function (callback) {
        this.removeListener(EXTERNAL_DATA_GET, callback);
    },

    registerCompanyProfileChangeListener: function (callback) {
        this.on(COMPANY_PROFILE_CHANGE, callback);
    },

    removeCompanyProfileChangeListener: function (callback) {
        this.removeListener(COMPANY_PROFILE_CHANGE, callback);
    },

    emitCompanyProfileChange: function () {
        this.emit(COMPANY_PROFILE_CHANGE);
    },

    registerServerStockLifetimeListener: function (callback) {
        this.addListener(COMPANY_LIFETIME_GET, callback);
    },

    removeServerStockLifetimeListener: function (callback) {
        this.removeListener(COMPANY_LIFETIME_GET, callback);
    },

    emitServerStockLifetimeListener: function (callback) {
        this.emit(COMPANY_LIFETIME_GET);
    },

    registerUserStockLifetimeListener: function (callback) {
        this.addListener(USER_LIFETIME_GET, callback);
    },

    removeUserStockLifetimeListener: function (callback) {
        this.removeListener(USER_LIFETIME_GET, callback);
    },

    emitUserStockLifetimeListener: function (callback) {
        this.emit(USER_LIFETIME_GET);
    }
});

/**
 * Register for actions with the DataDispatcher.
 * The dispatcher will run the callback with an action every time an action is
 * dispatched using the DataDispatcher.
 */
DataDispatcher.register(function (action) {
    /**
     * Check which action was dispatched and proceed accordingly
     */
    switch (action.actionType) {
        case PomegranateConstants.FILE_OK:
            console.log("File OK action triggered");
            updateServerFiles(action.serverFiles);
            updateUserRICs(action.RICs);
            updateEventVariables(action.eventVariables);
            DataStore.emitFileChange();
            events = [];
            stockLifetimeData = [];
            DataStore.emitEventsChange();
            DataStore.emitStockLifetimeDataChange();
            break;
        case PomegranateConstants.STORE_INITIALISE:
            updateServerFiles(action.serverFiles);
            updateUserRICs(action.RICs);
            updateEventVariables(action.eventVariables);
            DataStore.emitFileChange();
            break;
        case PomegranateConstants.FILE_ERROR:
            console.log("File ERROR action triggered");
            // TODO
            break;
        case PomegranateConstants.SERVER_FILE_DATA_ERROR:
            console.log("Error in getting data from server");
            break;
        case PomegranateConstants.DATA_GET_CUMULATIVE_RETURNS_OK:
            updateCumulativeReturns(action.request, action.data);
            DataStore.emitCumulativeReturnChange();
            break;
        case PomegranateConstants.DATA_GET_CUMULATIVE_RETURNS_ERROR:
            //TODO
            console.log(action.error);
            break;
        case PomegranateConstants.DATA_GET_SINGLE_EVENT_CUMULATIVE_RETURNS_OK:
            updateCumulativeReturns(action.request, action.data);
            DataStore.emitCumulativeReturnChange();
            break;
        case PomegranateConstants.DATA_GET_SINGLE_EVENT_CUMULATIVE_RETURNS_ERROR:
            //TODO
            console.log(action.error);
            break;
        case PomegranateConstants.DATA_GET_EVENTS_OK:
            updateEvents(action.data);
            DataStore.emitEventsChange();
            break;
        case PomegranateConstants.DATA_GET_EVENTS_ERROR:
            //TODO
            console.log(action.error);
            break;
        case PomegranateConstants.DATA_GET_RIC_LIFETIME_OK:
            updateStockLifetimeData(action.data);
            DataStore.emitStockLifetimeDataChange();
            break;
        case PomegranateConstants.DATA_GET_RIC_LIFETIME_ERROR:
            console.log(action.error);
            //TODO
            break;
        case PomegranateConstants.DATA_NEWS_GET_OK:
            updateNews(action.news);
            DataStore.emitNewsChange();
            break;
        case PomegranateConstants.DATA_GET_PORTFOLIO_STOCKS_OK:
            updatePortfolioStocks(action.data);
            DataStore.emitPortfolioStockChange();
            break;
        case PomegranateConstants.STATE_LOGOUT_SUCCESS:
            updateServerFiles({eventFile: null, stockFile: null});
            break;
        case PomegranateConstants.DATA_GET_EXTERNAL_FACTOR:
            console.log("FLASHLIGHT");
            updateExternalFactorData(action.data);
            DataStore.emitExternalFactorGet();
            break;
        case PomegranateConstants.GOT_SERVER_RICS:
            updateServerRICs(action.RICs);
            break;
        case PomegranateConstants.GOT_COMPANY_PROFILE:
            updateCompanyProfile(action.newProfile);
            DataStore.emitCompanyProfileChange();
            break;
        case PomegranateConstants.GOT_COMPANY_LIFETIME:
            updateServerStockLifetimeData(action.lifetime);
            DataStore.emitServerStockLifetimeListener();
            break;
        case PomegranateConstants.GOT_USER_STOCK_LIFETIME:
            updateStockLifetimeData(action.lifetime);
            DataStore.emitUserStockLifetimeListener();
            break;
        case PomegranateConstants.ADD_STOCK_TO_PORTFOLIO:
            addPortfolioStock(action.newStock);
            break;
        default:
            console.log("Bad Action Code");
    }
});

module.exports = DataStore;

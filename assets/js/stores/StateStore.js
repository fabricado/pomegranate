/**
 * StateDispatcher is used to dispatch an action to the StateStore
 *
 * EventEmitter is a node object that can be used to broadcast events to registered entities
 *
 * assign is a utility that allows one to combine two or more objects. Overlapping attributes will be overridden
 * by each subsequent object
 *
 * PomegranateConstants are predefined constants for the pomegranate app
 */

var StateDispatcher = require('../dispatchers/StateDispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var PomegranateConstants = require('../constants/PomegranateConstants');
var _ = require('lodash');

var STATE_STORE_CHANGE = 'state_store_changed';
var STATE_LOGIN_SUCCESS = 'state_login_success';
var STATE_LOGOUT_SUCCESS = 'state_logout_success';

/**
 * Variable to store the FilterVariableForm state
 */
var eventFilterState = {};

function updateEventFilterState(newState) {
    eventFilterState = newState;
}

/**
 * Variable to store the Event Study Graph state
 */
var eventStudyGraphState = {};

function updateEventStudyGraphState(newState) {
    eventStudyGraphState = newState;
}

/**
 * Variable to store username
 */
var username;

function setUsername(newName) {
    username = newName;
}

function purge() {
    console.log("PURGE");
    eventFilterState = {};
    eventStudyGraphState = {};
}

/**
 *
 */
var StateStore = assign({}, EventEmitter.prototype, {

    getEventFilterState() {
        return eventFilterState;
    },

    getEventStudyGraphState() {
        return eventStudyGraphState;
    },

    getUsername() {
        return username;
    },

    registerStateChangeListener(callback) {
        this.on(STATE_STORE_CHANGE, callback);
    },

    removeStateChangeListener(callback) {
        this.removeListener(STATE_STORE_CHANGE, callback);
    },

    emitStateChange() {
        this.emit(STATE_STORE_CHANGE);
    },

    registerLoginSuccessListener(callback) {
        this.on(STATE_LOGIN_SUCCESS, callback);
    },

    removeLoginSuccessListener(callback) {
        this.removeListener(STATE_LOGIN_SUCCESS, callback);
    },

    emitLoginSuccess() {
        this.emit(STATE_LOGIN_SUCCESS);
    },

    registerLogoutSuccessListener(callback) {
        this.on(STATE_LOGOUT_SUCCESS, callback);
    },

    removeLogoutSuccessListener(callback) {
        this.removeListener(STATE_LOGOUT_SUCCESS, callback);
    },

    emitLogoutSuccess() {
        this.emit(STATE_LOGOUT_SUCCESS);
    }
});


/**
 * Register for actions with the DataDispatcher.
 * The dispatcher will run the callback with an action every time an action is
 * dispatched using the DataDispatcher.
 */
StateDispatcher.register(function (action) {
    /**
     * Check which action was dispatched and proceed accordingly
     */
    switch (action.actionType) {
        case PomegranateConstants.STATE_SET_EVENT_FILTER:
            updateEventFilterState(action.newState);
            break;
        case PomegranateConstants.STATE_SET_EVENT_STUDY_GRAPH:
            updateEventStudyGraphState(action.newState);
            StateStore.emitStateChange();
            break;
        case PomegranateConstants.STATE_LOGIN_SUCCESS:
            setUsername(action.username);
            StateStore.emitLoginSuccess();
            break;
        case PomegranateConstants.STATE_LOGOUT_SUCCESS:
            setUsername(undefined);
            StateStore.emitLogoutSuccess();
            break;
        case PomegranateConstants.STATE_SET_ALL:
            if (action.states.EventFilterState) {
                updateEventFilterState(action.states.EventFilterState);
            }
            if (action.states.EventStudyGraphState) {
                updateEventStudyGraphState(action.states.EventStudyGraphState);
            }
            break;
        case PomegranateConstants.STATE_PURGE:
            purge();
            break;
    }
});

module.exports = StateStore;

var React = require('react');
var ReactBootstrap = require('react-bootstrap');
var PortfolioStocks = require('./PortfolioStocks');
var PortfolioEvents = require('./PortfolioEvents');
var PortfolioGraph = require('./PortfolioGraph');
var DataStore = require('../stores/DataStore');
var DataActions = require('../actions/DataActions');
var ServerInterface = require('../modules/ServerInterface');
var _ = require('lodash');

var PortfolioWindow = React.createClass({
    displayName: 'PortfolioWindow',

    getInitialState: function () {
        return {
            portfolioStocks: DataStore.getPortfolioStocks(),
            events: DataStore.getEvents(),
            stockData: DataStore.getStockLifetimeData(this.getRicList(DataStore.getPortfolioStocks())),
            //will just have a date and ric
            chosenEvent: {ric:"",date:""}
        };
    },

    getRicList: function (portfolioStocks) {
        var ricList = [];
        for (var i = 0; i < portfolioStocks.length;++i) {
            ricList.push(portfolioStocks[i].ric);
        }
        return ricList;
    },

    onEventDelete: function (event) {
        ServerInterface.sendEventDelete(event).then(function onResolve (result) {
            DataActions.getEvents();
        }, function onReject(err) {
            console.log(err);
        })
    },

    onStockDelete: function (stock) {
        ServerInterface.sendPortfolioStockDelete(stock).then(function onResolve (result) {
            DataActions.getPortfolioStocks();
        }, function onReject(err) {
            console.log(err);
        })
    },

    componentDidMount: function () {
        DataStore.registerPortfolioStockChangeListener(this.onPortfolioStockChange);
        DataStore.registerEventChangeListener(this.onEventsChange);
        DataStore.registerStockLifetimeDataChangeListener(this.onStockLifetimeDataChange);
        DataActions.getPortfolioStocks();
        DataActions.getEvents();
        DataActions.getLifetimeData(this.getRicList(this.state.portfolioStocks));
    },

    componentWillUnmount: function () {
        DataStore.removePortfolioStockChangeListener(this.onPortfolioStockChange);
        DataStore.removeEventChangeListener(this.onEventsChange);
        DataStore.removeStockLifetimeDataChangeListener(this.onStockLifetimeDataChange);
    },

    onPortfolioStockChange: function () {
        var portfolioStocks = DataStore.getPortfolioStocks();
        DataActions.getLifetimeData(this.getRicList(portfolioStocks));
        this.setState({portfolioStocks: portfolioStocks});
    },

    onStockLifetimeDataChange: function () {
        var portfolioStocks = this.state.portfolioStocks;
        var updatedStockData = DataStore.getStockLifetimeData(this.getRicList(portfolioStocks));
        this.setState({stockData: updatedStockData});
    },

    onEventsChange: function () {
        this.setState({events: DataStore.getEvents()});
    },

    render: function () {
        return (
            <div className="portfolioWindowWrapper">
                <div className="tableWrapper">
                    <PortfolioStocks stockInfo={this.state.portfolioStocks} onStockDelete={this.onStockDelete}/>
                    <PortfolioEvents eventInfo={this.state.events} onEventDelete={this.onEventDelete}/>
                </div>
                <PortfolioGraph stockData={this.state.stockData} stockInfo={this.state.portfolioStocks} 
                                events={this.state.events} chosenEvent={this.state.chosenEvent} />
            </div>
        );
    }
});

module.exports = PortfolioWindow;

var React = require('react');
var ReactBootstrap = require('react-bootstrap');
var _ = require('lodash');
var moment = require('moment');
var Reactable = require('reactable');

var PortfolioStocks = React.createClass({
    displayName: 'PortfolioStocks',

    getInitialState: function () {
        var stockInfo = this.props.stockInfo;
        for (var i = 0; i < stockInfo.length;++i) {
            stockInfo[i].buyInDate = moment(stockInfo[i].buyInDate,[moment.ISO_8601,'DD-MM-YYYY']).format('DD-MM-YYYY');
        }
        return {
            columns: ['RIC', 'Buy In Date', 'Amount','Stock Info','Remove'],
            stockInfo: stockInfo,
            showModal: false,
            currStockIndex: -1
        }
    },

    componentWillReceiveProps: function (nextProps) {
        var stockInfo = nextProps.stockInfo;
        for (var i = 0; i < stockInfo.length;++i) {
            stockInfo[i].buyInDate = moment(stockInfo[i].buyInDate,[moment.ISO_8601,'DD-MM-YYYY']).format('DD-MM-YYYY');
        }
        this.setState({stockInfo: nextProps.stockInfo});
    },

    handleStockDelete: function (stockIndex,event) {
        this.props.onStockDelete(this.state.stockInfo[stockIndex]);
    },

    handleInfoClick: function (stockIndex,event) {
        console.log(this.state.stockInfo[stockIndex]);
        this.setState({currStockIndex:stockIndex});
        this.setState({showModal:true});
    },

    handleInfoClose: function() {
        this.setState({currStockIndex:-1});
        this.setState({showModal:false});
    },

    renderTableColumns: function (columnName, i) {
        return (
            <th key={i}>{columnName}</th>
        );
    },

    renderRowData: function (colName,stockIndex,i) {
        if (i < this.state.columns.length - 2) {
            return (
                <Reactable.Td key={i} column={colName}>{this.state.stockInfo[stockIndex][_.camelCase(colName)]}</Reactable.Td>
            );
        } else if (i == this.state.columns.length - 2) {
            return <Reactable.Td key={i} column={colName}>
                <i className="fa fa-2x fa-info-circle infoSymbol" onClick={this.handleInfoClick.bind(this,stockIndex)}/>
            </Reactable.Td>
        } else {
            return (
                <Reactable.Td key={i} column={colName} value="exit">
                    <i className="fa fa-2x fa-times deleteSymbol"
                       onClick={this.handleStockDelete.bind(this,stockIndex)}/>
                </Reactable.Td>
            );
        }
    },

    renderTableData: function (tableObject, i) {
        return (
            <Reactable.Tr key={i}>
                {this.state.columns.map(function (colName, colIndex) {
                    return this.renderRowData(colName, i, colIndex);
                }, this)}
            </Reactable.Tr>
        );
    },

    tableDateSorter: function (a,b) {
        if (moment(a,'DD-MM-YYYY').isAfter(moment(b,'DD-MM-YYYY'))) {
            return 1;
        } else {
            return -1;
        }
    },

    renderStockInfo: function() {
        return(
            <ReactBootstrap.Modal.Body>
            </ReactBootstrap.Modal.Body>
        );
    },


    render: function () {
        return (
            <div className="portfolioStocksWrapper">
                <div className="stockInfoWrapper">
                    <ReactBootstrap.Modal show={this.state.showModal} onHide={this.handleInfoClose} dialogClassName="stockInfoModal">
                        <ReactBootstrap.Modal.Header closeButton>
                            <ReactBootstrap.Modal.Title>Stock Info</ReactBootstrap.Modal.Title>
                        </ReactBootstrap.Modal.Header>
                        {this.renderStockInfo()}
                        <ReactBootstrap.Modal.Footer>
                            <ReactBootstrap.Button onClick={this.handleInfoClose}>Close</ReactBootstrap.Button>
                        </ReactBootstrap.Modal.Footer>
                    </ReactBootstrap.Modal>
                </div>
                <Reactable.Table className="table table-hover" sortable={[
                    'RIC',
                    {column:'Buy In Date',sortFunction:this.tableDateSorter},
                    {column:'Amount',sortFunction:'Numeric'}
                ]} defaultSort={{column: 'Buy In Date',sortFunction: this.tableDateSorter,direction: 'desc'}}>
                    {this.state.stockInfo.map(this.renderTableData)}
                </Reactable.Table>
            </div>
        );
    }
});

module.exports = PortfolioStocks;

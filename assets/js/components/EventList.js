var React = require('react');
var PomegranateConstants = require('../constants/PomegranateConstants');
var EventList = React.createClass({

    propTypes: {
        events: React.PropTypes.array.isRequired
    },

    getInitialState: function () {
        return {
            events: [],
            selected: ""
        };
    },

    componentDidMount: function () {
        console.log(this.props);
        this.setState({
            events: this.props.events,
            selected: (this.props.events.length > 0)? this.props.events[0].ric + "_" + this.props.events[0].date: ""
        });
        if (this.props.events.length > 0) {
            this.props.onMainSetChange(this.props.events[0].ric,this.props.events[0].date);
        }
    },

    componentWillReceiveProps: function (nextProps) {
        console.log("WILL RECEIVE PROPS");
        this.setState({
            events: nextProps.events
        });
    },

    renderEventListEntry: function (variable, i) {
        if ((this.state.selected != "") && (variable.ric == this.state.selected.split("_")[0]) && (variable.date == this.state.selected.split("_")[1])) {
            return (
                <tr key={i} className="selectedListItem" onClick={this.handleListItemClick}>
                    <td id={variable.ric+"_"+variable.date}>{variable.ric}</td>
                    <td id={variable.ric+"_"+variable.date}>{variable.date}</td>
                </tr>
            );
        } else {
            return (
                <tr key={i} className="eventListItem" onClick={this.handleListItemClick}>
                    <td id={variable.ric+"_"+variable.date}>{variable.ric}</td>
                    <td id={variable.ric+"_"+variable.date}>{variable.date}</td>
                </tr>
            );
        }
    },

    handleListItemClick: function (event) {
        console.log(event.target.id);
        this.props.onMainSetChange(event.target.id.split("_")[0], event.target.id.split("_")[1]);
        this.setState({selected: event.target.id});
    },

    render: function () {
        //console.log("RENDERING EVENT LIST");
        //console.log(this.state.events);
        return (
            <div className="eventList">
                <h3 style={{'textAlign': 'center'}}>ALL EVENTS</h3>
                <table className="table">
                    <thead>
                    <tr>
                        <th>RIC</th>
                        <th>DATE</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.events.map(this.renderEventListEntry)}
                    </tbody>
                </table>
            </div>
        )
    }
});

module.exports = EventList;

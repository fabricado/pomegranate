var React = require('react');
var FactorStudyGraph = require('./FactorStudyGraph');
var FactorStudySidebar = require('./FactorStudySidebar');
var MiniEventStudyModal = require('./MiniEventStudyModal');
var moment = require('moment');
var DataStore = require('../stores/DataStore');

var FactorStudyWindow = React.createClass({

    displayName: "FactorStudyWindow",

    getInitialState: function () {
        return {
            showEventMaker: false,
            date: '',
            showModal: false,
            rics: DataStore.getUserRICs()
        };
    },

    componentDidMount: function () {

    },

    renderEventMaker: function () {

    },
    
    onShowFalse: function () {
        this.setState({showModal: false});
    },

    onGraphClick: function () {
        //needs to trigger showing the event-sudy modal
        console.log(AmCharts.myCurrentPosition);
        this.setState({date: moment(AmCharts.myCurrentPosition).toDate(),showModal:true});
    },

    render: function () {
        console.log(this.state);
        return (
            <div className="factorStudyWindowWrapper">
                {this.renderEventMaker()}
                <FactorStudyGraph clickHandler={this.onGraphClick}/>
                <FactorStudySidebar />
                <MiniEventStudyModal RICs={this.state.rics} onShowChange={this.onShowFalse} 
                                     studyDate={this.state.date} show={this.state.showModal}/>
            </div>
        );
    }
});

module.exports = FactorStudyWindow;

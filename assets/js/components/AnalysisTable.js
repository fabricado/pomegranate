var React = require('react');
var Reactable = require('reactable');

var AnalysisTable = React.createClass({
    onWillReceiveProps: function (nextProps) {
      console.log(nextProps);
    },

    render: function () {
        return (
            <div className="analysisTableWrapper" style={{overflowY:'scroll'}}>
                <Reactable.Table data={this.props.impactData} className="table table-hover"/>
            </div>
        )
    }
});

module.exports = AnalysisTable;

/**
 * React is Facebook's V framework that we are using
 *
 * LifetimeRightSidebar is the react element that renders the right sidebar
 * It will contain a list of events which the user can interact with
 *
 * LifetimeGraph is the react element that renders the graph for the lifetime section
 */
var React = require('react');
var DataStore = require('../stores/DataStore');
var DataActions = require('../actions/DataActions');
var AnalysisSidebar = require('./AnalysisSidebar');
var AnalysisTable = require('./AnalysisTable');
var async = require('async');
var ServerInterface = require('../modules/ServerInterface');
var moment = require('moment');

var AnalysisWindow = React.createClass({
    displayName: "AnalysisWindow",

    /**
     * Sets the inital state of the component on creation
     */
    getInitialState: function () {
        return {
            tableData : [],
            tempData : []
        };
    },

    /**
     * Called as soon as the component has mounted for the first time
     */
    componentDidMount: function () {
        //this.reAnalyse(DataStore.getPortfolioStocks()); //analyse on startup
        DataActions.getEvents();
    },

    /**
     * Called before the component is destroyed
     */
    componentWillUnmount: function () {
    },

    reAnalyse: function (portfolio) {
        //do a lot of stuff
        //then render that stuff
        var events = DataStore.getEvents();
        console.log(events);
        console.log("About to ASYNC");
        async.each(events, function(e, callback) {
           //an event,
           //console.log(e, callback);
           //make a call with random flag, lowerWindow:-20, upperWindow:20, randomDate: dd-mm-yy
           ServerInterface.getRandomEventData(moment(e.date, "DD-MMM-YYYY").format("DD-MM-YYYY"), -20, 20).then(function onResolve(result) {
                //Promise succeeded
                //console.log(result);
                this.getImpact(result.results.eventData);
                callback();
                console.log("called back");
            }.bind(this), function onReject(error) {
                console.log(error);
                callback(error);
            }.bind(this));
            }.bind(this),
           function (err) {
            //every async call done
            if (err) console.log(err);
            //move tempState thing true
            console.log("DUNNIGIN");
            this.setState({tableData: this.state.tempData});
           }.bind(this));
        //}.bind(this));
    },

    getImpact: function (data, callback) {//data is array, entry per stock
        var stocks = DataStore.getPortfolioStocks();
        //calulcate weighted sum
        //add it to the temp table
        //console.log("impact");
        //console.log(data);

        if (data.length == 0) {
            return;
        }

        var impact = {"Event Date":data[0].date,"Value Change ($)": 0, "Value Change (%)": 0, "Coefficient of Variation": 0};
        var portfolioInitValue = 0;
        var portfolioEndValue = 0;
        var totalVolatility = 0;
        for (var d in data) {
            var stock = _.find(stocks, function(s) {return s.ric == data[d].ric});
            //console.log(stock);
            if (stock == undefined) continue;
            //console.log(stock);
            var firstHalfAverage = this.getFirstHalfAverage(data[d].cumulativeReturns);
            var secondHalfAverage = this.getSecondHalfAverage(data[d].cumulativeReturns);
            //console.log(firstHalfAverage);
            //console.log(secondHalfAverage);
            portfolioInitValue += firstHalfAverage*stock.amount;
            portfolioEndValue += secondHalfAverage*stock.amount;
            totalVolatility += data[d].coefficientOfVariation * stock.amount * stock.buyInPrice;
        }
        //console.log("Doniggun");
        impact["Value Change ($)"] = portfolioEndValue - portfolioInitValue;
        impact["Value Change (%)"] = (portfolioEndValue - portfolioInitValue) / portfolioInitValue;
        //console.log("ttl: ", totalVolatility);
        //console.log("portfolioInitValue:", portfolioInitValue);
        //console.log("portfolioEndValue:", portfolioEndValue);
        impact["Coefficient of Variation"] = totalVolatility / portfolioInitValue;
        //console.log(impact);
        var temp = this.state.tempData;
        temp.push(impact);
        this.setState({tempData: temp});
        //console.log("cb");
        //callback();
        //console.log("beyone");
    },

    getFirstHalfAverage: function (vals) {
        var mid = vals.length/2;
        var sum = 0;
        for (var i = 0; i < mid; i++) {
            sum += vals[i];
        }
        return sum/mid;
    },

    getSecondHalfAverage: function (vals) {
        var mid = Math.ceil(vals.length/2);
        var sum = 0;
        for (var i = mid; i < vals.length; i++) {
            sum += vals[i];
        }
        return sum/mid;
    },

    /**
     * Renders a view for this component
     */
    render: function () {
    //console.log("RENDERING LTW");
    //console.log(this.state.events);
        return (
            <div className="analysisWindowWrapper">
                <AnalysisTable impactData={this.state.tableData}/>
                <AnalysisSidebar reAnalyse={this.reAnalyse}/>
            </div>
        );
    }
});

module.exports = AnalysisWindow;

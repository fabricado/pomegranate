/**
 * React is Facebook's V framework that we are using
 *
 * DataStore is a flux store that holds data about event + stock files
 *
 * PomegranateConstants are predefined constants for the pomegranate app
 *
 * assign is a utility that allows one to combine two or more objects. Overlapping attributes will be overridden
 * by each subsequent object
 *
 * TabBar is a react class that will visualise the inner feature tabs
 */
var React = require('react');
var DataStore = require('../stores/DataStore');
var StateStore = require('../stores/StateStore');
var PomegranateConstants = require('../constants/PomegranateConstants');
var assign = require('object-assign');
var TabBar = require('./TabBar');
var LifetimeWindow = require('./LifetimeWindow');
var FileUploadWindow = require('./FileUploadWindow');
var EventStudyWindow = require('./EventStudyWindow');
var NewsWindow = require('./NewsWindow');
var AddEventsWindow = require('./AddEventsWindow');
var DataActions = require('../actions/DataActions');
var StateActions = require('../actions/StateActions');
var PortfolioWindow = require('./PortfolioWindow');
var FactorStudyWindow = require('./FactorStudyWindow');
var StockStudyWindow = require('./StockStudyWindow');
var AnalysisWindow = require('./AnalysisWindow');

/**
 * The Root Component of the PomegranateApp
 */
var PomegranateApp = React.createClass({
    displayName: "PomegranateApp",

    /**
     * Sets the initial state of the component on creation
     */
    getInitialState: function () {
        return assign({
            currentTab: PomegranateConstants.TAB_UPLOAD
        }, this.getStateFromDataStore());
    },

    /**
     * Returns all data relevant to the PomegranateApp
     */
    getStateFromDataStore: function () {
        return {
            serverFiles: DataStore.getFiles(),
            RICs: DataStore.getUserRICs(),
            eventVariables: DataStore.getEventVariables()
        }
    },

    /**
     * Called as soon as the component has mounted for the first time
     */
    componentDidMount: function () {
        DataStore.registerFileChangeListener(this.onFileChange);
        StateStore.registerStateChangeListener(this.onStateStoreChange);
        StateStore.registerLoginSuccessListener(this.onLoginSuccess);
        StateStore.registerLogoutSuccessListener(this.onLogoutSuccess);
        StateActions.initialise();
        DataActions.getPortfolioStocks();

    },

    /**
     * Called before the component is destroyed
     */
    componentWillUnmount: function () {
        DataStore.removeFileChangeListener(this.onFileChange);
    },

    /**
     * Should be called when the file is changed
     * Made to be registered with the DataStore
     */
    onFileChange: function () {
        this.setState(this.getStateFromDataStore());
    },

    /**
     * Should be called when the state store is changed
     * Made to be registered with the StateStore
     */
    onStateStoreChange: function () {
        StateActions.sendStateToServer();
    },

    onLoginSuccess: function () {
        DataActions.initialise();
    },

    onLogoutSuccess: function () {
        this.setState({serverFiles: []});
    },

    /**
     * Called when a new tab has been clicked
     * @param newTab: must be a PomegranateConstant
     */
    onTabChange: function (newTab) {
        this.setState({currentTab: newTab});
    },

    renderLifetime: function () {
        return (
            <LifetimeWindow RICs={this.state.RICs}/>
        );
    },

    renderEventStudy: function () {
        return (
            <EventStudyWindow eventVariables={this.state.eventVariables}/>
        );
    },

    renderUpload: function () {
        return (
            <FileUploadWindow serverFiles={this.state.serverFiles}/>
        );
    },

    renderNews: function () {
        return (
            <NewsWindow eventVariables={this.state.eventVariables} RICs={this.state.RICs}/>
        );
    },

    renderPortfolio: function () {
        return (
            <PortfolioWindow />
        );
    },

    renderAddEvents: function () {
        return (
            <AddEventsWindow />
        );
    },

    renderLogin: function () {
        return (
            <Login />
        );
    },

    renderFactorStudy: function () {
        return (
            <FactorStudyWindow />
        )
    },

    renderStockStudy: function() {
        return (
            <StockStudyWindow />
        );
    },

    renderAnalysisTab: function() {
        return (
            <AnalysisWindow />
        );
    },

    /**
     * Renders a view for this component
     */
    render: function () {
        var InnerComponent;
        switch (this.state.currentTab) {
            case PomegranateConstants.TAB_LIFETIME:
                InnerComponent = this.renderLifetime();
                break;
            case PomegranateConstants.TAB_EVENT_STUDY:
                InnerComponent = this.renderEventStudy();
                break;
            case PomegranateConstants.TAB_UPLOAD:
                InnerComponent = this.renderUpload();
                break;
            case PomegranateConstants.TAB_NEWS:
                InnerComponent = this.renderNews();
                break;
            case PomegranateConstants.TAB_ADD_EVENTS:
                InnerComponent = this.renderAddEvents();
                break;
            case PomegranateConstants.TAB_PORTFOLIO:
                InnerComponent = this.renderPortfolio();
                break;
            case PomegranateConstants.TAB_FACTOR_STUDY:
                InnerComponent = this.renderFactorStudy();
                break;
            case PomegranateConstants.TAB_STOCK_STUDY:
                InnerComponent = this.renderStockStudy();
                break;
            case PomegranateConstants.TAB_ANALYSIS:
                InnerComponent = this.renderAnalysisTab();
                break;
            default:
                console.error("UNKNOWN TAB ", this.state.currentTab);
        }
        return (
            <div className="pomegranateAppRoot">
                <TabBar onTabChange={this.onTabChange}/>
                {InnerComponent}
            </div>
        );
    }
});

module.exports = PomegranateApp;

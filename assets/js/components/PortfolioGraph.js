var React = require('react');
var ReactBootstrap = require('react-bootstrap');
var ChartCreator = require('../modules/ChartCreator');
var moment = require('moment');

var PortfolioGraph = React.createClass({
    displayName: 'PortfolioGraph',

    getInitialState: function () {
        return {
            stockData: this.props.stockData,
            events: this.props.events,
            chosenEvent: this.props.chosenEvent
        };
    },

    componentDidMount: function () {
        ChartCreator.makeLifetimeChart("portfolioChartDiv",this.state.stockData,this.state.events,this.state.chosenEvent);
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState({
            stockData: nextProps.stockData,
            events: nextProps.events,
            chosenEvent: nextProps.chosenEvent
        });
    },

    //called after render is done
    //not when first mounted
    componentDidUpdate: function () {
        ChartCreator.makeLifetimeChart("portfolioChartDiv",this.state.stockData,this.state.events,this.state.chosenEvent);
    },

    render: function () {
        return (
            <div className="portfolioGraphWrapper">
                <div id="portfolioChartDiv" style={{width:'98%',height:'100%',margin:'0 auto'}}></div>
            </div>
        );
    }
});

module.exports = PortfolioGraph;

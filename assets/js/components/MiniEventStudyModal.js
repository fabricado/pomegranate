var React = require('react');
var ReactBootstrap = require('react-bootstrap');
var MiniEventStudy = require('./MiniEventStudy');
var DataStore = require('../stores/DataStore');
var moment = require('moment');
var ServerInterface = require('../modules/ServerInterface');
var DataActions = require('../actions/DataActions');

var MiniEventStudyModal = React.createClass({
    displayName: 'MiniEventStudyModal',

    getInitialState: function () {
        return {
            showES: this.props.show,
            showEvent: false,
            miniStudyDate: this.props.studyDate,
            selectedRic: '',
            newEvent: {ric: "", date: "", variables: {}},
            newVarName: '',
            newVarValue: ''
        };
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState({miniStudyDate: nextProps.studyDate,showES: nextProps.show});
    },

    changeSelectedRic: function (newRic) {
        this.setState({selectedRic: newRic});
    },

    handleHideMiniEventStudy: function () {
        this.props.onShowChange(false);
        this.setState({showES:false});
    },

    handleMiniEventStudyShow: function () {
        this.setState({showES: true});
    },

    handleShowAddEvent: function () {
        this.setState({
            newEvent: {ric:this.state.selectedRic,date:this.state.miniStudyDate,variables:{}},
            showEvent: true
        });
    },

    handleHideAddEvent: function () {
        this.setState({showEvent: false});
    },

    renderNewVariables: function (varName,i) {
        return (
            <h5>Variable: {varName} Value: {this.state.newEvent.variables[varName]}
                <ReactBootstrap.Button bsSize="small" onClick={this.removeNewVariable.bind(this,varName)}
                                       style={{marginLeft:'5px'}}>Remove</ReactBootstrap.Button></h5>
        );
    },

    addNewVariable: function (event) {
        event.preventDefault();
        var newEvent = this.state.newEvent;
        var variables = newEvent.variables;
        variables[this.state.newVarName] = this.state.newVarValue;
        this.setState({newEvent:newEvent});
    },

    removeNewVariable: function (varName) {
        var newEvent = this.state.newEvent;
        delete newEvent.variables[varName];
        this.setState({newEvent: newEvent});
    },

    changeNewVarName: function (event) {
        this.setState({newVarName:event.target.value});
    },

    changeNewVarValue: function (event) {
        this.setState({newVarValue:event.target.value});
    },

    finishEventAdd: function () {
        ServerInterface.postNewEvent(this.state.newEvent).then(function onResolve(result) {
            this.setState({showEvent: false});
            DataActions.getEvents();
        }.bind(this), function onReject(err) {
            console.log(err);
        });
    },

    renderMainModal: function () {
        console.log(this.state);
        return (
            <ReactBootstrap.Modal show={this.state.showES} onHide={this.handleHideMiniEventStudy} dialogClassName="miniEventstudyModal">
                <ReactBootstrap.Modal.Header closeButton>
                    <ReactBootstrap.Modal.Title>Mini Event Study</ReactBootstrap.Modal.Title>
                </ReactBootstrap.Modal.Header>
                <ReactBootstrap.Modal.Body>
                    <MiniEventStudy eventDate={moment(this.state.miniStudyDate).format('YYYY-MM-DD')} onRicSelect={this.changeSelectedRic}
                                    RICs={this.props.RICs}/>
                </ReactBootstrap.Modal.Body>
                <ReactBootstrap.Modal.Footer>
                    <ReactBootstrap.Button style={{float:'left'}} onClick={this.handleShowAddEvent}
                                           bsStyle='primary'>Add event to portfolio</ReactBootstrap.Button>
                    <ReactBootstrap.Button className="closeButton" onClick={this.handleHideMiniEventStudy}>Close</ReactBootstrap.Button>
                </ReactBootstrap.Modal.Footer>
            </ReactBootstrap.Modal>
        );
    },

    renderAddEventModal: function () {
        return (
            <ReactBootstrap.Modal show={this.state.showEvent} onHide={this.handleHideAddEvent}>
                <ReactBootstrap.Modal.Header closeButton>
                    <ReactBootstrap.Modal.Title>Add new event</ReactBootstrap.Modal.Title>
                </ReactBootstrap.Modal.Header>
                <ReactBootstrap.Modal.Body>
                    <h4>Event RIC: {this.state.newEvent.ric}</h4>
                    <h4>Event Date: {moment(this.state.newEvent.date).format('DD-MM-YYYY')}</h4>
                    {_.keys(this.state.newEvent.variables).map(this.renderNewVariables)}
                    <h5>Add new event variables</h5>
                    <ReactBootstrap.Form inline>
                        <ReactBootstrap.FormGroup>
                            <ReactBootstrap.ControlLabel>Name</ReactBootstrap.ControlLabel>
                            {' '}
                            <ReactBootstrap.FormControl onChange={this.changeNewVarName}
                                                        type="text" placeholder="Variable Name" />
                        </ReactBootstrap.FormGroup>
                        {' '}
                        <ReactBootstrap.FormGroup>
                            <ReactBootstrap.ControlLabel>Value</ReactBootstrap.ControlLabel>
                            {' '}
                            <ReactBootstrap.FormControl onChange={this.changeNewVarValue}
                                                        type="number" step="any" placeholder="E.g. 22.3" />
                        </ReactBootstrap.FormGroup>
                        {' '}
                        <ReactBootstrap.Button type="submit" onClick={this.addNewVariable}>
                            Add Variable
                        </ReactBootstrap.Button>
                    </ReactBootstrap.Form>
                </ReactBootstrap.Modal.Body>
                <ReactBootstrap.Modal.Footer>
                    <ReactBootstrap.Button style={{float:'left'}} bsStyle="primary"
                                           onClick={this.finishEventAdd}>Finalize Event</ReactBootstrap.Button>
                    <ReactBootstrap.Button onClick={this.handleHideAddEvent}>Close</ReactBootstrap.Button>
                </ReactBootstrap.Modal.Footer>
            </ReactBootstrap.Modal>
        );
    },

    render: function () {
        return (
            <div>
                {this.renderMainModal()}
                {this.renderAddEventModal()}
            </div>
        );
    }
});

module.exports = MiniEventStudyModal;

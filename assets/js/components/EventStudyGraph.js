/**
 * React is Facebook's V framework that we are using
 *
 * DataStore is a flux store that holds data about event + stock files
 *
 * PomegranateConstants are predefined constants for the pomegranate app
 */
var React = require('react');
var DataStore = require('../stores/DataStore');
var StateActions = require('../actions/StateActions');
var StateStore = require('../stores/StateStore');
var PomegranateConstants = require('../constants/PomegranateConstants');
var ChartCreator = require('../modules/ChartCreator');

var EventStudyGraph = React.createClass({
    displayName: "EventStudyGraph",

    /**
     * Property validation and requirements
     */
    propTypes: {
        crData: React.PropTypes.object.isRequired
    },

    /**
     * Sets the initial state of the component on creation
     */
    getInitialState: function () {
        return {};
    },

    /**
     * Called as soon as the component has mounted for the first time
     */
    componentDidMount: function () {
        var newState = StateStore.getEventStudyGraphState();
        this.setState(newState);;
        if (newState.crData) {
            ChartCreator.makeCumulativeReturnChart("chartdiv",newState.crData);
        }
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState(nextProps);
        ChartCreator.makeCumulativeReturnChart("chartdiv",nextProps.crData);
        StateActions.setEventStudyGraphState(nextProps);
    },

    /**
     * Called before the component is destroyed
     */
    componentWillUnmount: function () {
        StateActions.setEventStudyGraphState(this.state);
    },

    /**
     * Renders a view for this component
     */
    render: function () {
        return (
            <div className="eventStudyGraphWrapper">
                <div id="chartdiv" style={{width: '100%', height: '100%'}}></div>
            </div>
        );
    }
});

module.exports = EventStudyGraph;

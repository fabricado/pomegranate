/**
 * React is Facebook's V framework that we are using
 */
var React = require('react');
var $ = require('jquery');
var DataActions = require('../actions/DataActions');
var StateActions = require('../actions/StateActions');
var StateStore = require('../stores/StateStore');
var PomegranateConstants = require('../constants/PomegranateConstants');
var ServerInterface = require('../modules/ServerInterface');
var ReactBootstrap = require('react-bootstrap');
var LoginForm = require('./LoginForm');
var CreateUserForm = require('./CreateUserForm');

var FileUploadWindow = React.createClass({
    displayName: "FileUploadWindow",

    /**
     * Property validation and requirements
     */
    propTypes: {
        serverFiles: React.PropTypes.object.isRequired
    },

    /**
     * Sets the initial state of the component on creation
     */
    getInitialState: function () {
        return {
            showUserLogin: false,
            showUserCreate: false,
            loggedIn: (StateStore.getUsername() != undefined)
        };
    },

    /**
     * Called as soon as the component has mounted for the first time
     */
    componentDidMount: function () {

        //hide the loading gif
        var stockFileUploadSubmit = $('.stockFileUploadSubmit');
        var eventFileUploadSubmit = $('.eventFileUploadSubmit');
        $(stockFileUploadSubmit).find('.loader_gif').hide();
        $(eventFileUploadSubmit).find('.loader_gif').hide();
        //$(fileUploadSubmitContainer).find('input[type=submit]').show();

        StateStore.registerLoginSuccessListener(this.handleLoginSuccess);
        StateStore.registerLogoutSuccessListener(this.handleLogoutSuccess);
    },

    /**
     * Called before the component is destroyed
     */
    componentWillUnmount: function () {
        StateStore.removeLoginSuccessListener(this.handleLoginSuccess);
        StateStore.removeLogoutSuccessListener(this.handleLogoutSuccess);
    },

    handleStockSubmit: function(event) {
        event.preventDefault();

        var stockFileUploadSubmit = $('.stockFileUploadSubmit');
        $(stockFileUploadSubmit).find('.loader_gif').show();


        var stockFileData = new FormData();

        //stockFileData.append('token', StateStore.getToken());
        stockFileData.append('fileType', 'stockFile');
        stockFileData.append('stockFile', $(event.target).find('input[name=stockFile]')[0].files[0]);

        ServerInterface.sendStockFileToServer(stockFileData)
            .then(function onResolve(stockFileResult) {
                DataActions.onFileOK();
                StateActions.purge();
                $(stockFileUploadSubmit).find('.loader_gif').hide();
            }, function onReject(err) {
                DataActions.onFileError();
            });

    },

    /**
     *
     */
    handleEventSubmit: function (event) {
        event.preventDefault();

        var eventFileUploadSubmit = $('.eventFileUploadSubmit');
        $(eventFileUploadSubmit).find('.loader_gif').show();

        var eventFileData = new FormData();

        //eventFileData.append('token', StateStore.getToken());
        eventFileData.append('fileType', 'eventFile');
        eventFileData.append('eventFile', $(event.target).find('input[name=eventFile]')[0].files[0]);

        ServerInterface.sendEventFileToServer(eventFileData)
            .then(function onResolve(eventFileResult) {
                DataActions.onFileOK();
                StateActions.purge();
                $(eventFileUploadSubmit).find('.loader_gif').hide();
            }, function onReject(err) {
                DataActions.onFileError();
            });
    },

    /**
     *
     */
    handleNewSessionSubmit: function (event) {
        event.preventDefault();

        /*ServerInterface.getTokenFromServer()
            .then(function onResolve(result) {
                //new session ok
                StateActions.setStateToken(result.token);
            }, function onReject(err) {
                //new session not ok
                console.log("Could not make new session");
            });*/
    },

    /**
     *
     */
    handleExistingSessionSubmit: function (event) {
        event.preventDefault();

        /*var newToken = ($(event.target).find('input[name=newToken]')[0].value);
        console.log("token is? : ", newToken);
        ServerInterface.sendTokenToServer(newToken)
            .then(function onResolve() {
                //new session ok
                StateActions.setStateToken(newToken);
            }, function onReject(err) {
                //new session not ok
                console.log("Could not make new session");
            });*/
    },

    handleUserLoginClose: function () {
        this.setState({showUserLogin: false});
    },

    handleUserLoginShow: function () {
        this.setState({showUserLogin: true});
    },

    handleUserCreateClose: function () {
        this.setState({showUserCreate:false});
    },

    handleUserCreateShow: function () {
        this.setState({showUserCreate:true});
    },

    handleLoginSuccess: function () {
        this.setState({showUserLogin:false,showUserCreate: false ,loggedIn: true});
        //do other login things
    },

    handleLogoutSuccess: function () {
        this.setState({loggedIn: false});
    },

    handleLogout: function (event) {
        event.preventDefault();
        ServerInterface.sendLogout().then(function onResolve(result) {
            StateActions.logoutSuccess();
        }, function onReject (err) {
            console.log("FAILED",err);
        });
    },

    renderLoginArea: function () {
        if (this.state.loggedIn) {
            return (<div className="loginFormWrapper">
                        Logged in
                        <br/>
                        <ReactBootstrap.Button onClick={this.handleLogout}>Logout</ReactBootstrap.Button>
                    </div>);
        } else {
            return (<div className="loginFormWrapper">
                            <ReactBootstrap.Button onClick={this.handleUserCreateShow}>Create new account</ReactBootstrap.Button>
                            <ReactBootstrap.Modal show={this.state.showUserCreate} onHide={this.handleUserCreateClose} dialogClassName="accountModal">
                                <ReactBootstrap.Modal.Header closeButton>
                                    <ReactBootstrap.Modal.Title>Create a new account</ReactBootstrap.Modal.Title>
                                </ReactBootstrap.Modal.Header>
                                <ReactBootstrap.Modal.Body>
                                    <CreateUserForm />
                                </ReactBootstrap.Modal.Body>
                                <ReactBootstrap.Modal.Footer>
                                    <ReactBootstrap.Button onClick={this.handleUserCreateClose}>Close</ReactBootstrap.Button>
                                </ReactBootstrap.Modal.Footer>
                            </ReactBootstrap.Modal>
                            <h5><span>OR</span></h5>
                            <ReactBootstrap.Button onClick={this.handleUserLoginShow}>Sign in to existing account</ReactBootstrap.Button>
                            <ReactBootstrap.Modal show={this.state.showUserLogin} onHide={this.handleUserLoginClose} dialogClassName="accountModal">
                                <ReactBootstrap.Modal.Header closeButton>
                                    <ReactBootstrap.Modal.Title>Login to your account</ReactBootstrap.Modal.Title>
                                </ReactBootstrap.Modal.Header>
                                <ReactBootstrap.Modal.Body>
                                    <LoginForm />
                                </ReactBootstrap.Modal.Body>
                                <ReactBootstrap.Modal.Footer>
                                    <ReactBootstrap.Button onClick={this.handleUserLoginClose}>Close</ReactBootstrap.Button>
                                </ReactBootstrap.Modal.Footer>
                            </ReactBootstrap.Modal>
                        </div>);
        }
    },

    /**
     * Renders a view for this component
     */
    render: function () {
        return (
            <div className="fileUploadWindowWrapper">
                <div className="formCollectWrapper">
                    <div className="loginWrapper">
                        <h3>LOGIN OR CREATE ACCOUNT</h3>
                        {this.renderLoginArea()}
                    </div>
                    <div className="uploadWrapper">
                        <h3>MANAGE FILES</h3>
                        <div className="fileUploadWrapper">
                            <form id="stockFileUploadForm" enctype="multipart/form-data" onSubmit={this.handleStockSubmit}>
                                <div className="form-group">
                                    <div className="form-group fileUpload">
                                        <label>Upload Stock file</label>
                                        <input type="file" name="stockFile" accept="text/csv" required/>
                                        <br/>
                                        <p className="serverStockFile">Current file: {this.props.serverFiles.stockFile}</p>
                                    </div>
                                    <div className="form-group stockFileUploadSubmit">
                                        <input className="btn btn-default" type="submit" name="submitFiles" value="Upload"/>
                                        <img src="../../images/loading_gif.gif" className="loader_gif"/>
                                    </div>
                                </div>
                            </form>
                            <form id="eventFileUploadForm" enctype="multipart/form-data" onSubmit={this.handleEventSubmit}>
                                <div className="form-group">
                                    <div className="form-group fileUpload">
                                        <label>Upload Event file</label>
                                        <input type="file" name="eventFile" accept="text/csv" required/>
                                        <br/>
                                        <p className="serverEventFile">Current File: {this.props.serverFiles.eventFile}</p>
                                    </div>
                                    <div className="form-group eventFileUploadSubmit">
                                        <input className="btn btn-default" type="submit" name="submitFiles" value="Upload"/>
                                        <img src="../../images/loading_gif.gif" className="loader_gif"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="fileFormDescriptorWrapper">
                            <div className="stockDescriptionWrapper">
                                Stock file: A comma-separated file containing entries about stock trades over a period of time.
                                Example: <a href="/defaultInputFiles/farmingCompanies.csv"> farmingCompanies.csv </a>
                            </div>
                            <div className="eventDescriptionWrapper">
                                Event file: A comma-separated file containing entries about events that affect particular stocks.
                                Example: <a href="/defaultInputFiles/eventFile.csv">eventFile.csv </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = FileUploadWindow;

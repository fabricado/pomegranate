var React = require('react');
var DataStore = require('../stores/DataStore');
var moment = require('moment');

var AnalysisSidebar = React.createClass({
    getInitialState: function () {
        return {
        portfolio: DataStore.getPortfolioStocks()
        };
    },

    renderPortfolioForm: function () {
        //console.log(this.state.portfolio);
        return this.state.portfolio.map(this.renderPortfolioEntry)
    },

    onAmountChange: function (event) {
        var state = this.state;
        state.stocks[event.target.name.split('_')[1]].amount = event.target.value;
        // var slider = document.getElementById('slider_' + event.target.name.split('_')[1]);
        // slider.noUiSlider.set([null,event.target.value]);
        this.setState(state);
    },

    renderPortfolioEntry: function (stock, i) {
        return (<div id={i} >
          {stock.ric} {' '}
          {moment(stock.ric.buyInDate).format("DD-MM-YYYY")}<br/>
            {'$'}{stock.buyInPrice}<br/>
          <input type="number"
                   name={"amount_"+i}
                   onChange={this.onAmountChange}
                   value={stock.amount}
                   step="any"
                 style={{marginRight:'5px'}}
                />
          </div>);
    },

    handleSubmit: function (event) {
        event.preventDefault();
        this.props.reAnalyse(this.state.portfolio);
    },

    render: function () {
        return (
            <div className="analysisSidebarWrapper">
                Sidebar
                <br/>
                {this.renderPortfolioForm()}
                <form onSubmit={this.handleSubmit}>
                  <input className="btn btn-default" type="submit" name="filterFormSubmit" value="Calculate Impact"
                         style={{color:'#ffd100','backgroundColor':'#3b3b3b'}}/>
                </form>
            </div>
        )
    }
});

module.exports = AnalysisSidebar;

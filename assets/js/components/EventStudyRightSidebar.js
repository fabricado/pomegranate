/**
 * React is Facebook's V framework that we are using
 *
 * DataStore is a flux store that holds data about event + stock files
 *
 * PomegranateConstants are predefined constants for the pomegranate app
 */
var React = require('react');
var PomegranateConstants = require('../constants/PomegranateConstants');
var EventSelector = require('./EventSelector');
var FilterVariableForm = require('./FilterVariableForm');
var EventStudyRightSidebarTabs = require('./EventStudyRightSidebarTabs');
var EventList = require('./EventList');
var DataStore = require('../stores/DataStore');
var DataActions = require('../actions/DataActions');
var SingleEventFilter = require('./SingleEventFilter');
var ServerInterface = require('../modules/ServerInterface');

var EventStudyRightSidebar = React.createClass({
    displayName: "EventStudyRightSidebar",

    /**
     * Property validation and requirements
     */
    propTypes: {
        eventVariables: React.PropTypes.object.isRequired,
        crData: React.PropTypes.object.isRequired
    },

    /**
     * Sets the initial state of the component on creation
     */
    getInitialState: function () {
        return {
            eventVariables: this.props.eventVariables,
            cumulativeReturns: {
                data: {
                    eventData: []
                }
            },
            currentTab: PomegranateConstants.TAB_MULTI_EVENT_STUDY,
            events: []
        };
    },

    /**
     * Called as soon as the component has mounted for the first time
     */
    componentDidMount: function () {
        DataStore.registerEventChangeListener(this.onEventsChange);
        DataActions.getEvents();
    },

    onEventsChange: function () {
        this.setState({events: DataStore.getEvents()});
    },

    onTabChange: function (newTab) {
        this.setState({currentTab: newTab});
    },

    /**
     * Run when component receives new properties
     * NOT ON FIRST RENDER
     * @param nextProps
     */
    componentWillReceiveProps: function (nextProps) {
        this.setState({cumulativeReturns: nextProps.crData, eventVariables: nextProps.eventVariables});
    },

    /**
     * Called before the component is destroyed
     */
    componentWillUnmount: function () {
        DataStore.removeEventChangeListener(this.onEventsChange);
    },

    renderSingleEvent: function () {
        return (
            <div>
                <SingleEventFilter />
                <EventList events={this.state.events} onMainSetChange={this.onSingleEventSelect} />
            </div>
        );
    },

    renderMultiEvent: function () {
        return (
            <div>
                <EventSelector events={this.state.cumulativeReturns.data.eventData}/>
                <FilterVariableForm eventVariables={this.state.eventVariables}/>
            </div>
        );
    },

    onSingleEventSelect: function (ric, date) {
        var found = false;
        var index = -1;
        for (var i = 0; i < this.state.events.length; ++i) {
            if (this.state.events[i].ric == ric && this.state.events[i].date == date) {
                index = i;
            }
        }
        ServerInterface.makeFakeEvent(this.state.events[index],DataStore.getUserRICs()).then(function onResolve(result) {
            console.log("EVENT FILE MADE", result);
        }, function onReject(err) {
            console.log(err);
        });
    },

    /**
     * Renders a view for this component
     */
    render: function () {
        var innerComponent;
        switch (this.state.currentTab) {
            case PomegranateConstants.TAB_SINGLE_EVENT_STUDY:
                innerComponent = this.renderSingleEvent();
                break;
            case PomegranateConstants.TAB_MULTI_EVENT_STUDY:
                innerComponent = this.renderMultiEvent();
                break;
        }

        return (
            <div className="eventStudyRightSidebarWrapper">
                <EventStudyRightSidebarTabs onTabChange={this.onTabChange} />
                {innerComponent}
            </div>
        );
    }
});

module.exports = EventStudyRightSidebar;

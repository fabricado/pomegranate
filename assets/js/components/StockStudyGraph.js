var React = require('react');
var ReactBootstrap = require('react-bootstrap');
var ChartCreator = require('../modules/ChartCreator');
var DataStore = require('../stores/DataStore');

var StockStudyGraph = React.createClass({
    displayName: 'StockStudyGraph',

    getInitialState: function () {
        return {
            data: {},
        };
    },

    componentDidMount: function () {
        //ChartCreator.makeLifetimeChart('stockStudyChartDiv',this.state.stockData);
        DataStore.registerServerStockLifetimeListener(this.handleServerLifetime);
        DataStore.registerUserStockLifetimeListener(this.handleUserLifetime);
    },

    componentWillUnmount: function () {
        DataStore.removeServerStockLifetimeListener(this.handleServerLifetime);
        DataStore.removeUserStockLifetimeListener(this.handleUserLifetime);
    },

    handleServerLifetime: function () {
        console.log("DUPDATE");
        console.log(this.props.RIC);
        console.log(DataStore.getServerStockLifetimeData([this.props.RIC]));
        ChartCreator.makeLifetimeChart('lifeChartDiv',DataStore.getServerStockLifetimeData([this.props.RIC]),[],{ric:"",date:""},  this.props.clickHandler);
        //ChartCreator.makeLifetimeChart('stockStudyChartDiv',this.state.stockData);
    },

    handleUserLifetime: function () {
        console.log("DUPDATE");
        console.log(this.props.RIC);
        console.log(DataStore.getStockLifetimeData([this.props.RIC]));
        ChartCreator.makeLifetimeChart('lifeChartDiv',DataStore.getStockLifetimeData([this.props.RIC]),[],{ric:"",date:""}, this.props.clickHandler);
        //ChartCreator.makeLifetimeChart('stockStudyChartDiv',this.state.stockData);
    },

    componentWillReceiveProps: function (nextProps) {
        //ChartCreator.makeLifetimeChart('lifeChartDiv',DataStore.getServerStockLifetimeData([this.props.RIC]),[],{ric:"",date:""});
    },

    componentDidUpdate: function () {
    },

    render: function () {
        return (
            <div className="lifetimeGraphWrapper">
                <div id="lifeChartDiv" style={{width:'100%',height:'100%'}}></div>
            </div>
        );
    }
});

module.exports = StockStudyGraph;

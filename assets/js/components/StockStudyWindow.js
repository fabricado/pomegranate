/**
 * React is Facebook's V framework that we are using
 */
var React = require('react');
var ReactBootstrap = require('react-bootstrap');

var DataStore = require('../stores/DataStore');
var DataActions = require('../actions/DataActions');

var StockSelector = require('./StockSelector');
var CompanyProfile = require('./CompanyProfile');
var StockStudyGraph = require('./StockStudyGraph');
var AddStockForm = require('./AddStockForm');

var StockStudyWindow = React.createClass({
    displayName: "StockStudyWindow",

    /**
     * Sets the initial state of the component on creation
     */
    getInitialState: function () {
        return {
            cumulativeReturns: {},
            eventVariables: this.props.eventVariables,
            chosenRIC: null,
            showBuyinForm: false,
            providedByUser: false
        }
    },

    onCumulativeReturnChange: function () {
        this.setState({cumulativeReturns: DataStore.getCumulativeReturnData()});
    },

    /**
     * Called as soon as the component has mounted for the first time
     */
    componentDidMount: function () {
        DataStore.registerCumulativeReturnChangeListener(this.onCumulativeReturnChange);
    },

    /**
     * Called before the component is destroyed
     */
    componentWillUnmount: function () {
        DataStore.removeCumulativeReturnChangeListener(this.onCumulativeReturnChange);
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState({eventVariables:nextProps.eventVariables});
    },

    onGraphClick: function (event) {
        this.setState({showBuyinForm: true, date: AmCharts.myCurrentPosition, price: AmCharts.myCurrentValue});
        //console.log(AmCharts.myCurrentPosition);
        //console.log(AmCharts.myCurrentValue);
        //console.log(.chart.dataProvider[event.index].date);
    },

    handleStockSelect: function (RIC, serverSupplied) {
        if (serverSupplied) {
            this.setState({chosenRIC: RIC, providedByUser: false});
            DataActions.getCompanyProfile(RIC); //triggers getting the stock lifetime as well
        } else {
            this.setState({chosenRIC: RIC, providedByUser: true});
            DataActions.getUserStockLifetime(RIC);
        }
    },

    handleCloseBuyinForm: function () {
        this.setState({showBuyinForm: false});
    },

    handleBuyStock: function (amount) {
        this.setState({showBuyinForm: false});
        //add stock to portfolio
    },

    renderStockBuyin: function () {
        return (
                <ReactBootstrap.Modal show={this.state.showBuyinForm} onHide={this.handleCloseBuyinForm} dialogClassName="stockBuyinModal">
                    <ReactBootstrap.Modal.Header closeButton>
                        <ReactBootstrap.Modal.Title>Add to Portfolio</ReactBootstrap.Modal.Title>
                    </ReactBootstrap.Modal.Header>
                    <ReactBootstrap.Modal.Body>
                        <AddStockForm date={this.state.date} price={this.state.price} RIC={this.state.chosenRIC} providedByUser={this.state.providedByUser} onAddStock={this.handleBuyStock}/>
                    </ReactBootstrap.Modal.Body>
                    <ReactBootstrap.Modal.Footer>
                        <ReactBootstrap.Button onClick={this.handleCloseBuyinForm}>Close</ReactBootstrap.Button>
                    </ReactBootstrap.Modal.Footer>
                </ReactBootstrap.Modal>
        );
    },

    /**
     * Renders a view for this component
     */
    render: function () {
        return (
            <div className="lifetimeWindowWrapper">
                {this.renderStockBuyin()}
                <StockStudyGraph RIC={this.state.chosenRIC} clickHandler={this.onGraphClick}/>
                <div className="tableWrapper">
                    <StockSelector onStockSelect={this.handleStockSelect}/>
                    <CompanyProfile />
                </div>
            </div>
        );
    }
});

module.exports = StockStudyWindow;

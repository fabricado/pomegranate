/**
 * React is Facebook's V framework that we are using
 *
 * EventStudyRightSidebar is the react element that renders the right sidebar
 * It will contain a list of event variable filters that the user can interact with
 *
 * EventStudyGraph is the react element that renders the graph for the event study section
 */
var React = require('react');
var EventStudyRightSidebar = require('./EventStudyRightSidebar');
var EventStudyGraph = require('./EventStudyGraph');
var DataStore = require('../stores/DataStore');
var DataActions = require('../actions/DataActions');

var EventStudyWindow = React.createClass({
    displayName: "EventStudyWindow",

    /**
     * Property validation and requirements
     */
    propTypes: {
        eventVariables: React.PropTypes.object.isRequired
    },

    /**
     * Sets the initial state of the component on creation
     */
    getInitialState: function () {
        return {
            cumulativeReturns: {},
            eventVariables: this.props.eventVariables
        }
    },

    onCumulativeReturnChange: function () {
        this.setState({cumulativeReturns: DataStore.getCumulativeReturnData()});
    },

    /**
     * Called as soon as the component has mounted for the first time
     */
    componentDidMount: function () {
        DataStore.registerCumulativeReturnChangeListener(this.onCumulativeReturnChange);
    },

    /**
     * Called before the component is destroyed
     */
    componentWillUnmount: function () {
        DataStore.removeCumulativeReturnChangeListener(this.onCumulativeReturnChange);
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState({eventVariables:nextProps.eventVariables});
    },

    /**
     * Renders a view for this component
     */
    render: function () {
        return (
            <div className="eventStudyWindowWrapper">
                <EventStudyGraph crData={this.state.cumulativeReturns}/>
                <EventStudyRightSidebar crData={this.state.cumulativeReturns} eventVariables={this.props.eventVariables}/>
            </div>
        );
    }
});

module.exports = EventStudyWindow;

var React = require('react');
var ReactBootstrap = require('react-bootstrap');
var DataActions = require('../actions/DataActions')

var FactorStudySidebar = React.createClass({

    displayName: "FactorStudySidebar",

    getInitialState: function () {
        return {
            studyTypes: [
                "Interest Rate",
                "Australian Exchange Rate"
            ],
            selectedStudyType: "Interest Rate",

            "ERCountries": ["British Pound", "Chinese Renminbi", "Euro", "Japanese Yen", "South Korean Won",  "US Dollar"],
            "ERSelectedCountry": "British Pound",

            "IRCountries": ["Australia", "China", "United States of America"],
            "IRSelectedCountry": "Australia"
        }
    },

    renderMoreRefinements: function () {
       switch (this.state.selectedStudyType) {
       case "Australian Exchange Rate":
            return (
                <div>
                    <h4>Target Currency</h4>
                    <ReactBootstrap.DropdownButton title={this.state.ERSelectedCountry} id="CurrencyDropdown" onSelect={this.handleCurrencySelect}>
                        {this.state.ERCountries.map(this.renderCurrencySelector)}
                    </ReactBootstrap.DropdownButton>
                <br/>
                </div>
            );
        case "Interest Rate":
            return (
                <div>
                    <h4>Target Country</h4>
                    <ReactBootstrap.DropdownButton title={this.state.IRSelectedCountry} id="CountryDropdown" onSelect={this.handleCountrySelect}>
                        {this.state.IRCountries.map(this.renderCountrySelector)}
                    </ReactBootstrap.DropdownButton>
                <br/>
                </div>
            );
        default:
            return (<br />);
        }
    },

    renderStudyTypeList: function (val, index) {
        return (
            <ReactBootstrap.MenuItem onClick={this.handleStudyTypeSelect} eventKey={val}>{val}</ReactBootstrap.MenuItem>
        );
    },

    renderCurrencySelector: function (val, index) {
        return (
            <ReactBootstrap.MenuItem onClick={this.handleCurrencySelect} eventKey={val}>{val}</ReactBootstrap.MenuItem>
        );
    },

    renderCountrySelector: function (val, index) {
        return (
            <ReactBootstrap.MenuItem onClick={this.handleCountrySelect} eventKey={val}>{val}</ReactBootstrap.MenuItem>
        );
    },

    handleStudyTypeSelect: function (eventKey, event) {
        console.log(eventKey);
        this.setState({
            selectedStudyType: eventKey
        });
    },

    handleCountrySelect: function (eventKey, event) {
        //console.log(eventKey);
        var currst = this.state;
        currst.IRSelectedCountry = eventKey;
        this.setState(currst);

        switch (eventKey) {
            case "Australia":
                DataActions.getInterestRateData(eventKey);
                break;
            case "China":
                DataActions.getInterestRateData(eventKey);
                break;
            case "United States of America":
                console.log("USA USA USA");
                DataActions.getInterestRateData("UnitedStatesOfAmerica");
                break;
            default:
                return;
        }
    },

    handleCurrencySelect: function (eventKey, event) {
        //console.log(eventKey);
        var currst = this.state;
        currst.ERSelectedCountry = eventKey;
        this.setState(currst);


//["British Pound", "Chinese Renminbi", "Euro", "Japanese Yen", "South Korean Won",  "US Dollar"],
        switch (eventKey) {
            case "British Pound":
                DataActions.getExchangeRateData("UKPS");
                break;
            case "Chinese Renminbi":
                DataActions.getExchangeRateData("CR");
                break;
            case "Euro":
                DataActions.getExchangeRateData("EUR");
                break;
            case "Japanese Yen":
                DataActions.getExchangeRateData("JY");
                break;
            case "South Korean Won":
                DataActions.getExchangeRateData("SKW");
                break;
            default:
                return;
        }
    },

    render: function () {
        return (
            <div className="factorStudySidebarWrapper">
                <h3>FACTOR STUDY</h3>
                <h4>Select study type</h4>
                <ReactBootstrap.DropdownButton title={this.state.selectedStudyType} id="studyTypedropdown" onSelect={this.handleStudyTypeSelect}>
                    {this.state.studyTypes.map(this.renderStudyTypeList)}
                </ReactBootstrap.DropdownButton>
                {this.renderMoreRefinements()}
            </div>
        )
    }
});

module.exports = FactorStudySidebar;

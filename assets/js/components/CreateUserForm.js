var React = require('react');
var ReactBootstrap = require('react-bootstrap');
var ServerInterface = require('../modules/ServerInterface');
var StateActions = require('../actions/StateActions');

var CreateUserForm = React.createClass({
    displayName:'CreateUserForm',

    getInitialState: function () {
        return {
            username: '',
            email: '',
            password:'',
            passwordConfirm: ''
        };
    },

    getPassConfirmValidationState() {
        if (this.state.passwordConfirm !== '') {
            if (this.state.password === this.state.passwordConfirm) {
                return 'success';
            } else {
                return 'error';
            }
        }
    },

    handleUsernameChange: function (event) {
        event.preventDefault();
        this.setState({username: event.target.value});
    },

    handleEmailChange: function (event) {
        event.preventDefault();
        this.setState({email: event.target.value});
    },

    handlePasswordChange: function (event) {
        event.preventDefault();
        this.setState({password: event.target.value});
    },

    handleConfirmPasswordChange: function (event) {
        event.preventDefault();
        this.setState({passwordConfirm: event.target.value});
    },

    handleSubmit: function (event) {
        event.preventDefault();
        ServerInterface.sendNewUser(this.state.username,this.state.email,this.state.password).then(function onResolve (result) {
            ServerInterface.sendLogin(this.state.email,this.state.password).then(function onResolve(result) {
                console.log(result);
                StateActions.loginSuccess(result.username);
            }.bind(this), function onReject (err) {
                console.log("FAILED",err);
            }.bind(this));
        }.bind(this), function onReject (err) {
            console.log(err);
        });
    },

    render: function () {
        return (
            <form onSubmit={this.handleSubmit}>
                <ReactBootstrap.FormGroup>
                    <ReactBootstrap.ControlLabel>Username:</ReactBootstrap.ControlLabel>
                    <ReactBootstrap.FormControl
                        type="text"
                        value={this.state.username}
                        placeholder="Enter username"
                        onChange={this.handleUsernameChange}
                    />
                </ReactBootstrap.FormGroup>
                <ReactBootstrap.FormGroup>
                    <ReactBootstrap.ControlLabel>Email:</ReactBootstrap.ControlLabel>
                    <ReactBootstrap.FormControl
                        type="email"
                        value={this.state.email}
                        placeholder="Enter email"
                        onChange={this.handleEmailChange}
                    />
                </ReactBootstrap.FormGroup>
                <ReactBootstrap.FormGroup>
                    <ReactBootstrap.ControlLabel>Password:</ReactBootstrap.ControlLabel>
                    <ReactBootstrap.FormControl
                        type="password"
                        value={this.state.password}
                        placeholder="Enter password"
                        onChange={this.handlePasswordChange}
                    />
                </ReactBootstrap.FormGroup>
                <ReactBootstrap.FormGroup validationState={this.getPassConfirmValidationState()}>
                    <ReactBootstrap.ControlLabel>Confirm Password:</ReactBootstrap.ControlLabel>
                    <ReactBootstrap.FormControl
                        type="password"
                        value={this.state.passwordConfirm}
                        placeholder="Confirm password"
                        onChange={this.handleConfirmPasswordChange}
                    />
                </ReactBootstrap.FormGroup>
                <ReactBootstrap.FormGroup>
                    <ReactBootstrap.Button type="submit">
                        Submit
                    </ReactBootstrap.Button>
                </ReactBootstrap.FormGroup>
            </form>
        );
    }
});

module.exports = CreateUserForm;

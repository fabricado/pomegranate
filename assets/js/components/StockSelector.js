/**
 * React is Facebook's V framework that we are using
 *
 * DataStore is a flux store that holds data about event + stock files
 *
 * PomegranateConstants are predefined constants for the pomegranate app
 */
var React = require('react');
var RSelect = require('react-virtualized-select');
var PomegranateConstants = require('../constants/PomegranateConstants');
var EventSelector = require('./EventSelector');
var FilterVariableForm = require('./FilterVariableForm');
var EventStudyRightSidebarTabs = require('./EventStudyRightSidebarTabs');
var EventList = require('./EventList');
var DataStore = require('../stores/DataStore');
var DataActions = require('../actions/DataActions');
var SingleEventFilter = require('./SingleEventFilter');
var ServerInterface = require('../modules/ServerInterface');

var StockSelector = React.createClass({
    displayName: "StockSelector",

    /**
     * Sets the initial state of the component on creation
     */
    getInitialState: function () {
        return {
            serverRICs: this.makeOptions(DataStore.getServerRICs()),
            userRICs: this.makeOptions(DataStore.getUserRICs()),
            chosenRIC: false,
            chosenServerRIC: 'Select a RIC',
            chosenUserRIC: 'Select a RIC'
        };
    },

    makeOptions: function (RICs) {
        var options = [];
        for (var i in RICs) {
            options.push({value: RICs[i], label: RICs[i]});
        }
        return options;
    },

    /**
     * Called as soon as the component has mounted for the first time
     */
    componentDidMount: function () {
    },

    onUserRICChoice: function (val) {
        this.setState({chosenServerRIC: 'Select a RIC', chosenUserRIC: val});
        this.props.onStockSelect(val.value, false);
    },

    onServerRICChoice: function (val) {
        this.setState({chosenServerRIC: val, chosenUserRIC: 'Select a RIC'});
        this.props.onStockSelect(val.value, true);
    },

    /**
     * Renders a view for this component
     */
    render: function () {
        return (
            <div className="stockSelectorWrapper">
                <h5>My Supplied Stocks</h5>
                <RSelect.default options={this.state.userRICs} onChange={this.onUserRICChoice} value={this.state.chosenUserRIC}/>
                <br/>
                <h5>Server Supplied Stocks</h5>
                <RSelect.default options={this.state.serverRICs} onChange={this.onServerRICChoice} value={this.state.chosenServerRIC}/>
            </div>
        );
    }
});

module.exports = StockSelector;

/**
 * React is Facebook's V framework that we are using
 *
 * DataStore is a flux store that holds data about event + stock files
 *
 * PomegranateConstants are predefined constants for the pomegranate app
 */
var React = require('react');
var DataStore = require('../stores/DataStore');
var PomegranateConstants = require('../constants/PomegranateConstants');
var ChartCreator = require('../modules/ChartCreator');

var LifetimeGraph = React.createClass({
    displayName: "LifetimeGraph",

    /**
     * Property validation and requirements
     */
    propTypes: {
        stockLifetimeData: React.PropTypes.object.isRequired,
        events: React.PropTypes.array.isRequired,
        mainSet: React.PropTypes.object
    },

    /**
     * Sets the initial state of the component on creation
     */
    getInitialState: function () {
        return {};
    },

    /**
     * Called as soon as the component has mounted for the first time
     */
    componentDidMount: function () {
    },

    componentWillReceiveProps: function (nextProps) {
        ChartCreator.makeLifetimeChart("lifeChartDiv",nextProps.stockLifetimeData, nextProps.events, nextProps.mainSet);
    },

    /**
     * Called before the component is destroyed
     */
    componentWillUnmount: function () {

    },

    /**
     * Renders a view for this component
     */
    render: function () {
        return (
            <div className="lifetimeGraphWrapper">
                <div id="lifeChartDiv" style={{width: '99%', height: '99%'}}></div>
            </div>
        );
    }
});

module.exports = LifetimeGraph;

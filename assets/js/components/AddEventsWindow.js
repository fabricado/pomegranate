/**
 * React is Facebook's V framework that we are using
 *
 * LifetimeRightSidebar is the react element that renders the right sidebar
 * It will contain a list of events which the user can interact with
 *
 * LifetimeGraph is the react element that renders the graph for the lifetime section
 */
var React = require('react');
var LifetimeRightSidebar = require('./LifetimeRightSidebar');
var LifetimeGraph = require('./LifetimeGraph');
var DataStore = require('../stores/DataStore');
var DataActions = require('../actions/DataActions');
var ServerInterface = require('../modules/ServerInterface');
var $ = require('jquery');

var AddEventsWindow = React.createClass({
    displayName: "AddEventsWindow",

    /**
     * Property validation and requirements
     */
    propTypes: {
    },

    /**
     * Sets the inital state of the component on creation
     */
    getInitialState: function () {
        return {
            filedata: ""
        };
    },

    /**
     * Called as soon as the component has mounted for the first time
     */
    componentDidMount: function () {
    },

    /**
     * Called before the component is destroyed
     */
    componentWillUnmount: function () {
    },

    handleSubmit: function (event) {
        var comp = this;
        event.preventDefault();
        console.log(this.state.filedata);
        ServerInterface.writeToEventFile(this.state.filedata).then(function onResolve () {
            DataActions.initialise();
            DataActions.getEvents();
            comp.handleGetRaw();
        }, function onReject () {
            console.log("LOL FAIL");
        });
    },

    handleGetRaw: function () {
        var comp = this;
        //console.log(this);
        ServerInterface.getRawEventData().then(function onResolve (result) {
            console.log(result);
            comp.setState({filedata: result});
            //$('.form-control').val(result);
        }, function onReject (error) {
            console.log("RAW EVENT FAIL");
        });
    },

    onTextChange: function (event) {
        this.setState({filedata: event.target.value});
    },

    /**
     * Renders a view for this component
     */
    render: function () {
        console.log(this.state);
        return (
            <div className="addEventsWindowWrapper">
                <div className="eventsFormWrapper">
                    <h3>MANAGE EVENT FILE</h3>
                    <div className="formArea">
                        <button type="submit" className="btn btn-default" onClick={this.handleGetRaw}>Get raw file data</button>
                        <form className="eventSubmit" onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <textarea className="form-control" rows="10" value={this.state.filedata}
                                          onChange={this.onTextChange}></textarea>
                                <button type="submit" className="btn btn-default">Save to file</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = AddEventsWindow;

var React = require('react');
var ReactBootstrap = require('react-bootstrap');
var DataActions = require('../actions/DataActions');

var AddStockForm = React.createClass({
    displayName:'AddStockForm',

    getInitialState: function () {
        return {
            amount: 0
        };
    },

    handleAmountChange: function (event) {
        event.preventDefault();
        this.setState({amount: event.target.value});
    },

    handleSubmit: function (event) {
        event.preventDefault();
        //add stock to portfolio/send to backend
        DataActions.addStockToPortfolio({
            ric: this.props.RIC,
            buyInDate: this.props.date,
            buyInPrice: this.props.price,
            amount: this.state.amount,
            providedByUser: this.props.providedByUser
        });
        this.props.onAddStock();
    },

    render: function () {
        return (
            <form onSubmit={this.handleSubmit}>
                <b>RIC: </b>{this.props.RIC}
                <br/>
                <b>Date: </b>{this.props.date.toString()}
                <br/>
                <b>Price per Share: </b>{this.props.price}
                <br/>
                <ReactBootstrap.FormGroup>
                    <ReactBootstrap.ControlLabel>Amount:</ReactBootstrap.ControlLabel>
                    <ReactBootstrap.FormControl
                        type="number"
                        value={this.state.amount}
                        onChange={this.handleAmountChange}
                    />
                </ReactBootstrap.FormGroup>
                <b>Cost: </b>{this.state.amount * this.props.price}
                <ReactBootstrap.FormGroup>
                    <ReactBootstrap.Button type="submit">
                        Submit
                    </ReactBootstrap.Button>
                </ReactBootstrap.FormGroup>
            </form>
        );
    }
});

module.exports = AddStockForm;

/**
 * React is Facebook's V framework that we are using
 *
 * LifetimeRightSidebar is the react element that renders the right sidebar
 * It will contain a list of events which the user can interact with
 *
 * LifetimeGraph is the react element that renders the graph for the lifetime section
 */
var React = require('react');
var ReactBootstrap = require('react-bootstrap');
var LifetimeRightSidebar = require('./LifetimeRightSidebar');
var LifetimeGraph = require('./LifetimeGraph');
var DataStore = require('../stores/DataStore');
var DataActions = require('../actions/DataActions');

var CustomEventForm = React.createClass({
    displayName: "CustomEventForm",

    /**
     * Property validation and requirements
     */
    propTypes: {
        RICs: React.PropTypes.array.isRequired
    },

    /**
     * Sets the inital state of the component on creation
     */
    getInitialState: function () {
        //console.log("getinit: ", this.state);
        var varFilterVals = {};
        console.log(DataStore.getEventVariables());
        _.forOwn(DataStore.getEventVariables(), function (value, key) {
            varFilterVals[key] = {};
            varFilterVals[key]['value'] = 0;
        });

        return {
            newEvent: {RIC:"Select RIC",date:"",variables:varFilterVals}
        };
    },

    /**
     * Called as soon as the component has mounted for the first time
     */
    componentDidMount: function () {
        var s = this.state;
        s.newEvent.date = this.props.date;
        this.setState(s);
    },

    /**
     * Called before the component is destroyed
     */
    componentWillUnmount: function () {
    },

    componentWillReceiveProps: function (nextProps) {
        var s = this.state;
        s.newEvent.date = nextProps.date;
        this.setState(s);
    },

    renderCustomEventForm: function () {
        console.log("Rendering custom form: ", this.props);
        console.log(this.state.newEvent);
        return (<form id="addEvent" onSubmit={this.handleAddEventClick}>
                    <div className="form-group newEventSubmit">
                        {this.renderRICDropdown()}
                        {this.props.date}
                        {_.keys(DataStore.getEventVariables()).map(this.renderVariableInputs)}
                        <input className="btn btn-default" type="submit" value="Add Event" onSubmit={this.handleAddEventClick}/>
                    </div>
                </form>);
    },

    renderVariableInputs: function (variable, i) {
        return (
            <div key={i} class="variableInputSection">
                <h>{variable}</h><br></br>
                <input type="number"
                   name={"varVal_"+variable}
                   onChange={this.onValueChange}
                   value={this.state.newEvent.variables[variable].value}
                   step="any"
                />
            </div>
        );
    },

    onValueChange: function (event) {
        var state = this.state;
        state.newEvent.variables[event.target.name.split('_')[1]].value = event.target.value;
        // var slider = document.getElementById('slider_' + event.target.name.split('_')[1]);
        // slider.noUiSlider.set([event.target.value,null]);
        this.setState(state);
        //console.log("lower bound change on ", event.target.name);
        //console.log(this.state);
    },

    selectRIC: function (event) {
        //event is index
        var s = this.state;
        s.newEvent.ric = DataStore.getRICs()[event];
        this.setState(s);
    },

    renderRICDropdown: function () {
        return (<ReactBootstrap.DropdownButton title={this.state.newEvent.ric} id="selectRIC" onSelect={this.selectRIC}>
                    {_.keys(DataStore.getRICs()).map(this.renderDropDownElement)}
                </ReactBootstrap.DropdownButton>);
    },

    renderDropDownElement: function (RIC, i) {
        return (<ReactBootstrap.MenuItem eventKey={RIC}>{DataStore.getRICs()[RIC]}</ReactBootstrap.MenuItem>)
    },

    formatArticle: function (article, i) {
        if (i == this.state.selectedArticle) {
            return (<div className="newsArticle">
                        <h1>{article.headline}</h1>
                        {this.customEventForm()}
                        <br/>
                        {article.body}
                    </div>);
        } else {
            return (<div className="newsArticle">
                        <h1>{article.headline}</h1>
                        <form id="makeEvent" name={i} onSubmit={this.handleAddEventClick}>
                            <div className="form-group makeEventSubmit">
                                <input className="btn btn-default" type="submit" name={article} value="Make Event"/>
                            </div>
                        </form>
                        <br/>
                        {article.body}
                    </div>);
        }
    },

    handleAddEventClick: function (event) {
        event.preventDefault();
        DataActions.addNewEvent(this.state.newEvent);
    },

    /**
     * Renders a view for this component
     */
    render: function () {
        //var formattedNews = 'beep';
        //console.log(this.state.news);
        return (<div>
                    {this.renderCustomEventForm()}
                </div>);
    }
});

module.exports = CustomEventForm;

/**
 * React is Facebook's V framework that we are using
 *
 * LifetimeRightSidebar is the react element that renders the right sidebar
 * It will contain a list of events which the user can interact with
 *
 * LifetimeGraph is the react element that renders the graph for the lifetime section
 */
var React = require('react');
var LifetimeRightSidebar = require('./LifetimeRightSidebar');
var LifetimeGraph = require('./LifetimeGraph');
var DataStore = require('../stores/DataStore');
var DataActions = require('../actions/DataActions');

var LifeTimeWindow = React.createClass({
    displayName: "LifeTimeWindow",

    /**
     * Property validation and requirements
     */
    propTypes: {
        RICs: React.PropTypes.array.isRequired
    },

    /**
     * Sets the inital state of the component on creation
     */
    getInitialState: function () {
        return {
            events: [],
            stockLifetimeData: {},
            mainSet: {ric: "", date: ""}
        };
    },

    /**
     * Called as soon as the component has mounted for the first time
     */
    componentDidMount: function () {
        DataStore.registerEventChangeListener(this.onEventsChange);
        DataStore.registerStockLifetimeDataChangeListener(this.onStockLifetimeDataChange);
        DataActions.getEvents();
        DataActions.getLifetimeData(this.props.RICs);
    },

    /**
     * Called before the component is destroyed
     */
    componentWillUnmount: function () {
        DataStore.removeEventChangeListener(this.onEventsChange);
        DataStore.removeStockLifetimeDataChangeListener(this.onStockLifetimeDataChange);
    },

    onEventsChange: function () {
        this.setState({events: DataStore.getEvents()});
    },

    onStockLifetimeDataChange: function () {
        this.setState({stockLifetimeData: DataStore.getStockLifetimeData(this.props.RICs)});
    },

    onMainSetChange: function (ric, date) {
        //console.log("ON MAIN SET CHANGE");
        //console.log(ric+date);
        this.setState({mainSet: {ric: ric, date: date}});
    },

    /**
     * Renders a view for this component
     */
    render: function () {
    //console.log("RENDERING LTW");
    //console.log(this.state.events);
        return (
            <div className="lifetimeWindowWrapper">
                <LifetimeGraph stockLifetimeData={this.state.stockLifetimeData} events={this.state.events} mainSet={this.state.mainSet}/>
                <LifetimeRightSidebar events={this.state.events} onMainSetChange={this.onMainSetChange}/>
            </div>
        );
    }
});

module.exports = LifeTimeWindow;

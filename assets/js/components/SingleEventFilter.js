var React = require('react');
var DataActions = require('../actions/DataActions');

var SingleEventFilter = React.createClass({
    displayName: 'SingleEventFilter',

    getInitialState: function () {
        return {
            lowerWindow: 0,
            upperWindow: 0,
            relState: false,
            studyType: "eventStudy"
        };
    },

    onLowerWindowChange: function (event) {
        event.preventDefault();
        this.setState({lowerWindow: event.target.value});
    },

    onUpperWindowChange: function (event) {
        event.preventDefault();
        this.setState({upperWindow: event.target.value});
    },

    handleSubmit: function (event) {
        event.preventDefault();
        var low = parseInt(this.state.lowerWindow);
        var high = parseInt(this.state.upperWindow);
        if (low != NaN && high != NaN) {
            DataActions.getSingleEventCumulativeReturnData({lowerBound:low,upperBound:high},
                this.state.studyType,this.state.relState);
        }
    },

    onRelDateChange: function (event) {
        switch (event.target.value) {
            case 'rel':
                this.setState({relState: true});
                break;
            case 'start':
                this.setState({relState: false});
                break;
        }
    },

    eventStudyTypeChange: function (event) {
        this.setState({studyType: event.target.value});
    },

    render: function () {
        return (
          <div className="singleEventFilterWrapper">
              <form onSubmit={this.handleSubmit} className="form-inline">
                  <label>Date Window</label><br/>
                  <div className="form-group">
                      <input type="number" name="lowerWindow" className="form-control"
                             onChange={this.onLowerWindowChange}
                             value={this.state.lowerWindow} required/>
                  </div>
                  <div className="form-group">
                      <input type="number" name="upperWindow" className="form-control"
                             onChange={this.onUpperWindowChange}
                             value={this.state.upperWindow} required/>
                  </div>
                  <div className="form-group" style={{display:"block"}}>
                      <select className="form-control" style={{display:"block"}}
                              value={this.state.studyType} onChange={this.eventStudyTypeChange}>
                          <option value="eventStudy">Cumulative Return Absolute</option>
                          <option value="eventStudyPercent">Cumulative Return Percentage</option>
                          <option value="vol">Volume</option>
                      </select>
                  </div>
                  <label><input type="radio" name="relOrStart" id="relDay"
                                value="rel" onChange={this.onRelDateChange} checked={this.state.relState}/> Buy from window</label>
                  <label><input type="radio" name="relOrStart"
                                id="start" value="start" onChange={this.onRelDateChange} checked={!this.state.relState}/> Buy from start</label>
                  <div class="form-group">
                      <input type="submit" className="btn btn-default" value="Submit"/>
                  </div>
              </form>
          </div>
        );
    }
});

module.exports = SingleEventFilter;

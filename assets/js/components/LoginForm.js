var React = require('react');
var ReactBootstrap = require('react-bootstrap');
var ServerInterface = require('../modules/ServerInterface');
var StateActions = require('../actions/StateActions');

var LoginForm = React.createClass({
    displayName: 'Login',

    getInitialState: function () {
        return {
            email: "",
            password: "",
            errors: []
        }
    },

    handleEmailChange: function (event) {
        event.preventDefault();
        this.setState({email: event.target.value});
    },

    handlePasswordChange: function (event) {
        event.preventDefault();
        this.setState({password: event.target.value});
    },

    handleSubmit: function (event) {
        event.preventDefault();
        ServerInterface.sendLogin(this.state.email,this.state.password).then(function onResolve(result) {
            console.log(result);
            this.setState({errors:[]});
            StateActions.loginSuccess(result.username);
        }.bind(this), function onReject (err) {
            console.log("FAILED",err);
            this.setState({errors:['Invalid email or password']});
        }.bind(this));
    },

    displayErrors: function (error, i) {
        return (
            <ReactBootstrap.ControlLabel>{error}</ReactBootstrap.ControlLabel>
        );
    },

    render: function () {
        return (
            <form onSubmit={this.handleSubmit}>
                <ReactBootstrap.FormGroup>
                    <ReactBootstrap.ControlLabel>Email:</ReactBootstrap.ControlLabel>
                    <ReactBootstrap.FormControl
                        type="text"
                        value={this.state.email}
                        placeholder="Enter email"
                        onChange={this.handleEmailChange}
                    />
                </ReactBootstrap.FormGroup>
                <ReactBootstrap.FormGroup>
                    <ReactBootstrap.ControlLabel>Password:</ReactBootstrap.ControlLabel>
                    <ReactBootstrap.FormControl
                        type="password"
                        value={this.state.password}
                        placeholder="Enter password"
                        onChange={this.handlePasswordChange}
                    />
                </ReactBootstrap.FormGroup>
                <ReactBootstrap.FormGroup validationState="error">
                    {this.state.errors.map(this.displayErrors)}
                </ReactBootstrap.FormGroup>
                <ReactBootstrap.FormGroup>
                    <ReactBootstrap.Button type="submit">
                        Submit
                    </ReactBootstrap.Button>
                </ReactBootstrap.FormGroup>
            </form>
        );
    }
});

module.exports = LoginForm;

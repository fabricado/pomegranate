var React = require('react');
var PomegranateConstants = require('../constants/PomegranateConstants');
var DataActions = require('../actions/DataActions');
var StateActions = require('../actions/StateActions');
var StateStore = require('../stores/StateStore');
var _ = require('lodash');
var $ = require('jquery');
var noUiSlider = require('../../bower_components/nouislider/distribute/nouislider');

module.exports = React.createClass({
    /**
     * Name displayed on the react chrome tool
     */
    displayName: 'FilterVariableForm',

    propTypes: {
        eventVariables: React.PropTypes.object.isRequired,
    },

    /**
     * Called when the component is first created, it returns the initial state
     */
    getInitialState: function () {
        //console.log("getinit: ", this.state);
        var varFilterVals = {};
        _.forOwn(this.props.eventVariables, function (value, key) {
            varFilterVals[key] = {};
            varFilterVals[key]['lowerWindow'] = value.lower;
            varFilterVals[key]['upperWindow'] = value.upper;
        });
        return {
            eventVariables: this.props.eventVariables,
            variableFilterValues: varFilterVals,
            relState: false,
            studyType: "eventStudy"
        }
    },

    componentWillReceiveProps: function (nextProps) {
        /*
        var varFilterVals = {};
        _.forOwn(this.props.eventVariables, function (value, key) {
            varFilterVals[key] = {};
            varFilterVals[key]['lowerWindow'] = value.lower;
            varFilterVals[key]['upperWindow'] = value.upper;
        });
        this.setState({
            eventVariables: nextProps.eventVariables,
            variableFilterValues: varFilterVals
        });
        */
    },

    componentDidMount: function () {
        console.log("Brain check (return from store): ", StateStore.getEventFilterState());
        var newState = StateStore.getEventFilterState();
        this.setState(newState);
        //console.log("my state should now be:", newState);
        //console.log("my state is now:", this.state);
        _.keys(this.state.eventVariables).map(this.makeSlider.bind(this, newState));
    },

    componentWillUnmount: function () {
        StateActions.setEventFilterState(this.state);
    },

    onLowerWindowChange: function (event) {
        this.setState({
            lowerWindow: event.target.value,
            boundsChanged: true
        });
    },

    onUpperWindowChange: function (event) {
        this.setState({
            upperWindow: event.target.value,
            boundsChanged: true
        });
    },

    componentWillUpdate: function (nextProps,nextState) {
        _.keys(nextState.eventVariables).map(this.updateSlider.bind(this,nextState));
    },

    onLowerBoundChange: function (event) {
        var state = this.state;
        state.boundsChanged = true;
        state.variableFilterValues[event.target.name.split('_')[1]].lowerWindow = event.target.value;
        // var slider = document.getElementById('slider_' + event.target.name.split('_')[1]);
        // slider.noUiSlider.set([event.target.value,null]);
        this.setState(state);
        //console.log("lower bound change on ", event.target.name);
        //console.log(this.state);
    },

    onUpperBoundChange: function (event) {
        var state = this.state;
        state.boundsChanged = true;
        state.variableFilterValues[event.target.name.split('_')[1]].upperWindow = event.target.value;
        // var slider = document.getElementById('slider_' + event.target.name.split('_')[1]);
        // slider.noUiSlider.set([null,event.target.value]);
        this.setState(state);
    },

    updateSlider: function (nextState,variable, i) {
        var slider = document.getElementById('slider_' + variable);
        // slider.noUiSlider.set([nextState.variableFilterValues[variable].lowerWindow,
        //     nextState.variableFilterValues[variable].upperWindow]);
    },

    makeSlider: function (state, variable, i) {
        if (!state.variableFilterValues) {
            state = this.state;
        }
        /*
        if (this.state.variableFilterValues[variable].lowerWindow
            == this.state.variableFilterValues[variable].upperWindow) {
            return null;
        }
        */
        var connectSlider = document.getElementById('slider_'+variable);
        var lowerInput = $('input[name=\"lowerWindow_'+variable+'\"]');
        var upperInput = $('input[name=\"upperWindow_'+variable+'\"]');
        var component = this;
        ////console.log(lowerInput);
        ////console.log(upperInput);
        //console.log("Creating sliders state: ", this.state);
        var leftPos = state.variableFilterValues[variable].lowerWindow;
        var rightPos = state.variableFilterValues[variable].upperWindow;
        console.log(variable, ": lower: ", state.eventVariables[variable].lower, ", upper: ", state.eventVariables[variable].upper);
        if (state.eventVariables[variable].lower == state.eventVariables[variable].upper) {
            return;
        }
        noUiSlider.create(connectSlider,{
            range: {
                min: state.eventVariables[variable].lower,
                max: state.eventVariables[variable].upper
            },
            start: [leftPos,rightPos],
            connect: true
        }).on('update', function (values, handle) {
            //console.log("Logs for the Log Throne!");
            var state;
            if (handle) {
                // //console.log("UPPER");
                state = component.state;
                state.variableFilterValues[variable].upperWindow = values[handle];
                component.setState(state);
            } else {
                // //console.log("LOWER");
                state = component.state;
                state.variableFilterValues[variable].lowerWindow = values[handle];
                component.setState(state);
            }
        });
    },

    renderVariableInputs: function (variable, i) {
        /*
        if (this.state.variableFilterValues[variable].lowerWindow
            == this.state.variableFilterValues[variable].upperWindow) {
            return null;
        }
        */
        if (this.state.eventVariables[variable].lower == this.state.eventVariables[variable].upper) {
            return;
        }
        return (
            <div key={i} class="variableInputSection">
                <h>{variable}</h><br></br>
                <div id={"slider_"+variable} style={{width:'80%', margin: '0 auto'}}></div>
                <input type="number"
                   name={"lowerWindow_"+variable}
                   onChange={this.onLowerBoundChange}
                   value={this.state.variableFilterValues[variable].lowerWindow}
                   step="any"
                />
                <input type="number"
                   name={"upperWindow_"+variable}
                   onChange={this.onUpperBoundChange}
                   value={this.state.variableFilterValues[variable].upperWindow}
                   step="any"
                />
                <input type="checkbox" name={"filterEnable_"+variable}/>
            </div>
        );
    },

    handleSubmit: function (event) {
        event.preventDefault();
        var windowBounds = {
            lowerBound: this.state.lowerWindow,
            upperBound: this.state.upperWindow
        };
        var variableFilters = {};
        _.forOwn(this.state.variableFilterValues, function (value, key) {
            var variable = key;
            variableFilters[variable] = {};
            var lowerWindow = value.lowerWindow;
            var upperWindow = value.upperWindow;
            variableFilters[variable].lowerWindow = lowerWindow;
            variableFilters[variable].upperWindow = upperWindow;
        });

        DataActions.getCumulativeReturnData(windowBounds, variableFilters,this.state.studyType,this.state.relState);
        var state = this.state;
        state.submitted=true;
        this.setState(this.state);
        StateActions.setEventFilterState(this.state);
        //console.log("Submitted: ", state);
    },

    onRelDateChange: function (event) {
        switch (event.target.value) {
            case 'rel':
                this.setState({relState: true});
                break;
            case 'start':
                this.setState({relState: false});
                break;
        }
    },

    eventStudyTypeChange: function (event) {
        this.setState({studyType: event.target.value});
    },

    /**
     * Function is called every time the state changes
     * Creates a form
     */
    render: function () {
        //console.log('render state: ', this.state);
        return (
            <div className="filterVariableFormWrapper">
                <h3 style={{margin: "0px", padding:"2px"}}>FILTERS</h3>
                <br></br>
                <form onSubmit={this.handleSubmit}>
                    <h>Date window</h><br></br>
                    <input type="number" name="lowerWindow" onChange={this.onLowerWindowChange} value={this.state.lowerWindow} required/>
                    <input type="number" name="upperWindow" onChange={this.onUpperWindowChange} value={this.state.upperWindow} required/>
                    <div className="form-group">
                        <select className="form-control"
                                value={this.state.studyType} onChange={this.eventStudyTypeChange}>
                            <option value="eventStudy">Cumulative Return Absolute</option>
                            <option value="eventStudyPercent">Cumulative Return Percentage</option>
                            <option value="vol">Volume</option>
                        </select>
                    </div>
                    <label><input type="radio" name="relOrStart" id="relDay"
                                  value="rel" onChange={this.onRelDateChange} checked={this.state.relState}/> Buy from window</label>
                    <label><input type="radio" name="relOrStart"
                                  id="start" value="start" onChange={this.onRelDateChange} checked={!this.state.relState}/> Buy from start</label>
                    <br></br>

                    {_.keys(this.state.eventVariables).map(this.renderVariableInputs)}
                    <br></br>
                    <div>
                        <input className="btn btn-default" type="submit" name="filterFormSubmit" value="submit"/>
                    </div>
                </form>
            </div>
        );
    }
});

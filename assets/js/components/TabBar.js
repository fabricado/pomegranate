/**
 * React is Facebook's V framework that we are using
 *
 * PomegranateConstants are predefined constants for the pomegranate app
 */
var React = require('react');
var PomegranateConstants = require('../constants/PomegranateConstants');
var StateStore = require('../stores/StateStore');

var TabBar = React.createClass({
    displayName: "TabBar",

    /**
     * Sets the initial state of the component on creation
     */
    getInitialState: function () {
        return {
            tabColors:["select active fileUpload",
                "select stockLifeTime", "select eventStudy",
                "select addEvents", "select news","select portfolio", "select stockStudy", "select factorStudy", "select Analysis"],
            };
    },

    /**
     * Called as soon as the component has mounted for the first time
     */
    componentDidMount: function () {
        StateStore.registerLoginSuccessListener(this.handleLoginChange);
        StateStore.registerLogoutSuccessListener(this.handleLoginChange);
    },

    /**
     * Called before the component is destroyed
     */
    componentWillUnmount: function () {
        StateStore.removeLoginSuccessListener(this.handleLoginChange);
        StateStore.removeLogoutSuccessListener(this.handleLoginChange);
    },

    handleLoginChange: function () {
        this.setState({username:StateStore.getUsername()});
    },

    handleLifeTimeClick: function (event) {
        this.props.onTabChange(PomegranateConstants.TAB_LIFETIME);
        this.setState({tabColors:["select fileUpload", "select active stockLifeTime", "select eventStudy", "select addEvents", "select news","select portfolio", "select stockStudy", "select factorStudy", "select Analysis"]});
    },

    handleEventStudyClick: function (event) {
        this.props.onTabChange(PomegranateConstants.TAB_EVENT_STUDY);
        this.setState({tabColors:["select fileUpload", "select stockLifeTime", "select active eventStudy", "select addEvents", "select news","select portfolio", "select stockStudy", "select factorStudy", "select Analysis"]});
    },

    handleUploadClick: function (event) {
        this.props.onTabChange(PomegranateConstants.TAB_UPLOAD);
        this.setState({tabColors:["select active fileUpload", "select stockLifeTime", "select eventStudy", "select addEvents", "select news","select portfolio", "select stockStudy", "select factorStudy", "select Analysis"]});
    },

    handleNewsClick: function (event) {
        this.props.onTabChange(PomegranateConstants.TAB_NEWS);
        this.setState({tabColors:["select fileUpload", "select stockLifeTime", "select eventStudy", "select addEvents", "select active news","select portfolio", "select stockStudy", "select factorStudy", "select Analysis"]});
    },

    handleAddEventsClick: function (event) {
        this.props.onTabChange(PomegranateConstants.TAB_ADD_EVENTS);
        this.setState({tabColors:["select fileUpload", "select stockLifeTime", "select eventStudy", "select active addEvents", "select news","select portfolio", "select stockStudy", "select factorStudy", "select Analysis"]});
    },

    handlePortfolioClick: function (event) {
        this.props.onTabChange(PomegranateConstants.TAB_PORTFOLIO);
        this.setState({tabColors:["select fileUpload", "select stockLifeTime", "select eventStudy", "select addEvents", "select news","select active portfolio", "select stockStudy", "select factorStudy", "select Analysis"]});
    },

    handleFactorStudyClick: function (event) {
        this.props.onTabChange(PomegranateConstants.TAB_FACTOR_STUDY);
        this.setState({tabColors:["select fileUpload", "select stockLifeTime", "select eventStudy", "select addEvents", "select news","select portfolio", "select stockStudy", "select active factorStudy", "select Analysis"]});
    },

    handleStockStudyClick: function (event) {
        this.props.onTabChange(PomegranateConstants.TAB_STOCK_STUDY);
        this.setState({tabColors:["select fileUpload", "select stockLifeTime", "select eventStudy", "select addEvents", "select news","select portfolio", "select active stockStudy", "select factorStudy", "select Analysis"]});
    },

    handleAnalysisClick: function (event) {
        this.props.onTabChange(PomegranateConstants.TAB_ANALYSIS);
        this.setState({tabColors:["select fileUpload", "select stockLifeTime", "select eventStudy", "select addEvents", "select news","select portfolio", "select stockStudy", "select factorStudy", "select active Analysis"]});
    },

    getToken: function () {
        /*
        var token = StateStore.getToken();
        if (!token) {
            token = "None";
        }
        console.log("Render token: ", token);
        return token;
        */
    },

    renderUsername: function() {
        if (this.state.username) {
            return (<li><a>{this.state.username}</a></li>);
        } else {
            return null;
        }
    },

    /**
     * Renders a view for this component
     */
    render: function () {
        return (
            <nav className="navbar navbar-default tabBarWrapper">
                <div className="container">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#spark-navbar-collapse">
                            <span className="sr-only">Toggle Navigation</span>
                            <span className="icon-bar"/>
                            <span className="icon-bar"/>
                            <span className="icon-bar"/>
                        </button>
                        <a className="navbar-brand">
                            Pomegranate
                        </a>
                    </div>

                    <div className="collapse navbar-collapse" id="spark-navbar-collapse">
                        <ul className="nav navbar-nav">
                            <li onClick={this.handleUploadClick}><a href="#" className={this.state.tabColors[0]}>File Upload <i
                                className="fa fa-upload"/></a></li>
                        </ul>
                        <ul className="nav navbar-nav">
                            <li onClick={this.handlePortfolioClick}><a href="#" className={this.state.tabColors[5]}>Portfolio</a></li>
                        </ul>
                        <ul className="nav navbar-nav">
                            <li onClick={this.handleLifeTimeClick}><a href="#" className={this.state.tabColors[1]}>Stock Lifetime</a></li>
                        </ul>
                        <ul className="nav navbar-nav">
                            <li onClick={this.handleEventStudyClick}><a href="#" className={this.state.tabColors[2]}>Event Study</a></li>
                        </ul>
                        <ul className="nav navbar-nav">
                            <li onClick={this.handleNewsClick}><a href="#" className={this.state.tabColors[4]}>News</a></li>
                        </ul>
                        <ul className="nav navbar-nav">
                            <li onClick={this.handleFactorStudyClick}><a href="#" className={this.state.tabColors[7]}>Factor Study</a></li>
                        </ul>
                        <ul className="nav navbar-nav">
                            <li onClick={this.handleStockStudyClick}><a href="#" className={this.state.tabColors[6]}>Stock Study</a></li>
                        </ul>
                        <ul className="nav navbar-nav">
                            <li onClick={this.handleAnalysisClick}><a href="#" className={this.state.tabColors[7]}>Analysis</a></li>
                        </ul>
                        <ul className="nav navbar-nav navbar-right">
                            <li id="appExit"><a href="/">Exit Pomegranate</a></li>
                        </ul>
                        <ul className="nav navbar-nav navbar-right">
                            {this.renderUsername()}
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
});

/*
 <ul className="nav navbar-nav">
 <li onClick={this.handleAddEventsClick}><a href="#" className={this.state.tabColors[3]}>Add Events</a></li>
 </ul>
 */

module.exports = TabBar;

/**
 * React is Facebook's V framework that we are using
 *
 * DataStore is a flux store that holds data about event + stock files
 *
 * PomegranateConstants are predefined constants for the pomegranate app
 */
var React = require('react');
var DataStore = require('../stores/DataStore');
var PomegranateConstants = require('../constants/PomegranateConstants');
var EventList = require('./EventList');

var LifetimeRightSidebar = React.createClass({
    displayName: "LifetimeRightSidebar",

    /**
     * Property validation and requirements
     */
    propTypes: {
        events: React.PropTypes.array.isRequired
    },

    /**
     * Sets the initial state of the component on creation
     */
    getInitialState: function () {
        return {
            events: []
        };
    },

    /**
     * Called as soon as the component has mounted for the first time
     */
    componentDidMount: function () {

    },

    componentWillReceiveProps: function (nextProps) {
        this.setState({events : nextProps.events});
    },

    /**
     * Called before the component is destroyed
     */
    componentWillUnmount: function () {

    },

    /**
     * Renders a view for this component
     */
    render: function () {
            //console.log("RENDERING EVENT SIDEBAR");
    //console.log(this.state.events);
        var tableHeaders = "<div ";
        return (
            <div className="lifetimeRightSidebarWrapper">
                <EventList events={this.state.events} onMainSetChange={this.props.onMainSetChange}/>
            </div>
        );
    }
});

module.exports = LifetimeRightSidebar;

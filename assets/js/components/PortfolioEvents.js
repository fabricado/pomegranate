var React = require('react');
var ReactBootstrap = require('react-bootstrap');
var Reactable = require('reactable');
var moment = require('moment');

var PortfolioEvents = React.createClass({
    displayName: 'PortfolioEvents',

    getInitialState: function () {
        return {
            columns: ['RIC','Date','Get Info','Remove'],
            eventInfo: this.props.eventInfo,
            showModal: false,
            currEventIndex: -1
        };
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState({eventInfo: nextProps.eventInfo});
    },

    handleEventDeleteClick: function (eventIndex,event) {
        this.props.onEventDelete(this.state.eventInfo[eventIndex]);
    },

    handleInfoClick: function (eventIndex,event) {
        console.log(this.state.eventInfo[eventIndex]);
        this.setState({currEventIndex:eventIndex});
        this.setState({showModal:true});
    },

    handleInfoClose: function() {
        this.setState({currEventIndex:-1});
        this.setState({showModal:false});
    },

    renderTableColumns: function (columnName,i) {
        return (
            <th key={i}>{columnName}</th>
        );
    },

    renderRowData: function (colName,eventIndex,i) {
        if (i < this.state.columns.length - 2) {
            return (
                <Reactable.Td key={i} style={{cursor:'pointer'}}
                              column={colName} data={this.state.eventInfo[eventIndex][_.camelCase(colName)]} />
            );
        } else if (i == this.state.columns.length - 2) {
            return <Reactable.Td key={i} column={colName}>
                <i className="fa fa-2x fa-info-circle infoSymbol" onClick={this.handleInfoClick.bind(this,eventIndex)}/>
            </Reactable.Td>
        } else {
            return <Reactable.Td key={i} column={colName}>
                <i className="fa fa-2x fa-times deleteSymbol" onClick={this.handleEventDeleteClick.bind(this,eventIndex)}/>
            </Reactable.Td>
        }
    },

    renderTableData: function (event, i) {
        return (
            <Reactable.Tr key={i}>
                {this.state.columns.map(function (colName, colIndex) {
                    return this.renderRowData(colName, i, colIndex);
                }, this)}
            </Reactable.Tr>
        );
    },

    tableDateSorter: function (a,b) {
        if (moment(a,'DD-MMM-YYYY').isAfter(moment(b,'DD-MMM-YYYY'))) {
            return 1;
        } else {
            return -1;
        }
    },

    renderEventInfo: function() {
        if (this.state.showModal == true) {
            var stock = this.state.eventInfo[this.state.currEventIndex].ric;
            var eventDate = this.state.eventInfo[this.state.currEventIndex].date;
            var variables = this.state.eventInfo[this.state.currEventIndex].variables;
            return( 
                <ReactBootstrap.Modal.Body>
                <h4>RIC</h4>
                <p>{stock}</p>
                <hr></hr>
                <h4>DATE</h4>
                <p>{eventDate}</p>
                <hr></hr>
                <h4>VARIABLES</h4>
                {Object.keys(variables).map(function(key) {
                    return <div>{key}: {variables[key]}</div>;
                })}
                </ReactBootstrap.Modal.Body>
            );
        } else {
            return( 
                <ReactBootstrap.Modal.Body>
                </ReactBootstrap.Modal.Body>
            );   
        }
        
    },

    render: function () {
        return (
            <div className="portfolioEventsWrapper">
                <div className="eventInfoWrapper">
                    <ReactBootstrap.Modal show={this.state.showModal} onHide={this.handleInfoClose} dialogClassName="eventInfoModal">
                        <ReactBootstrap.Modal.Header closeButton>
                            <ReactBootstrap.Modal.Title>Event Info</ReactBootstrap.Modal.Title>
                        </ReactBootstrap.Modal.Header>
                        {this.renderEventInfo()}
                        <ReactBootstrap.Modal.Footer>
                            <ReactBootstrap.Button onClick={this.handleInfoClose}>Close</ReactBootstrap.Button>
                        </ReactBootstrap.Modal.Footer>
                    </ReactBootstrap.Modal>
                </div>
                <Reactable.Table className="table table-hover" sortable={[
                    'RIC',
                    {column:'Date',sortFunction:this.tableDateSorter}
                ]} defaultSort={{column: 'Date',sortFunction: this.tableDateSorter,direction: 'desc'}}>
                    {this.state.eventInfo.map(this.renderTableData)}
                </Reactable.Table>
            </div>
        );
    }
});

module.exports = PortfolioEvents;

/**
 * React is Facebook's V framework that we are using
 *
 * LifetimeRightSidebar is the react element that renders the right sidebar
 * It will contain a list of events which the user can interact with
 *
 * LifetimeGraph is the react element that renders the graph for the lifetime section
 */
var React = require('react');
var ReactBootstrap = require('react-bootstrap');
var LifetimeRightSidebar = require('./LifetimeRightSidebar');
var LifetimeGraph = require('./LifetimeGraph');
var DataStore = require('../stores/DataStore');
var DataActions = require('../actions/DataActions');
var CustomEventForm = require('./CustomEventForm');
var MiniEventStudy = require('./MiniEventStudy');
var ServerInterface = require('../modules/ServerInterface');
var _ = require('lodash');

var NewsRenderer = React.createClass({
    displayName: "NewsRenderer",

    /**
     * Property validation and requirements
     */
    propTypes: {
        RICs: React.PropTypes.array.isRequired
    },

    /**
     * Sets the inital state of the component on creation
     */
    getInitialState: function () {
        return {
            news:[],
            newEvent: {ric:"",date:"",variables:{}},
            showMiniStudy: false,
            showAddEvent: false,
            newVarName: '',
            newVarValue: ''
        };
    },

    /**
     * Called as soon as the component has mounted for the first time
     */
    componentDidMount: function () {
        DataStore.registerNewsChangeListener(this.onNewsChange);
    },

    /**
     * Called before the component is destroyed
     */
    componentWillUnmount: function () {
        DataStore.removeNewsChangeListener(this.onNewsChange);
    },

    onNewsChange: function () {
        this.setState({news: DataStore.getNews()});
    },

    handleGetNews: function (event) {
        event.preventDefault();
        DataActions.getNews();
    },

    handleMiniEventStudyClick: function (event) {
        this.setState({
            showMiniStudy: true,
            miniStudyDate: event.target.name,
            selectedRic: ''
        });
    },

    handleShowAddEvent: function () {
        this.setState({
            newEvent: {ric:this.state.selectedRic,date:this.state.miniStudyDate,variables:{}},
            showAddEvent: true
        });
    },

    handleHideMiniEventStudy: function (event) {
        this.setState({showMiniStudy: false});
    },

    handleHideAddEvent: function () {
        this.setState({showAddEvent: false});
    },

    changeSelectedRic: function (newRic) {
        this.setState({selectedRic: newRic});
    },

    renderNewVariables: function (varName,i) {
        return (
            <h5>Variable: {varName} Value: {this.state.newEvent.variables[varName]}
                <ReactBootstrap.Button bsSize="small" onClick={this.removeNewVariable.bind(this,varName)}
                                       style={{marginLeft:'5px'}}>Remove</ReactBootstrap.Button></h5>
        );
    },

    addNewVariable: function (event) {
        event.preventDefault();
        var newEvent = this.state.newEvent;
        var variables = newEvent.variables;
        variables[this.state.newVarName] = this.state.newVarValue;
        this.setState({newEvent:newEvent});
    },

    removeNewVariable: function (varName) {
        var newEvent = this.state.newEvent;
        delete newEvent.variables[varName];
        this.setState({newEvent: newEvent});
    },

    changeNewVarName: function (event) {
        this.setState({newVarName:event.target.value});
    },

    changeNewVarValue: function (event) {
        this.setState({newVarValue:event.target.value});
    },

    finishEventAdd: function () {
        ServerInterface.postNewEvent(this.state.newEvent).then(function onResolve(result) {
            this.setState({showAddEvent: false});
            DataActions.getEvents();
        }.bind(this), function onReject(err) {
            console.log(err);
        });
    },

    renderAddEventModal: function () {
        return (
            <ReactBootstrap.Modal show={this.state.showAddEvent} onHide={this.handleHideAddEvent} dialogClassName="addEventModal">
                <ReactBootstrap.Modal.Header closeButton>
                    <ReactBootstrap.Modal.Title>Add new event</ReactBootstrap.Modal.Title>
                </ReactBootstrap.Modal.Header>
                <ReactBootstrap.Modal.Body>
                    <b>Event RIC: </b>{this.state.newEvent.ric}
                    <hr />
                    <b>Event Date: </b>{this.state.newEvent.date}
                    {_.keys(this.state.newEvent.variables).map(this.renderNewVariables)}
                    <hr />
                    <b>Add new event variables</b>
                    <br />
                    <br />
                    <ReactBootstrap.Form inline>
                        <ReactBootstrap.FormGroup>
                            <ReactBootstrap.ControlLabel>Name</ReactBootstrap.ControlLabel>
                            {' '}
                            <ReactBootstrap.FormControl onChange={this.changeNewVarName}
                                type="text" placeholder="Variable Name" />
                        </ReactBootstrap.FormGroup>
                        {' '}
                        <ReactBootstrap.FormGroup>
                            <ReactBootstrap.ControlLabel>Value</ReactBootstrap.ControlLabel>
                            {' '}
                            <ReactBootstrap.FormControl onChange={this.changeNewVarValue}
                                type="number" step="any" placeholder="E.g. 22.3" />
                        </ReactBootstrap.FormGroup>
                        {' '}
                        <ReactBootstrap.Button type="submit" onClick={this.addNewVariable}>
                            Add Variable
                        </ReactBootstrap.Button>
                    </ReactBootstrap.Form>
                </ReactBootstrap.Modal.Body>
                <ReactBootstrap.Modal.Footer>
                    <ReactBootstrap.Button style={{float:'left'}} bsStyle="primary"
                        onClick={this.finishEventAdd}>Finalize Event</ReactBootstrap.Button>
                    <ReactBootstrap.Button className="closeButton" onClick={this.handleHideAddEvent}>Close</ReactBootstrap.Button>
                </ReactBootstrap.Modal.Footer>
            </ReactBootstrap.Modal>
        );
    },

    renderModal: function () {
        return (
            <ReactBootstrap.Modal show={this.state.showMiniStudy} onHide={this.handleHideMiniEventStudy} dialogClassName="miniEventstudyModal">
                <ReactBootstrap.Modal.Header closeButton>
                    <ReactBootstrap.Modal.Title>Mini Event Study</ReactBootstrap.Modal.Title>
                </ReactBootstrap.Modal.Header>
                <ReactBootstrap.Modal.Body>
                    <MiniEventStudy eventDate={this.state.miniStudyDate} onRicSelect={this.changeSelectedRic}
                                    RICs={this.props.RICs} eventVariables={this.props.eventVariables} />
                </ReactBootstrap.Modal.Body>
                <ReactBootstrap.Modal.Footer>
                    <ReactBootstrap.Button style={{float:'left'}} onClick={this.handleShowAddEvent}
                                           bsStyle='primary'>Add event to portfolio</ReactBootstrap.Button>
                    <ReactBootstrap.Button className="closeButton" onClick={this.handleHideMiniEventStudy}>Close</ReactBootstrap.Button>
                </ReactBootstrap.Modal.Footer>
            </ReactBootstrap.Modal>
        );
    },

    formatArticle: function (article, i) {
        var newsId = "#" + i;
        return (
            <div className="newsArticle">
                <div className="panel-heading">
                <h4><a role="button" data-toggle="collapse" href={newsId}>{article.date.split('T')[0]}: {article.headline}</a></h4>
                </div>
                <div id={i} className="panel-collapse collapse" role="tabpanel">
                <ReactBootstrap.Button name={article.date} onClick={this.handleMiniEventStudyClick}>Perform event study...</ReactBootstrap.Button>
                <br/>
                {article.body}
                </div>
            </div>
        );
    },

    /**
     * Renders a view for this component
     */
    render: function () {
        //var formattedNews = 'beep';
        console.log(this.state.news);
        return (
            <div className="newsRenderWrapper">
                {this.state.news.map(this.formatArticle)}
                {this.renderModal()}
                {this.renderAddEventModal()}
            </div>
        );
    }
});

module.exports = NewsRenderer;

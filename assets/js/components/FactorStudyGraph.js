var React = require('react');
var ReactBootstrap = require('react-bootstrap');
var ChartCreator = require('../modules/ChartCreator');
var DataStore = require('../stores/DataStore');

var FactorStudyGraph = React.createClass({
    displayName: 'FactorStudyGraph',

    getInitialState: function () {
        return {
            data: {}
        };
    },

    componentDidMount: function () {
        //render the chart, with the portfolio panel populated
        //TODO
        //ChartCreator.makeLifetimeChart('stockStudyChartDiv',this.state.stockData);
        DataStore.registerExternalFactorGetListener(this.handleExternalFactorData);
    },

    componentWillUnmount: function () {
        DataStore.removeExternalFactorGetListener(this.handleExternalFactorData);
    },

    handleExternalFactorData: function () {
        //update factor panel in the chart
        //TODO
        ChartCreator.makeExternalDataChart('lifeChartDiv',DataStore.getExternalFactorData(),[],{ric:"",date:""}, this.props.clickHandler);
    },

    componentDidUpdate: function () {
    },

    render: function () {
        return (
            <div className="factorStudyGraphWrapper">
                <div id="lifeChartDiv" style={{width:'100%',height:'100%'}}></div>
            </div>
        );
    }
});

module.exports = FactorStudyGraph;

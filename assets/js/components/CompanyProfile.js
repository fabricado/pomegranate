/**
 * React is Facebook's V framework that we are using
 *
 * DataStore is a flux store that holds data about event + stock files
 *
 * PomegranateConstants are predefined constants for the pomegranate app
 */
var React = require('react');
var PomegranateConstants = require('../constants/PomegranateConstants');
var EventSelector = require('./EventSelector');
var FilterVariableForm = require('./FilterVariableForm');
var EventStudyRightSidebarTabs = require('./EventStudyRightSidebarTabs');
var EventList = require('./EventList');
var DataStore = require('../stores/DataStore');
var DataActions = require('../actions/DataActions');
var SingleEventFilter = require('./SingleEventFilter');
var ServerInterface = require('../modules/ServerInterface');

var CompanyProfile = React.createClass({
    displayName: "CompanyProfile",

    /**
     * Sets the initial state of the component on creation
     */
    getInitialState: function () {
        return {
            companyProfile: DataStore.getCompanyProfile()
        };
    },

    /**
     * Called as soon as the component has mounted for the first time
     */
    componentDidMount: function () {
        DataStore.registerCompanyProfileChangeListener(this.handleProfileChange);
    },

    renderCompanyProfile: function () {
        return (
            <div>{_.keys(this.state.companyProfile).map(this.renderProfileData)}</div>
        );
    },

    handleProfileChange: function () {
        this.setState({companyProfile: DataStore.getCompanyProfile()});
    },

    renderProfileData: function (key, i) {
        return (
            <div>{key}: {this.state.companyProfile[key]}</div>
        );
    },

    /**
     * Renders a view for this component
     */
    render: function () {
        return (
            <div className="companyProfileWrapper">
                {this.renderCompanyProfile()}
            </div>
        );
    }
});

module.exports = CompanyProfile;

var React = require('react');
var ReactBootstrap = require('react-bootstrap');
var ServerInterface = require('../modules/ServerInterface');
var ChartCreator = require('../modules/ChartCreator');

var MiniEventStudy = React.createClass({

    displayName: "MiniEventStudy",

    propTypes: {
        eventDate: React.PropTypes.string.isRequired,
        RICs: React.PropTypes.array.isRequired
    },

    getInitialState: function () {
        var datespl = this.props.eventDate.split('T');
        datespl = datespl[0].split('-');
        var date = datespl[2]+'-'+datespl[1]+'-'+datespl[0];
        this.props.onRicSelect(this.props.RICs[0]);
        return {
            eventDate: date,
            RICs: this.props.RICs,
            selectedRIC: this.props.RICs[0]
        }
    },

    onLowerWindowChange: function (event) {
        this.setState({
            lowerWindow: event.target.value
        })
    },

    onUpperWindowChange: function (event) {
        this.setState({
            upperWindow: event.target.value
        })
    },

    submitMiniStudy: function (event) {
        event.preventDefault();
        console.log("SUBMITTING FORM... ");
        console.log(this.state);
        //var datespl = this.state.eventDate
        ServerInterface.getRandomEventData(this.state.eventDate, this.state.lowerWindow,
                                                this.state.upperWindow).then(function onResolve (result) {
            console.log(result);
            ChartCreator.makeSingleEventLifetimeChart('miniEventStudyGraph',result,this.state.selectedRIC);
        }.bind(this), function onReject (error) {
            console.log("EPIC FAIL");
        });
    },

    handleRICSelect: function (eventKey, event) {
        console.log(eventKey);
        this.props.onRicSelect(eventKey);
        this.setState({
            selectedRIC: eventKey
        });
    },

    renderRICList: function (val, index) {
        return (
            <ReactBootstrap.MenuItem onClick={this.handleRICSelect} eventKey={val}>{val}</ReactBootstrap.MenuItem>
        );
    },

    render: function () {
        return (
            <div className="MiniEventStudyWrapper" onSubmit={this.submitMiniStudy}>
                <form className="miniEventStudyForm">
                    <b className="miniEventStudyDate">Date: </b> {this.state.eventDate}
                    <b className="miniEventStudyRIC">RIC: </b>
                    <ReactBootstrap.DropdownButton title={this.state.selectedRIC} id="RICdropdown" onSelect={this.handleRICSelect}>
                        {this.state.RICs.map(this.renderRICList)}
                    </ReactBootstrap.DropdownButton>
                    <b className="miniEventStudyWindow">Window: </b>
                    <input type="number" name="lowerWindow" className="dateWindow" onChange={this.onLowerWindowChange} required/>
                    <input type="number" name="upperWindow" className="dateWindow" onChange={this.onUpperWindowChange} required/>
                    <input type="submit" className="btn btn-default"/>
                    <hr />
                    <div id="miniEventStudyChart"></div>
                </form>
                <div id="miniEventStudyGraph"></div>
            </div>
        );
    }
});

module.exports = MiniEventStudy;

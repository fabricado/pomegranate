/**
 * React is Facebook's V framework that we are using
 *
 * LifetimeRightSidebar is the react element that renders the right sidebar
 * It will contain a list of events which the user can interact with
 *
 * LifetimeGraph is the react element that renders the graph for the lifetime section
 */
var React = require('react');
var LifetimeRightSidebar = require('./LifetimeRightSidebar');
var LifetimeGraph = require('./LifetimeGraph');
var DataStore = require('../stores/DataStore');
var DataActions = require('../actions/DataActions');
var NewsRenderer = require('./NewsRenderer');

var NewsWindow = React.createClass({
    displayName: "NewsWindow",

    /**
     * Sets the inital state of the component on creation
     */
    getInitialState: function () {
        return {rics: DataStore.getUserRICs()};
    },

    /**
     * Called as soon as the component has mounted for the first time
     */

    handleGetNews: function(event) {
        event.preventDefault();
        DataActions.getNews(this.state.rics);
    },

    /**
     * Renders a view for this component
     */
    render: function () {
    //console.log("RENDERING LTW");
    //console.log(this.state.events);
        return (
            <div className="newsWindowWrapper">
                <form id="newsForm" onSubmit={this.handleGetNews}>
                    <div className="form-group newsSubmit">
                        <input className="btn btn-default" type="submit" name="getNews" value="Get News"/>
                    </div>
                </form>
                <NewsRenderer RICs={this.state.rics} eventVariables={this.props.eventVariables} />
            </div>
        );
    }
});

module.exports = NewsWindow;

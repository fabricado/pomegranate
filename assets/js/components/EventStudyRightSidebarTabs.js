var React = require('react');
var PomegranateConstants = require('../constants/PomegranateConstants');

var EventStudyRightSidebarTabs = React.createClass({
    displayName: "EventStudyRightSidebarTabs",

    getInitialState: function () {
        return {tabColors:["select singleEventView", "select active multiEventView"]};
    },

    handleSingleViewClick: function () {
        this.props.onTabChange(PomegranateConstants.TAB_SINGLE_EVENT_STUDY);
        this.setState({tabColors:['select active singleEventView','select multiEventView']});
    },

    handleMultiViewClick: function () {
        this.props.onTabChange(PomegranateConstants.TAB_MULTI_EVENT_STUDY);
        this.setState({tabColors:['select singleEventView','select active multiEventView']});
    },

    render: function () {
        return (
            <nav className="navbar navbar-default tabBarWrapper">
                <div className="container">
                        <ul className="nav navbar-nav inner-nav">
                            <li onClick={this.handleSingleViewClick}><a href="#" className={this.state.tabColors[0]}>Single Event Study</a></li>
                        </ul>
                        <ul className="nav navbar-nav inner-nav">
                            <li onClick={this.handleMultiViewClick}><a href="#" className={this.state.tabColors[1]}>Multi Event Study</a></li>
                        </ul>
                </div>
            </nav>
        );
    }
});

module.exports = EventStudyRightSidebarTabs;

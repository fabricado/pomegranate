/*
 The releases module handles events on the api releases page

 The init function should be called on module load
 */

var $ = require('jquery');

module.exports = (function () {
    return {
        //initialises all functions for module
        init: function () {
            this.bindUI();
        },

        //handles dom events
        bindUI: function () {
            $(document).ready(function () {
                /**
                 * Toggles visibility of release notes
                 */
                $('.rNotesToggle').on('click', function () {
                    var rNotesContainer = $(this).parent().find('.releaseNotesContainer');
                    $(rNotesContainer).toggle();
                    if ($(rNotesContainer).is(':visible')) {
                        $(this).find('span').removeClass('fa-plus-circle').addClass('fa-minus-circle');
                    } else {
                        $(this).find('span').removeClass('fa-minus-circle').addClass('fa-plus-circle');
                    }
                });
            });
        }
    };
})();

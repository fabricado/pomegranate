var $ = require('jquery');
var async = require('async');
var moment = require('moment');
var StateStore = require('../stores/StateStore');
var DataProcessor = require('./DataProcessor');
var _ = require('lodash');

/**
 * This module can be used to receive data from the server
 */
var ServerInterface = {
    /**
     * This function will ask for data from the following endpoints
     * /pomegranate/filesInSession
     * /api/es?mode=analyse
     * @returns {Promise}
     */
    getFileAnalyses: function () {
        return new Promise(function (resolve, reject) {
            $.when(
                $.ajax({
                    url: '/pomegranate/filesInSession',
                    type: 'GET',
                    dataType: "json"
                }),
                $.ajax({
                    //this is the call to --analyse
                    url: '/api/es',
                    type: 'GET',
                    data: {mode: 'analyse'},
                    dataType: 'json'
                })
            ).then(function onSuccess(filesInSession, fileAnalysis) {
                var eventVariables = [];
                var rics = [];
                if (!('error' in fileAnalysis[0])) {
                    eventVariables = fileAnalysis[0].results.eventVariables;
                    rics = fileAnalysis[0].results.rics;
                }
                var result = {
                    serverFiles: {
                        stockFile: filesInSession[0].stockFile,
                        eventFile: filesInSession[0].eventFile
                    },
                    RICs: rics,
                    eventVariables: eventVariables
                };
                resolve(result);
            }, function onFailure(err) {
                console.log("API queries failed");
                reject(err);
            });
        });
    },

    /**
     * This function will ask for data from the following endpoints
     * /api/event
     * @returns {Promise}
     */
    getAllEvents: function () {
        return new Promise(function (resolve, reject) {
            $.when(
                $.ajax({
                    url: 'api/event',
                    type: 'GET',
                    dataType: 'json'
                })
            ).then(function onSuccess(allEvents) {
                resolve(DataProcessor.processEventsJson(allEvents));
            }, function onFailure(err) {
                console.log(err);
                reject(Error(err));
            });
        });
    },


    /**
     * This function will ask for data from the following endpoints
     * /api/es?mode=getRIC&RIC=ric
     * One call will be made for each RIC, the returned promise is resoved when all calls are completed
     * @param rics A list of RICs
     * @returns {Promise}
     */
    getStockLifetimeData: function (rics) {
        return new Promise(function (resolve, reject) {
            var ricsLifetimeData = {};
            async.each(rics, function (ric, callback) {
                $.when(
                    $.ajax({
                        url: '/api/es',
                        type: 'GET',
                        data: {mode: 'getRIC', RIC: ric},
                        dataType: 'json'
                    })
                ).then(function onSuccess(ricValues) {
                    ricsLifetimeData[ricValues.results.ric] = {
                        startDate: moment(ricValues.results.startDate, "DD-MM-YYYY"),
                        values: ricValues.results.values
                    };
                    callback();
                }, function onFailure(error) {
                    callback(error);
                });
            }, function (err) {
                if (err) {
                    reject(err);
                } else {
                    console.log(ricsLifetimeData);
                    return resolve(ricsLifetimeData);
                }
            });
        });
    },

    /**
     *
     * @param windowBounds: @type {lowerBound: Integer, upperBound: Integer}
     * @param variableFilters @type {VariableName: {lowerWindow: Double, upperWindow: Double}}
     * @param mode
     * @param rel
     * @returns {Promise}
     */
    getCumulativeReturns: function (windowBounds, variableFilters,mode, rel) {
        return new Promise(function (resolve, reject) {
            var eventTypeValues = [];
            for (var variableName in variableFilters) {
                if (variableFilters.hasOwnProperty(variableName)) {
                    eventTypeValues.push("\"" + variableName + "\"",
                        variableFilters[variableName].lowerWindow,
                        variableFilters[variableName].upperWindow);
                }
            }

            var dataToSend = {
                lowerWindow: windowBounds.lowerBound,
                upperWindow: windowBounds.upperBound,
                eventTypeBounds: eventTypeValues,
                mode: mode,
                relFlag: rel,
            };
            $.ajax({
                url: "/api/es",
                type: "GET",
                data: dataToSend,
                datatType: "json",
                success: function (data, status, xhr) {
                    resolve(data);
                },
                error: function (xhr, status, error) {
                    console.log("Data form failed with status ", status, " and error ", error);
                    reject(Error(error));
                }
            });
        });
    },

    /**
     * This method can be used to send user inputted files to the server
     * An example for creating the input parameter is -
     * stockFileData.append('fileType', 'stockFile');
     * stockFileData.append('stockFile', $(event.target).find('input[name=stockFile]')[0].files[0]);
     * @param stockFileFormData @type {{FormData}}
     * @param eventFileFormData @type {{FormData}}
     * @returns {Promise}
     */
    sendStockFileToServer: function (stockFileFormData) {
        return new Promise(function (resolve, reject) {
            $.when(
                $.ajax({    // AJAX request for stock file
                    url: '/api/stockFile',
                    type: 'POST',
                    data: stockFileFormData,
                    processData: false,
                    contentType: false
                })
            ).then(function onSuccess(stockFileResult) {
                // note both params have data, status, xhr as 0 1 2
                console.log('TOTAL AJAX SUCCESS');
                resolve(stockFileResult);
            }, function onFailure(err) {
                console.log('TOTAL AJAX FAIL FOR FILE UPLOADING');
                reject(err);
            });
        });
    },

    sendEventFileToServer: function (eventFileFormData) {
        return new Promise(function (resolve, reject) {
            $.when(
                $.ajax({    // AJAX request for event file
                    url: '/api/eventFile',
                    type: 'POST',
                    data: eventFileFormData,
                    processData: false,
                    contentType: false
                })
            ).then(function onSuccess(eventFileResult) {
                // note both params have data, status, xhr as 0 1 2
                console.log('TOTAL AJAX SUCCESS');
                resolve(eventFileResult);
            }, function onFailure(err) {
                console.log('TOTAL AJAX FAIL FOR FILE UPLOADING');
                reject(err);
            });
        });
    },

    /**
     * This method can be used to send session state to the server
     */
    sendStateToServer: function (data) {
        console.log(data);
        return new Promise(function (resolve, reject) {
            $.when(
                $.ajax({    // AJAX request for stock file
                    url: '/session/state',
                    type: 'POST',
                    dataType: 'application/json',
                    data: data
                })
            ).then(function onSuccess(result) {
                resolve(result);
            }, function onFailure(err) {
                reject(err);
            });
        });
    },

    /**
     * This method can be used to retrieve session state from the server
     */
    getStateFromServer: function () {
        return new Promise(function (resolve, reject) {
            $.when(
                $.ajax({    // AJAX request for stock file
                    url: '/session/state',
                    type: 'GET',
                    dataType: 'json',
                })
            ).then(function onSuccess(result) {
                console.log("State got ok");
                console.log(result);
                resolve(result);
            }, function onFailure(err) {
                console.log("state got went bad");
                reject(err);
            });
        });
    },

    /**
     * This method can be used to retrieve user's active session from the server
     */
    getActiveSessionFromServer: function () {
        return new Promise(function (resolve, reject) {
            $.when(
                $.ajax({    // AJAX request for stock file
                    url: '/session/active',
                    type: 'GET'
                })
            ).then(function onSuccess(result) {
                resolve(result);
            }, function onFailure(err) {
                reject(err);
            });
        });
    },

    /**
     * Can be used when we want the make an event file for single event study
     * @param event
     * @param rics
     * @returns {Promise}
     */
    makeFakeEvent: function (event,rics) {
        console.log("Make fake event call");
        console.log(event, rics);
        return new Promise(function (resolve,reject) {
            $.when(
                $.ajax({
                    url: '/pomegranate/makeFakeEvent',
                    type: 'POST',
                    data: {"rics":rics,"event":event}
                })
            ).then(function onSuccess(result) {
                resolve(result);
            }, function onFailure (err) {
                reject(err);
            });
        });
    },

    /**
     * Will run a cumulative return calc with the fake event file
     * @param windowBounds: @type {lowerBound: Integer, upperBound: Integer}
     */
    getSingleEventCumulativeReturns: function (windowBounds,mode, rel) {
        return new Promise(function (resolve, reject) {
            var dataToSend = {
                lowerWindow: windowBounds.lowerBound,
                upperWindow: windowBounds.upperBound,
                mode: mode,
                relFlag: rel,
                singleEvent: true
            };
            $.ajax({
                url: "/api/es",
                type: "GET",
                data: dataToSend,
                dataType: "json",
                success: function (data, status, xhr) {
                    resolve(data);
                },
                error: function (xhr, status, error) {
                    console.log("Data form failed with status ", status, " and error ", error);
                    reject(Error(error));
                }
            });
        });
    },

    getNewsFromServer: function (rics) {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: "/api/news",
                type: "GET",
                data: {'rics': rics},
                success: function (data, status, xhr) {
                    //console.log("sinterface: news data: ", data);
                    resolve(data);
                },
                error: function (xhr, status, error) {
                    console.log("News failed with status ", status, " and error ", error);
                    reject(Error(error));
                }
            });
        });
    },

    postNewEvent: function (e) {
        var dataToSend = {};
        for (var key in e) {
            if (e.hasOwnProperty(key)) {
                if (key == 'ric') {
                    dataToSend['#RIC'] = e[key];
                } else if (key == 'date') {
                    dataToSend['Event Date'] = moment(e[key]).format('DD-MMM-YYYY');
                } else if (key == 'variables') {
                    for (var varKey in e[key]) {
                        if (e[key].hasOwnProperty(varKey)) {
                            dataToSend[varKey] = e[key][varKey];
                        }
                    }
                }
            }
        }
        return new Promise(function (resolve, reject) {
            $.when(
                $.ajax({    // AJAX request for stock file
                    url: '/api/event',
                    type: 'POST',
                    data: dataToSend
                })
            ).then(function onSuccess() {
                resolve();
            }, function onFailure(err) {
                reject(err);
            });
        });
    },

    getRawEventData: function () {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: "/api/es",
                type: "GET",
                data: {
                    mode: 'rawEventData',
                },
                success: function (data, status, xhr) {
                    resolve(data);
                },
                error: function (xhr, status, error) {
                    console.log("Failed to get raw event data, status ", status, " and error ", error);
                    reject(Error(error));
                }
            })
        });
    },

    writeToEventFile: function (data) {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: "/api/es",
                type: "GET",
                data: {
                    mode: 'postEventData',
                    RIC: data
                },
                success: function (data, status, xhr) {
                    resolve(data);
                },
                error: function (xhr, status, error) {
                    console.log("Failed write event data, status ", status, " and error ", error);
                    reject(Error(error));
                }
            });
        }) ;
    },

    sendLogin: function (email,password) {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: '/login',
                type: 'POST',
                data: {email: email,password: password},
                dataType: 'json',
                success: function (data, status, xhr) {
                    resolve(data);
                },
                error: function (xhr,status,error) {
                    reject(error);
                }
            });
        });
    },

    sendLogout: function () {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data, status, xhr) {
                    resolve(data);
                },
                error: function (xhr,status,error) {
                    reject(error);
                }
            });
        });
    },

    getLoginStatus: function () {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: '/loginStatus',
                type: 'GET',
                success: function (data,status,xhr) {
                    resolve(data);
                },
                error: function (xhr, status,error) {
                    reject(err);
                }
            });
        });
    },

    sendNewUser: function (username, email, password) {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: '/user',
                type: 'POST',
                data: {username: username,email: email,password: password},
                success: function (data,status,xhr) {
                    resolve(data);
                },
                error: function (xhr, status,error) {
                    reject(err);
                }
            });
        });
    },

    getPortfolioStocks: function () {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: '/stocks',
                type: 'GET',
                dataType: 'json',
                success: function (data,status,xhr) {
                    resolve(data);
                },
                error: function (xhr, status,error) {
                    reject(error);
                }
            });
        });
    },

    getServerRICList: function () {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: '/api/serverStocksList',
                type: 'GET',
                dataType: 'json',
                success: function (data,status,xhr) {
                    resolve(data);
                },
                error: function (xhr, status,error) {
                    reject(error);
                }
            });
        });
    },

    getCompanyProfile: function (RIC) {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: '/api/profile',
                type: 'GET',
                data: {'RIC': RIC},
                dataType: 'json',
                success: function (data,status,xhr) {
                    console.log(data);
                    resolve(data);
                },
                error: function (xhr, status,error) {
                    reject(error);
                }
            });
        });
    },

    getServerStockLifetime: function (RIC) {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: '/api/serverStockLifetime',
                type: 'GET',
                data: {'RIC': RIC},
                dataType: 'json',
                success: function (data,status,xhr) {
                    console.log(data);
                    //process data into required form:
                    var ricsLifetimeData = {};
                    ricsLifetimeData[RIC] = {
                        startDate: moment(data.results.startDate, "DD-MM-YYYY"),
                        values: data.results.values
                    };

                    resolve(ricsLifetimeData);
                },
                error: function (xhr, status,error) {
                    reject(error);
                }
            });
        });
    },

    getUserStockLifetime: function (RIC) {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: '/api/es',
                type: 'GET',
                data: {mode: 'getRIC', 'RIC': RIC},
                dataType: 'json',
                success: function (data,status,xhr) {
                    console.log("FISH FOR BRAINS");
                    //process data into required form:
                    var ricsLifetimeData = {};
                    ricsLifetimeData[RIC] = {
                        startDate: moment(data.results.startDate, "DD-MM-YYYY"),
                        values: data.results.values
                    };

                    resolve(ricsLifetimeData);
                },
                error: function (xhr, status,error) {
                    reject(error);
                }
            });
        });
    },

    sendEventDelete: function (event) {
        var dataToSend = {};
        for (var key in event) {
            if (event.hasOwnProperty(key)) {
                if (key === 'ric') {
                    dataToSend['#RIC'] = event[key];
                } else if (key === 'date') {
                    dataToSend['Event Date'] = event[key];
                } else if (key === 'variables') {
                    for (var varKey in event[key]) {
                        if (event[key].hasOwnProperty(varKey)) {
                            dataToSend[varKey] = event[key][varKey];
                        }
                    }
                }
            }
        }
        return new Promise(function (resolve,reject) {
            $.ajax({
                url: '/api/event',
                type: 'DELETE',
                data: dataToSend,
                success: function (data,status,xhr) {
                    resolve(data);
                },
                error: function (xhr, status,error) {
                    reject(error);
                }
            });
        });
    },

    sendPortfolioStockDelete: function (stock) {
        return new Promise(function (resolve,reject) {
            $.ajax({
                url: '/Stocks',
                type: 'DELETE',
                data: {id:stock.id},
                success: function (data,status,xhr) {
                    resolve(data);
                },
                error: function (xhr, status,error) {
                    reject(error);
                }
            });
        });
    },

    getRandomEventData: function (date, lowerWindow, upperWindow) {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: '/api/es',
                type: 'GET',
                dataType: 'json',
                data: {
                    lowerWindow: lowerWindow,
                    upperWindow: upperWindow,
                    mode: "eventStudy",
                    randomDate: date
                },
                success: function (data,status,xhr) {
                    resolve(data);
                },
                error: function (xhr, status,error) {
                    reject(err);
                }
            });
        });
    },

    getInterestRateData: function (country) {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: '/interestRate',
                type: 'GET',
                data: {"country": country},
                dataType: 'json',
                success: function (data,status,xhr) {
                    resolve(data);
                },
                error: function (xhr, status,error) {
                    reject(err);
                }
            });
        });
    },

    getExchangeRateData: function (currency) {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: '/exchangeRate',
                type: 'GET',
                data: {"currency": currency},
                dataType: 'json',
                success: function (data,status,xhr) {
                    resolve(data);
                },
                error: function (xhr, status,error) {
                    reject(err);
                }
            });
        });
    },

    addStockToPortfolio: function (stock) {
        console.log(stock);
        var d = moment(stock.buyInDate);
        stock.buyInDate = d.format("DD-MM-YYYY");
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: '/stocks/create',
                type: 'POST',
                data: stock,
                success: function (data,status,xhr) {
                    resolve(data);
                },
                error: function (xhr, status,error) {
                    reject(err);
                }
            });
        });
    }
};

module.exports = ServerInterface;

/*
 The HomePage module handles events on the homepage

 The init function should be called on module load
 */
var $ = require('jquery');

module.exports = (function () {
    return {
        //initialises all functions for module
        init: function () {
            this.bindUI();
        },

        //handles dom events
        bindUI: function () {
            /**
             * Bound to the page scroll arrow on the homepage
             * Will move the window to the position of the location of the linked element
             */
            $('.btn-circle').bind('click', function (event) {
                event.preventDefault();
                var $anchor = $(this);
                $('html, body').stop().animate({
                    scrollTop: $($anchor.attr('href')).offset().top
                }, 500);
            });
            /**
             * Bound to the start pomegranate button on the homepage
             */
            $('#appStarter').on('click', function (event) {
                location.href = "/pomegranate";
            });
        }
    };
})();


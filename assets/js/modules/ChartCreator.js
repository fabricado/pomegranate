var RandomColor = require('randomcolor');
var _ = require('lodash');
var moment = require('moment');

var chart;
var lifetimeChart;

var ChartCreator = {
    makeCumulativeReturnChart: function (containerID, crData) {
        console.log(containerID);
        console.log(crData);
        if (typeof crData.data === 'undefined') return;
        if (typeof crData.data.cumulativeReturn_average === 'undefined') return;
        var crAverage = crData.data.cumulativeReturn_average;
        var data = [];
        for (var i = 0; i < crAverage.length; ++i) {
            var day = {"Rel_Day": i + parseInt(crData.req.lowerWindow), "CR_AV": crAverage[i]};
            var j = 0;
            for (j = 0; j < crData.data.eventData.length; ++j) {
                day[crData.data.eventData[j].ric + " " + crData.data.eventData[j].date] =
                    crData.data.eventData[j].cumulativeReturns[i];
            }
            data.push(day);
        }

        var graphs = [{
            "title": "CR_AV",
            "id": "g1",
            "balloonText": "CR_AV<br>Rel Day: [[category]]<br><b><span style='font-size:14px;'>[[value]]</span></b>",
            "bullet": "round",
            "bulletSize": 3,
            "lineColor": "black",
            "lineThickness": 2,
            // "negativeLineColor": "#637bb6",
            "valueField": "CR_AV",
            "legendPeriodValueText": "",
            "legendValueText": ""
        }];
        var gCount = 2;
        for (i = 0; i < crData.data.eventData.length;++i) {
            var bEventVars = "";
            for (var variable in crData.data.eventData[i].variables) {
                bEventVars += "<br>" + variable + ": " + crData.data.eventData[i].variables[variable];
            }
            graphs.push({
                "title": crData.data.eventData[i].ric + " " + crData.data.eventData[i].date,
                "id": "g"+ gCount,
                "balloonText": crData.data.eventData[i].ric + " " + crData.data.eventData[i].date +
                        "<br>Volatility: " + crData.data.eventData[i].coefficientOfVariation +
                        "<br>Rel Day: [[category]]<br><b><span style='font-size:14px;'>[[value]]</span></b>" + bEventVars,
                "bullet": "round",
                "bulletSize": 3,
                "lineColor": RandomColor(),
                "lineThickness": 2,
                "valueField": crData.data.eventData[i].ric + " " + crData.data.eventData[i].date,
                "legendPeriodValueText": " ",
                "legendValueText": " "
            });
            ++gCount;
        }

        graphs.push({
            title: "All",
            id: "all",
            legendValueText: " ",
            legendPeriodValueText: " "
        });

        chart = AmCharts.makeChart(containerID, {
            "type": "serial",
            "theme": "light",
            "marginTop": 0,
            "marginRight": 10,
            "dataProvider": data,
            //"legend": {
            //    "equalWidths": true,
            //    "position": "top",
            //    "valueAlign": "left",
            //},
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left",
                'title': "Cumulative Return"
            }],
            "graphs": graphs,
            "chartScrollbar": {
                "gridAlpha": 0,
                "color": "#888888",
                "scrollbarHeight": 55,
                "backgroundAlpha": 0,
                "selectedBackgroundAlpha": 0.1,
                "selectedBackgroundColor": "#888888",
                "graphFillAlpha": 0,
                "autoGridCount": true,
                "selectedGraphFillAlpha": 0,
                "graphLineAlpha": 0.2,
                "graphLineColor": "#c2c2c2",
                "selectedGraphLineColor": "#888888",
                "selectedGraphLineAlpha": 1

            },
            "valueScrollbar":{
                "offset":15
            },
            "chartCursor": {
                "categoryBalloonDateFormat": "YYYY",
                "cursorAlpha": 0,
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true,
                "valueLineAlpha": 0.5,
                "fullWidth": true,
                "oneBalloonOnly": (graphs.length > 3)
            },
            "categoryField": "Rel_Day",
            "categoryAxis": {
                "parseDates": false,
                "minorGridAlpha": 0.1,
                "minorGridEnabled": true,
                "guides": [
                        {
                            category: "0",
                            lineAlpha: 1,
                            lineThickness: 2,
                            lineColor: "#000000",
                            inside: true
                        }
                    ]
            },
            "export": {
                "enabled": true,
                "position":"top-right"
            }
        });

        chart.addListener("rendered", zoomChart);
        if (chart.zoomChart) {
            chart.zoomChart();
        }

        //chart.legend.addListener('hideItem',legendHandler);
        //chart.legend.addListener('showItem',legendHandler);

        //function legendHandler(evt) {
        //    var state = evt.dataItem.hidden;
        //    if ( evt.dataItem.id == 'all' ) {
        //        for ( var i1 in evt.chart.graphs ) {
        //            if ( evt.chart.graphs[i1].id != 'all' ) {
        //                evt.chart[evt.dataItem.hidden?'hideGraph':'showGraph'](evt.chart.graphs[i1]);
        //            }
        //        }
        //    }
        //}

        function zoomChart() {
            chart.zoomToIndexes(Math.round(chart.dataProvider.length * 0.4), Math.round(chart.dataProvider.length * 0.55));
        }
    },

    makeSingleEventLifetimeChart: function (containerID,eventStudyData,mainDataRic) {
        var i = 0;
        var dataSets = [];
        var eventData = eventStudyData.results.eventData;
        var randomColors = RandomColor({
            count: eventData.length,
            luminosity: 'bright'
        });
        var guideDate = {};
        var mainDataSet = null;
        var startRelDay = parseInt(eventStudyData.request.lowerWindow);
        for (i = 0; i < eventData.length; ++i) {
            var startDate = moment(eventData[i].date,'DD-MM-YYYY');
            var currData = [];
            var j = 0;
            for (j = 0; j < eventData[i].cumulativeReturns.length;++j) {
                var date = startDate.clone().add(startRelDay+j,'days').toDate();
                if (startRelDay + j == 0) {
                    guideDate = date;
                }
                currData.push({
                    date: date,
                    value: eventData[i].cumulativeReturns[j]
                });
            }
            var dataSet = {
                title: eventData[i].ric,
                fieldMappings: [{
                    fromField: 'value',
                    toField: 'value'
                }],
                dataProvider: currData,
                categoryField: "date",
                color: randomColors.pop()
            };
            dataSets.push(dataSet);
            if (dataSet.title == mainDataRic) {
                mainDataSet = dataSet;
            }
        }
        if (mainDataSet == null) {
            mainDataSet = dataSets[0];
        }

        console.log("GUIDE",guideDate);
        var singleEventGraph = AmCharts.makeChart(containerID,{
            "type": "stock",
            "dataSets": dataSets,
            "mainDataSet": mainDataSet,
            "panels": [ {
                "showCategoryAxis": true,
                "title": "Stock Data",
                "percentHeight": 70,
                "stockGraphs": [ {
                    "id": "g1",
                    "valueField": "value",
                    "comparable": true,
                    "compareField": "value",
                    "lineThickness":'2',
                    "lineAlpha": '1',
                    "balloonText": "[[title]]:<b>[[value]]</b>",
                    "compareGraphBalloonText": "[[title]]:<b>[[value]]</b>"
                } ],
                "stockLegend": {
                    "periodValueTextComparing": "[[percents.value.close]]%",
                    "periodValueTextRegular": "[[value.close]]"
                },
                "categoryAxis": {
                    "guides": [
                        {
                            date: guideDate,
                            lineAlpha: 1,
                            lineThickness: 2,
                            lineColor: "#000000",
                            inside: true
                        }
                    ]
                }
            }],

            "chartScrollbarSettings": {
                "graph": "g1"
            },

            "chartCursorSettings": {
                "valueBalloonsEnabled": true,
                "fullWidth": true,
                "cursorAlpha": 0.1,
                "valueLineBalloonEnabled": true,
                "valueLineEnabled": true,
                "valueLineAlpha": 0.5
            },

            "periodSelector": {
                "position": "left",
                "periods": [ {
                    "period": "MM",
                    "count": 1,
                    "label": "1 month"
                }, {
                    "period": "YYYY",
                    "count": 1,
                    "label": "1 year"
                }, {
                    "period": "YTD",
                    "label": "YTD"
                }, {
                    "period": "MAX",
                    "label": "MAX",
                    "selected": true
                } ]
            },

            "dataSetSelector": {
                "position": "left"
            },

            "export": {
                "enabled": true
            }
        });

        return singleEventGraph;
    },

    makeLifetimeChart: function (containerID, lifetimeData, eventData, eventSelected, clickHandler) {
        var i = 0;
        var dataSetsMap = [];
        var dataSets = [];
        var mainDataSet = null;
        var guides = [];
        var randomColors = RandomColor({
            count: _.keys(lifetimeData).length,
            luminosity: 'bright'
        });
        _.forOwn(lifetimeData, function (value, key) {
            var startDate = lifetimeData[key].startDate.clone();
            var currData = [];
            for (i  = 0; i < lifetimeData[key].values.length;++i) {
                currData.push({
                    date: startDate.clone().add(i,'days').toDate(),
                    value: lifetimeData[key].values[i]
                });
            }
            var dataSet = {
                title: key,
                fieldMappings: [{
                    fromField: "value",
                    toField: "value"
                }],
                "dataProvider": currData,
                "categoryField": "date",
                "color": randomColors.pop(),
                stockEvents : []
            };
            dataSets.push(dataSet);
            if (dataSet.title === eventSelected.ric) {
                mainDataSet = dataSet;
            }
            dataSetsMap[key] = dataSet;
        });
        if (mainDataSet == null) {
            mainDataSet = dataSets[0];
        }
        //console.log("IN HERE NOW");
        //console.log(mainDataSet);

        //loop over events,
        //create the stockEvent object and
        //add it to the relevant
        for (i = 0; i < eventData.length; ++i) {
            //console.log(eventData[i]);
            if (!(eventData[i].ric in dataSetsMap)) continue;

            var desc = "<strong>" + eventData[i].date + "</strong><br>";
            for (var variable in eventData[i].variables) {
                if (eventData[i].variables.hasOwnProperty(variable)) {
                    desc += variable + ": " + eventData[i].variables[variable] + "<br>";
                }
            }

            var stockEvent;
            var theDate = moment(eventData[i].date,'DD-MMM-YYYY').toDate();
            //console.log(eventSelected.date);
            //console.log(eventData[i].date);
            if (eventSelected.date == eventData[i].date && eventSelected.ric == eventData[i].ric) {
                console.log("DATE CHECK SUCCESS");
                stockEvent = {
                    date: theDate,
                    backgroundColor: "red",
                    backgroundAlpha: 1,
                    type: "pin",
                    borderColor: '#000000',
                    text: " ",
                    graph: "g1",
                    description: desc
                };
                guides.push({
                    date: theDate,
                    lineAlpha: 0.8,
                    lineThickness: 1.7,
                    inside: true,
                    lineColor: "red"
                });
            } else {
                stockEvent = {
                    date: theDate,
                    // backgroundColor: "red",
                    backgroundAlpha: 1,
                    type: "pin",
                    borderColor: '#000000',
                    text: " ",
                    graph: "g1",
                    description: desc
                };
            }

            //console.log("Adding Stock Event");
            dataSetsMap[eventData[i].ric].stockEvents.push(stockEvent);
        }

        lifetimeChart = AmCharts.makeChart(containerID,{
            "type": "stock",
            "dataSets": dataSets,
            "mainDataSet": mainDataSet,
            "panels": [ {
                "showCategoryAxis": true,
                "title": "Stock Data",
                "percentHeight": 70,
                "stockGraphs": [ {
                    "id": "g1",
                    "valueField": "value",
                    "comparable": true,
                    "compareField": "value",
                    "lineThickness":'2',
                    "lineAlpha": '1',
                    "balloonText": "[[title]]:<b>[[value]]</b>",
                    "compareGraphBalloonText": "[[title]]:<b>[[value]]</b>"
                } ],
                "stockLegend": {
                    "periodValueTextComparing": "[[percents.value.close]]%",
                    "periodValueTextRegular": "[[value.close]]"
                },
                "categoryAxis": {
                    "guides": guides
                }
            }],

            "chartScrollbarSettings": {
                "graph": "g1"
            },

            "chartCursorSettings": {
                "valueBalloonsEnabled": true,
                "fullWidth": true,
                "cursorAlpha": 0.1,
                "valueLineBalloonEnabled": true,
                "valueLineEnabled": true,
                "valueLineAlpha": 0.5
            },

            "periodSelector": {
                "position": "left",
                "periods": [ {
                    "period": "MM",
                    "count": 1,
                    "label": "1 month"
                }, {
                    "period": "YYYY",
                    "count": 1,
                    "label": "1 year"
                }, {
                    "period": "YTD",
                    "label": "YTD"
                }, {
                    "period": "MAX",
                    "label": "MAX",
                    "selected": true
                } ]
            },

            "dataSetSelector": {
                "position": "left"
            },

            "export": {
                "enabled": true
            }
        });

        if (clickHandler) {
            AmCharts.myHandleMove = function ( event ) {
                if ( undefined === event.index)
                    return;
                AmCharts.myCurrentValue = event.chart.dataProvider[event.index].dataContext.value; //valueOpen is the closing price on day shown
                AmCharts.myCurrentPosition = event.chart.dataProvider[event.index].dataContext.date;
                AmCharts.myCurrentPosition.setHours( 12, 0, 0 ); // set it to middle of the day
            };
            lifetimeChart.panels[0].chartCursor.addListener("changed", AmCharts.myHandleMove);
            lifetimeChart.panels[0].chartDiv.onclick = clickHandler;
        }
    },

    makeExternalDataChart: function (containerID, lifetimeData, eventData, eventSelected, clickHandler) {
        var i = 0;
        var dataSetsMap = [];
        var dataSets = [];
        var mainDataSet = null;
        var guides = [];
        var randomColors = RandomColor({
            count: _.keys(lifetimeData).length,
            luminosity: 'bright'
        });

        _.forOwn(lifetimeData, function (value, key) {
            console.log(lifetimeData[key]);
            var startDate = lifetimeData[key].startDate.clone();
            
            var period = "days";
            if (lifetimeData[key].period == "monthly") {
                period = "months";
            }
            var jump = lifetimeData[key].jump;

            console.log(period, jump);

            var currData = [];
            for (i  = 0; i < lifetimeData[key].values.length;++i) {
                currData.push({
                    date: startDate.clone().add(i*jump, period).toDate(),
                    value: lifetimeData[key].values[i]
                });
            }
            var dataSet = {
                title: key,
                fieldMappings: [{
                    fromField: "value",
                    toField: "value"
                }],
                "dataProvider": currData,
                "categoryField": "date",
                "color": randomColors.pop(),
                stockEvents : []
            };
            dataSets.push(dataSet);
            if (dataSet.title === eventSelected.ric) {
                mainDataSet = dataSet;
            }
            dataSetsMap[key] = dataSet;
        });
        if (mainDataSet == null) {
            mainDataSet = dataSets[0];
        }
        //console.log("IN HERE NOW");
        //console.log(mainDataSet);

        //loop over events,
        //create the stockEvent object and
        //add it to the relevant
        for (i = 0; i < eventData.length; ++i) {
            //console.log(eventData[i]);
            if (!(eventData[i].ric in dataSetsMap)) continue;

            var desc = "<strong>" + eventData[i].date + "</strong><br>";
            for (var variable in eventData[i].variables) {
                if (eventData[i].variables.hasOwnProperty(variable)) {
                    desc += variable + ": " + eventData[i].variables[variable] + "<br>";
                }
            }

            var stockEvent;
            var theDate = moment(eventData[i].date,'DD-MMM-YYYY').toDate();
            //console.log(eventSelected.date);
            //console.log(eventData[i].date);
            if (eventSelected.date == eventData[i].date && eventSelected.ric == eventData[i].ric) {
                console.log("DATE CHECK SUCCESS");
                stockEvent = {
                    date: theDate,
                    backgroundColor: "red",
                    backgroundAlpha: 1,
                    type: "pin",
                    borderColor: '#000000',
                    text: " ",
                    graph: "g1",
                    description: desc
                };
                guides.push({
                    date: theDate,
                    lineAlpha: 0.8,
                    lineThickness: 1.7,
                    inside: true,
                    lineColor: "red"
                });
            } else {
                stockEvent = {
                    date: theDate,
                    // backgroundColor: "red",
                    backgroundAlpha: 1,
                    type: "pin",
                    borderColor: '#000000',
                    text: " ",
                    graph: "g1",
                    description: desc
                };
            }

            //console.log("Adding Stock Event");
            dataSetsMap[eventData[i].ric].stockEvents.push(stockEvent);
        }

        lifetimeChart = AmCharts.makeChart(containerID,{
            "type": "stock",
            "dataSets": dataSets,
            "mainDataSet": mainDataSet,
            "panels": [ {
                "showCategoryAxis": true,
                "title": "Stock Data",
                "percentHeight": 70,
                "stockGraphs": [ {
                    "id": "g1",
                    "valueField": "value",
                    "comparable": true,
                    "compareField": "value",
                    "lineThickness":'2',
                    "lineAlpha": '1',
                    "balloonText": "[[title]]:<b>[[value]]</b>",
                    "compareGraphBalloonText": "[[title]]:<b>[[value]]</b>"
                } ],
                "stockLegend": {
                    "periodValueTextComparing": "[[percents.value.close]]%",
                    "periodValueTextRegular": "[[value.close]]"
                },
                "categoryAxis": {
                    "guides": guides
                }
            }],

            "chartScrollbarSettings": {
                "graph": "g1"
            },

            "chartCursorSettings": {
                "valueBalloonsEnabled": true,
                "fullWidth": true,
                "cursorAlpha": 0.1,
                "valueLineBalloonEnabled": true,
                "valueLineEnabled": true,
                "valueLineAlpha": 0.5
            },

            "periodSelector": {
                "position": "left",
                "periods": [ {
                    "period": "MM",
                    "count": 1,
                    "label": "1 month"
                }, {
                    "period": "YYYY",
                    "count": 1,
                    "label": "1 year"
                }, {
                    "period": "YTD",
                    "label": "YTD"
                }, {
                    "period": "MAX",
                    "label": "MAX",
                    "selected": true
                } ]
            },

            "dataSetSelector": {
                "position": "left"
            },

            "export": {
                "enabled": true
            }
        });

        if (clickHandler) {
            AmCharts.myHandleMove = function (event) {
                if ( undefined == event.index ) {
                    return;
                }
                AmCharts.myCurrentPosition = event.chart.dataProvider[event.index].dataContext.date;
                AmCharts.myCurrentPosition.setHours(12, 0, 0);
            }
            lifetimeChart.panels[0].chartCursor.addListener("changed", AmCharts.myHandleMove);
            lifetimeChart.panels[0].chartDiv.onclick = clickHandler;
        }

    },

    changeDataSet: function (setRIC) {
        var mds = {};
        //console.log (lifetimeChart);
        for (var currset in lifetimeChart.dataSets) {
            //console.log("CURRSET ");
            //console.log(lifetimeChart.dataSets[currset].title);
            if (lifetimeChart.dataSets[currset].title == setRIC) {
                mds = lifetimeChart.dataSets[currset];
            }
        }
        if (mds != {}) {
            lifetimeChart.mainDataSet = mds;
            lifetimeChart.validateNow();
        }
    }
};

module.exports = ChartCreator;


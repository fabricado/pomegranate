var async = require('async');
var moment = require('moment');

/**
 * This module can be used to receive data from the server
 {[{ric: String,date: Date, variables: {VarName: Number}}]}
 */
var DataProcessor = {
    processEventsJson: function (eventsJson) {
        var events = [];
        for (var i in eventsJson.events) {
            var e = eventsJson.events[i];
            var newEvent = {'ric': e['#RIC'], 'date': e["Event Date"], 'variables':{}}
            for (var j in eventsJson.eventVars) {
                var v = eventsJson.eventVars[j];
                if (e[v]) {
                    newEvent.variables[v] = e[v];
                } else {
                    newEvent.variables[v] = 0;
                }
            }
            events.push(newEvent);
        }
        return events;
    }
};

module.exports = DataProcessor;

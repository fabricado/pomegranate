/*
 The ContactPage module handles events on the contacts page

 The init function should be called on module load
 */

var $ = require('jquery');

module.exports = (function () {
    return {
        init: function () {
            var module = this;
            $(document).ready(function () {
                module.bindUI();
            });
        },

        bindUI: function () {
            $("body").on("input propertychange", ".floating-label-form-group", function (e) {
                $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
            }).on("focus", ".floating-label-form-group", function () {
                $(this).addClass("floating-label-form-group-with-focus");
            }).on("blur", ".floating-label-form-group", function () {
                $(this).removeClass("floating-label-form-group-with-focus");
            });

            $('#contactForm').submit(function (event) {
                event.preventDefault();
                console.log($(this).serialize());
                console.log("SENDING REQ");
                $.ajax({
                    url: '/contact/send',
                    type: 'post',
                    data: $(this).serialize(),
                    success: function () {
                        var successDiv = $('#success');
                        $(successDiv).html("<div class='alert alert-success'>");
                        $(successDiv).find('> .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                            .append("</button>");
                        $(successDiv).find('> .alert-success')
                            .append("<strong>Your message has been sent. Our developers will follow up with you soon!</strong>");
                        $(successDiv).find('> .alert-success')
                            .append('</div>');

                        //clear all fields
                        $('#contactForm').trigger("reset");
                    },
                    error: function () {
                        // Fail message
                        var successDiv = $('#success');
                        $(successDiv).html("<div class='alert alert-danger'>");
                        $(successDiv).find('> .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                            .append("</button>");
                        $(successDiv).find('> .alert-danger').append("<strong>Sorry it seems that my mail server is not responding. Please try again later!");
                        $(successDiv).find('> .alert-danger').append('</div>');
                        //clear all fields
                        $('#contactForm').trigger("reset");
                    }
                });
            });
        }
    }
})();

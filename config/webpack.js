// config/webpacik.js

var webpack = require('webpack');
var path = require('path');

// compile js assets into a single bundle file
module.exports.webpack = {
    options: {
        devtool: 'eval',
        entry: [
            './assets/js/main.js'
        ],
        output: {
            path: '.tmp/public/js',
            filename: 'bundle.js'
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new webpack.NoErrorsPlugin()
        ],
        module: {
            loaders: [
                // requires "npm install --save-dev babel-loader"
                {
                    test: /\.js$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/,
                    query: {
                        presets: ['es2015', 'react']
                    }
                }
            ]
        }
    }
};

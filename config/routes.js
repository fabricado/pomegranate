/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

    /***************************************************************************
     *                                                                          *
     * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
     * etc. depending on your default view engine) your home page.              *
     *                                                                          *
     * (Alternatively, remove this and add an `index.html` file in your         *
     * `assets` directory)                                                      *
     *                                                                          *
     ***************************************************************************/
    'GET /': 'PagesController.index',
    'GET /documentation' : 'PagesController.documentation',
    'GET /releases': 'PagesController.releases',
    'GET /contact' : 'PagesController.contact',

    /**
     * Routes for contacts form
     */
    'POST /contact/send': 'ContactController.notifyDevs',

    /***************************************************************************
     *                                                                          *
     * Custom routes here...                                                    *
     *                                                                          *
     * If a request to a URL doesn't match any of the custom routes above, it   *
     * is matched against Sails route blueprints. See `config/blueprints.js`    *
     * for configuration options and examples.                                  *
     *                                                                          *
     ***************************************************************************/

    /**
     * Event Study API routes
     */
    'GET /api/es': 'APIController.es_get',
    'POST /api/stockFile' : 'APIController.es_post_stockfile',
    'POST /api/eventFile' : 'APIController.es_post_eventfile',
    'POST /api/event': 'APIController.event_post',
    'GET /api/event' : 'APIController.event_get',
    'GET /api/serverStocksList': 'APIController.server_stocks_list_get',
    'GET /api/profile': 'APIController.company_profile_get',
    'GET /api/serverStockLifetime': 'APIController.server_stock_lifetime_get',
    'DELETE /api/event': 'APIController.event_delete',

    /**
     * External api routes
     */
    'GET /api/news': 'APIController.news_get',

    /**
     * External Factor data routes
     */
    'GET /interestRate': 'APIController.interest_rate_get',
    'GET /exchangeRate': 'APIController.exchange_rate_get',

    /**
     * State/Session routes
     */
    'GET /session/active': 'SessionController.active_get',
    'POST /session/active': 'SessionController.active_post',
    'GET /session/token': 'SessionController.token_get',
    'POST /session/token': 'SessionController.token_post',
    'GET /session/state': 'SessionController.state_get',
    'POST /session/state': 'SessionController.state_post',

    /**
     * Pomegranate app routes
     */
    'GET /pomegranate': 'PomegranateController.index',
    'GET /pomegranate/filesInSession': 'PomegranateController.filesInSession',
    'GET /pomegranate/eventVariablesInSession': 'PomegranateController.eventVariablesInSession',
    'POST /pomegranate/makeFakeEvent': 'PomegranateController.makeDummyEventFile',

    /**
     * Auth routes
     */
    'POST /login': 'AuthController.process',
    'POST /logout':'AuthController.logout',
    'GET /loginStatus': 'AuthController.loginStatus'
};

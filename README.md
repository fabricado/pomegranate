# Pomegranate by Fabricado

This application is the SENG3011 project for team Fabricado. It is written in [SailsJs](http://sailsjs.org) with the frontend
app completely made using [ReactJs](http://facebook.github.io/react/) and the [Flux](https://facebook.github.io/flux/) pattern.

The team consists of -

* Adishwar Rishi
* Aman Singh
* Nathan Jones
* Allan Allaf
* Andy Seah

## Install instructions

In order to run this application, you need to have the following software -

1. [NodeJs](http://nodejs.org/) - A javascript runtime
2. [SailsJs](http://sailsjs.org) - An MVC framework for NodeJS
3. [Bower](http://bower.io/) - A package manager for the web
4. [Ruby](http://www.ruby-lang.org/) v2.3.0 and [Bundler](http://bundler.io/) - A Ruby package manager

Once you have NodeJs and Ruby downloaded, SailsJS Bower and SASS can be installed with the following commands
```
npm install -g sails #Installs SailsJS

npm install -g bower #Installs bower

gem install bundler #installs a package manager for ruby
```

If the above has succeeded, go into the root folder of the project and run the following
```
npm install

bower install

bundle install
```

Great work! You should now be able to run our application!

In order to start it up, please run
```
sails lift
```

**Note: A hosted version of this applicaton can be found [Over here!](http://fabricado.herokuapp.com/)**

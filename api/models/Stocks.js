/**
 * Stocks.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
      id: {
          type: "integer",
          primaryKey: true,
          autoIncrement: true,
          unique: true
      },

      userId: {
          model: 'user'
      },

      ric: {
          type: 'string'
      },

      buyInDate: {
          type: 'date'
      },

      buyInPrice: {
          type: 'float'
      },

      amount: {
          type: 'integer'
      },

      providedByUser: {
          type: 'boolean',
          defaultsTo: function () {
              return false;
          }
      }
  }
};


/**
 * User.js
 *
 * @description :: This model represents a registered user
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var bcrypt = require('bcrypt');

module.exports = {

    attributes: {
        id: {
            type: "integer",
            primaryKey: true,
            autoIncrement: true,
            unique: true
        },

        username: {
            type: 'string',
            required: true
        },

        email: {
            type: 'string',
            unique: true,
            required: true
        },

        password: {
            type: 'string',
            required: true
        },

        portfolioStocks: {
            collection:'stocks',
            via: 'userId'
        },

        providedStocks: {
            collection: 'userprovidedstocks',
            via: 'userId'
        },

        eventFileName: {
            type: 'string'
        },

        stockFileName: {
            type: 'string'
        }
    },

    //  Callback to run before a user is created (to set a hashed password).
    beforeCreate: function (user, cb) {
        //  Execute 10 rounds of processing.
        bcrypt.genSalt(10, function (err, salt) {
            //  Hash the provided password.
            bcrypt.hash(user.password, salt, function (err, hash) {
                //  Error checks.
                if (err) {
                    console.log("ERROR", err);
                    cb(err);
                } else {
                    //  Store the hashed password.
                    user.password = hash;
                    //  Run configured callback.
                    cb();
                }
            });
        });
    }
};


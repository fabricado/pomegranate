/**
 * PagesController
 *
 * @description :: Server-side logic for managing Pages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var fs = require('fs');
var _ = require('lodash');

module.exports = {
    //function to handle call to '/'
    index: function (req,res) {
        return res.view('home',{
            layout: 'layout_navbar',
            title: 'Fabricado Home'
        });
    },

    //function to handle call to /documentation
    documentation: function (req, res) {
        return res.view('documentation', {
            layout: 'layout_navbar',
            title: 'Product Documentation'
        });
    },

    //function to handle call to '/releases'
	releases: function (req, res) {

        var input = FileHelper.findReleases();
        return res.view('releases',{
            layout: 'layout_navbar',
            title: 'Fabricado Releases',
            releaseData: input,
            _: _,
            versionComp: UtilHelper.versionComp
        });
    },

    //function to handle call to '/contact'
    contact: function (req, res) {
        return res.view('contact_us', {
            layout: 'layout_navbar',
            title: 'Contact Us'
        });
    }
};


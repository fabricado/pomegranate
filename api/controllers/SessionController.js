/**
 * APIController
 *
 * @description :: Server-side logic for managing APIS
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var async = require('async');
var fs = require('fs');

module.exports = {

    active_get: function(req, res) {
        var active = SessionInterface.session_get_active(req.session.id);
        //console.log("active: ", active);
        return res.json({token: active});
    },

    active_post: function(req, res) {
        if (req.param('token') != null) {
            //token given, store in that folder
            token = req.query.token;
        } else {
            //console.log("No Token!");
            return res.badRequest({
                        "code": 400,
                        "message": "No Token Given"
                    }); 
        }
    },

    token_get: function (req, res) {
        //console.log("token get");
        var newToken = SessionInterface.session_generate_new(req.session.id);
        var result = {token: newToken};

        SessionInterface.session_set_active(req.session.id, newToken);
        return res.json(result);
    },

    token_post: function (req, res) {
        var token = req.body.token;
        if (!token) {
            res.badRequest({
                        "code": 400,
                        "message": "No Token supplied."
                    });
        }

        if (!fs.existsSync(sails.config.appPath + "/.tmp/uploads/" + token + '/')) {
            return res.badRequest({
                        "code": 400,
                        "message": "Invalid Token supplied."
                    });
        }

        SessionInterface.session_set_active(req.session.id, token);
        return res.json({"message": "Session Updated"});
    },

    state_get: function (req, res) {
        //console.log("state get");
        var token = '';
        if (req.param('token') != null) {
            //token given, store in that folder
            token = req.query.token;
        } else {
            //console.log("No Token!");
            return res.badRequest({
                        "code": 400,
                        "message": "No Token Given"
                    }); 
        }
        //console.log("state_get, token: ", token);
        if (!fs.existsSync(sails.config.appPath + "/.tmp/uploads/" + token + '/')) {
            //console.log("No Folder exists, invalid token?");
            return res.badRequest({
                        "code": 400,
                        "message": "Invalid Token Given"
                    }); 
        }

        var state = SessionInterface.session_get_state(token);
        //console.log("Returning from state_get");
        return res.json(state);
    },

    state_post: function (req, res) {
        //console.log("state post");
        //console.log(req.body);
        var token = req.body.token;
        if (!token) {
            res.badRequest({
                        "code": 400,
                        "message": "No Token supplied."
                    });
        }

        var stateFileDir = sails.config.appPath + "/.tmp/uploads/" + token + "/state.json";

        //console.log(req.body.state);
        fs.writeFile(stateFileDir, req.body.state, (err) => {
            if (err) {
                //console.log("File not written.");
                return res.badRequest({
                        "code": 400,
                        "message": "Error occured when writing to file."
                    });
            } else {
                //console.log("File written");
                return res.send(200);
            }
        });
    }
};


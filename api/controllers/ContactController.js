/**
 * ContactController
 *
 * @description :: Server-side logic for managing Contacts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    /**
     * Handles post requests to 'contact/send'
     * Sends an email to the required people
     * Assumes that the parameters passed in are - 'name', 'email', 'message'
     */
    notifyDevs: function (req,res) {;
        var sendgrid  = require('sendgrid')('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');
        sendgrid.send({
            to:       ['adiswa123@gmail.com','adudez96@gmail.com'],
            from:     req.param('email'),
            subject:  'Communication from ' + req.param('name'),
            text:     req.param('message')
        }, function(err, json) {
            if (err) {
                return res.negotiate(err);
            } else {
                return res.json(200,json);
            }
        });
    }
};


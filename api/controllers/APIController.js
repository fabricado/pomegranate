/**
 * APIController
 *
 * @description :: Server-side logic for managing APIS
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var child_process = require('child_process');
var request = require('request');
var async = require('async');
var fs = require('fs');
var CsvConverter = require('csvtojson').Converter;

module.exports = {
    /**
     * The parameters req expects are 'lowerWindow', 'upperWindow', 'eventTypeBounds'
     * 'eventTypeBounds' is an array and is optional
     *
     *
     * extra parameters:
     * mode: <eventStudy, eventStudyPercent, analyse, listEvents, getRIC, all>
     *
     *    eventStudy        - perform normal event study, return absolute cumulative returns with buyin at the start of file
     *
     *    eventStudyPercent - perform event study, return percentage cumulative returns with buyin at the start of the file
     *
     *    analyse           - return list of RICs, event variables and their bounds
     *
     *    listEvents        - list the events that occur on the given stock data
     *
     *    getRIC            - get the lifetime stock price of the specified RIC
     *                      - requires RIC parameter to be passed
     *
     *    all               - perform normal event study, return open, high, low, close values within the event windows
     *
     * other parameters:
     *
     *    randomDate        - does an event study around the date passed in this parameter (format: dd-mm-yy)
     *                      - only works in conjunction with eventStudy/eventStudyPercent modes
     *
     *    buyInDate         - does an event study but cumulative returns are calculated against the supplied buy in date (format: dd-mm-yy)
     *                      - requires the RIC parameter to be passed
     *                      - only works in conjunction with eventStudy/eventStudyPercetn modes
     */
    es_get: function (req, res) {
        //console.log(req.allParams()); //returns an object with all parameters

        //check mode parameter
        var mode = "eventStudy";
        if (req.query.mode != null) {
            mode = req.query.mode;
        }

        //check rel flag
        var relFlag = false;
        if (req.query.relFlag != null) {
            relFlag = req.query.relFlag === 'true';
        }

        var singleEvent = false;
        if (req.query.singleEvent != null) {
            singleEvent = req.query.singleEvent === 'true';
        }

        var buyInDate = null;
        if (req.query.buyInDate != null) {
            buyInDate = req.query.buyInDate;
        }

        var RIC = "";
        if (mode == "getRIC") {
            if (req.query.RIC != null) {
                RIC = req.query.RIC;
            } else {
                mode = "eventStudy";
            }
        } else if (buyInDate) {
            if (req.query.RIC != null) {
                RIC = req.query.RIC;
            } else {
                buyInDate = null;
            }
        }

        var randomDate = null;
        if (req.query.randomDate != null) {
            randomDate = req.query.randomDate;
        }

        console.log(req.allParams());
        console.log(req.user.id);
        var result = APIInterface.es_request(req.user.id, mode, relFlag, singleEvent, RIC, buyInDate, randomDate, req.param('lowerWindow'), req.param('upperWindow'), req.param('eventTypeBounds', []));

        return res.json(result);
    },

    es_post_eventfile: function (req, res) {
        //dir is created upon user creation
        var files_dir = AppConstants.base_path + '/' + req.user.id + "/";

        req.file('eventFile').upload({
            //saves file to .tmp/uploads
            saveAs: ('eventFile'),
            dirname: (files_dir)
            // onProgress: function (state) {
            //     console.log("STILL PROGRESSING");
            //     console.log(state.id, "bytes written ", state.written);
            // }
        }, function whenDone(err, uploadedFiles) {
            if (err) {
                //if errors exist, push them to our errors array
                return res.negotiate(err);
            } else {
                if (uploadedFiles.length !== 1) {
                    //if no files were provided for the param
                    //we push a 400 error
                    return res.badRequest({
                        "code": 400,
                        "message": "A file was not provided in parameter eventFile"
                    });
                } else {
                    //instead of storing the filename in a file as before, we now store it in the database
                    User.update({id: req.user.id}, {eventFileName: req.file('eventFile')._files[0].stream.filename},
                        function (err, updated) {
                            if (err) return res.negotiate(err);
                        });
                    var csvToJson = new CsvConverter({trim: true});
                    console.log(uploadedFiles[0].fd);
                    csvToJson.fromFile(uploadedFiles[0].fd, function (err, result) {
                        EventFileManager.update(req.user.id, result, function (err) {
                            if (err) return res.negotiate(err);
                            return res.send(200, {'response': 'eventFile was accepted'});
                        });
                    });
                }
            }
        });
    },

    es_post_stockfile: function (req, res) {
        //dir is created upon user creation
        var files_dir = AppConstants.base_path + '/' + req.user.id + "/";

        req.file('stockFile').upload({
            //saves file to .tmp/uploads
            saveAs: ('stockFile'),
            dirname: (files_dir)
            // onProgress: function (state) {
            //     console.log("STILL PROGRESSING");
            //     console.log(state.id, "bytes written ", state.written);
            // }
        }, function whenDone(err, uploadedFiles) {
            if (err) {
                //if errors exist, push them to our errors array
                return res.negotiate(err);
            } else {
                if (uploadedFiles.length !== 1) {
                    //if no files were provided for the param
                    //we push a 400 error
                    return res.badRequest({
                        "code": 400,
                        "message": "A file was not provided in parameter stockFile"
                    });
                } else {
                    //instead of storing the filename in a file as before, we now store it in the database
                    User.update({id: req.user.id}, {stockFileName: req.file('stockFile')._files[0].stream.filename},
                        function (err, updated) {
                            if (err) return res.negotiate(err);
                            return res.send(200, {'response': 'stockFile was accepted'});
                        });
                    UserProvidedStocks.destroy({userId:req.user.id}).exec(function (err) {
                        if (err) {
                            console.log(err);
                        } else {
                            var oldDir = process.cwd();
                            process.chdir(AppConstants.base_path + '/' + req.user.id + "/");
                            var params = " --analyse stockFile eventFile";
                            console.log(AppConstants.getBinPath() + params);
                            child_process.exec(AppConstants.getBinPath() + params,function (err,stdout,stderr) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    var output = JSON.parse(stdout);
                                    var rics = output.rics;
                                    async.each(rics,function (ric,cb) {
                                        UserProvidedStocks.findOrCreate({ric:ric,userId:req.user.id},
                                            {ric:ric,userId:req.user.id}, function (err,createdRecord) {
                                                if (err) {
                                                    console.log(err);
                                                    cb(err);
                                                } else {
                                                    cb();
                                                }
                                            });
                                    }, function (err) {
                                        //when all are done
                                        if (err) console.log(err);
                                    });
                                }
                            });
                            process.chdir(oldDir);
                        }
                    });
                }
            }
        });
    },

    /**
     * Handles POST request to /api/es
     * The post request expects a form entry with two files
     * Stores the files in .tmp/uploads/<req.session.id>/
     * The names are user_stockFile and user_eventFile
     * The function expects that stockFile and eventFile are parameters
     *
     es_post: function (req, res) {
        //console.log("IN POST");
        var fileParam = req.param('fileType');
        //console.log(fileParam);
        response = {'response': fileParam + " was accepted"};

        var session_dir = AppConstants.base_path + '/' + req.user.id + "/";

        if (req.param('token') != null) {
            //token given, store in that folder
            //console.log("Token given: ", req.param('token'));
            session_dir = req.param('token') + '/';
        } else {
            //console.log("No Token!");
            return res.badRequest({
                "code": 400,
                "message": "No Token Given"
            });
        }

        if (!fs.existsSync(sails.config.appPath + "/.tmp/uploads/" + session_dir)) {
            //console.log("No Folder exists, invalid token?");
            return res.badRequest({
                "code": 400,
                "message": "Invalid Token Given"
            });
        }

        req.file(fileParam).upload({
            //saves file to .tmp/uploads
            saveAs: (fileParam),
            dirname: (sails.config.appPath + "/.tmp/uploads/" + session_dir)
            // onProgress: function (state) {
            //     console.log("STILL PROGRESSING");
            //     console.log(state.id, "bytes written ", state.written);
            // }
        }, function whenDone(err, uploadedFiles) {
            if (err) {
                //if errors exist, push them to our errors array
                return res.negotiate(err);
            } else {
                if (uploadedFiles.length !== 1) {
                    //if no files were provided for the param
                    //we push a 400 error
                    return res.badRequest({
                        "code": 400,
                        "message": "A " + fileParam + " was not provided in parameter '" + fileParam + "'"
                    });
                } else {
                    var content = {};
                    //file param is either 'stockFile' or 'eventFile'
                    content[fileParam] = req.file(fileParam)._files[0].stream.filename;
                    //console.log(content);
                    //create file storing given filenames by users
                    fs.writeFile(sails.config.appPath + "/.tmp/uploads/" + session_dir + fileParam + "_name", JSON.stringify(content),
                        function (err) {
                            if (err) {
                                res.negotiate(err);
                            } else {
                                //delete old state file
                                fs.unlink(sails.config.appPath + "/.tmp/uploads/" + session_dir + 'state.json', function (err) {
                                    if (err) console.log(err);
                                });
                                return res.send(200, JSON.stringify(response));
                            }
                        });
                }
            }
        });
    },
     */

    /**
     * get news from pygmy owl api
     */
    news_get: function (req, res) {
        var response = '';

        var post_data = {
            start_date: "2014-10-01T00:00:00.092Z",
            end_date: "2016-10-01T00:00:00.092Z",
            instr_list: req.query.rics,
            tpc_list: ['STX','HOT','RCH','LEN','BLR', 'CMPNY', 'RELX', 'AU', 'CYCS', 'CCOS', 'ENTS', 'ASIA'],
            range_length: 50
        };

        console.log(post_data);

        var options = {
            uri: 'http://pacificpygmyowl.herokuapp.com/api/query',
            method: 'POST',
            json: post_data
        };

        request(options, function (error, response, body) {
            return res.send(200, JSON.stringify(body));
        });
    },

    event_post: function (req, res) {
        //console.log(req.allParams());
        //params expected: "#RIC", "Event Date", and variables
        var e = req.allParams();
        for (var i in e) {
            if (i != "#RIC" && i != "Event Date") {
                //console.log("Converting ", i);
                e[i] = parseInt(e[i]);
            }
        }
        EventFileManager.update(req.user.id, [e], function (err, result) {
                            if (err) return res.negotiate(err);
                            return res.send(200, {'response': 'new event was accepted'});
                        });
    },

    event_delete: function (req, res) {
        //params expected "#RIC", "Event Date", and variables
        var e = req.allParams();
        for (var i in e) {
            if (e.hasOwnProperty(i)) {
                if (i != "#RIC" && i != "Event Date") {
                    //console.log("Converting ", i);
                    e[i] = parseInt(e[i]);
                }
            }
        }
        EventFileManager.deleteEvents(req.user.id, [e], function (err, result) {
                            if (err) return res.negotiate(err);
                            return res.send(200, {'response': 'event was deleted'});
                        });
    },

    event_get: function (req, res) {
        var eventFileDir = AppConstants.base_path + '/' + req.user.id + '/eventFile.json';
        var events = fs.readFile(eventFileDir, 'utf-8', function (err, data) {
            if (err) return res.negotiate(err);
            return res.send(data);
        });
    },

    interest_rate_get: function (req, res) {
        var dataFileDir = sails.config.appPath + '/assets/externalData/cashRates/' + req.query.country + '.json';
        console.log(dataFileDir);
        var events = fs.readFile(dataFileDir, 'utf-8', function (err, data) {
            if (err) return res.negotiate(err);
            return res.send(data);
        });
    },

    server_stocks_list_get: function (req, res) {
        var stockFileDir = sails.config.appPath + '/assets/stockData/storedRics.json';
        var events = fs.readFile(stockFileDir, 'utf-8', function (err, data) {
            if (err) return res.negotiate(err);
            return res.send(data);
        });
    },

    exchange_rate_get: function (req, res) {
        var dataFileDir = sails.config.appPath + '/assets/externalData/exchangeRates/FXR' + req.query.currency + '.json';
        var events = fs.readFile(dataFileDir, 'utf-8', function (err, data) {
            if (err) return res.negotiate(err);
            return res.send(data);
        });
    },

    company_profile_get: function (req, res) {
        var profileFileDir = sails.config.appPath + '/assets/stockData/' + req.query.RIC + '/' + req.query.RIC + '.json';
        var events = fs.readFile(profileFileDir, 'utf-8', function (err, data) {
            if (err) return res.negotiate(err);
            return res.send(data);
        });
    },

    server_stock_lifetime_get: function (req, res) {
        var data = APIInterface.get_server_stock_lifetime(req.query.RIC);
        return res.send(200, data);
    }
};

/**
 * PomegranateController
 *
 * @description :: Server-side logic for managing Pomegranates
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var fs = require('fs');
var child_process = require('child_process');
var async = require('async');
var moment = require('moment');

module.exports = {
    /**
     * Handles GET requests to /pomegranate
     * @param req
     * @param res
     */
	index: function (req, res) {
        return res.view('pomegranate_home',{
            layout: 'layout_pomegranate',
            title: 'Pomegranate'
        });
    },

    /**
     * Handles GET request to /pomegranate/filesInSession
     * Checks if user has uploaded files, returns file names
     * if they exist.
     * @param req
     * @param res
     */
    filesInSession : function (req, res) {
        var filePath = AppConstants.base_path + '/' + req.user.id + "/";
        var result = {stockFile: req.user.stockFileName, eventFile: req.user.eventFileName};
        return res.send(result);
    },

    /**
     * Handles GET request to /pomegranate/eventVariablesInSession
     * Checks the uploaded eventFile and extracts the list of event variables
     * The method returns a JSON list of the variable names if they exist
     * Or an empty object if no files exist in session
     */
    eventVariablesInSession : function (req, res) {
        var fileName = AppConstants.base_path + '/' + req.user.id + '/eventFile';
        var eventVars = { variables: [] };

        //read first line of event file
        //console.log("IN THE EVENT VARIABLES SESSION CHECK FUNCTION THING");
        //console.log(fileName);
        if(fs.existsSync(fileName)) {
            var firstLine = child_process.execSync("head -1 " + fileName) + '';
            firstLine = firstLine.replace(/[\r\n]/g,'');
            //console.log("Beeyoop");
            //console.log(firstLine);
            eventVars.variables = firstLine.split(/\s*,\s*/);
            eventVars.variables.shift();
            eventVars.variables.shift();
            //console.log("EVENT VARIABLES BLAH");
            //console.log(eventVars.variables);
        }

        return res.json(eventVars);
    },

    /**
     * Makes a dummy event file for single event analysis
     */
    makeDummyEventFile: function (req,res) {
        console.log(req.allParams());
        var rics = null;
        var event = null;
        if (req.param('rics') != null && req.param('event') != null) {
            rics = req.param('rics');
            event = req.param('event');
        } else {
            return res.badRequest({
                code: 400,
                "message": "Malformed request"
            });
        }
        //#RIC,Event Date,Cash Rate,China Growth Rate Change,CEO Health issue,Earning announcements,Macroevent,Government Policy changes
        var output = "";
        //first we create the starting line
        output = "#RIC,Event Date";
        var vars = [];
        var date = moment(event.date,'DD-MMM-YYYY');
        for (varName in event.variables) {
            if (event.variables.hasOwnProperty(varName)) {
                output += "," + varName;
                vars.push(varName);
            }
        }
        output += "\n";
        for (var i = 0; i < rics.length; ++i) {
            output += rics[i] + "," + date.format('DD-MMM-YYYY');
            for (var j = 0; j < vars.length; ++j) {
                output += "," + event.variables[vars[j]];
            }
            output += "\n";
        }
        fs.writeFile(AppConstants.base_path + '/' + req.user.id + "/" + "fakeEventFile", output,function (err) {
            if (err) {
                res.negotiate(err);
            } else {
                res.json(200,{'message':'Successfully made file'});
            }
        });
    }

};


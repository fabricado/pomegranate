/**
 * StocksController
 *
 * @description :: Server-side logic for managing Stocks
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var moment = require('moment');
var child_process = require('child_process');

module.exports = {
	create: function (req,res) {
        var input = req.allParams();
        if (input.hasOwnProperty('buyInDate')) {
            var givenDate = moment(input['buyInDate'],'DD-MM-YYYY');
            input['buyInDate'] = givenDate.toDate();
        }
        
        input['userId'] = req.user.id;
        
        Stocks.create(input, function (err,stock) {
            if (err) return res.negotiate(err);
            //if the ric is not supplied by them...
            UserProvidedStocks.find({userId: req.user.id, ric: input.ric}).exec(function (err, records) {
                if (err) return res.negotiate(err);
                if (records.length == 0) {
                    //add the stock, append the file
                    UserProvidedStocks.create({userId: req.user.id, ric: input.ric}).exec(function (err, entry) {
                        //added properly
                        var ricFilePath = sails.config.appPath + '/assets/stockData/' + input.ric + '/' + input.ric + '.csv';
                        var userFilePath = sails.config.appPath + '/.tmp/uploads/' + req.user.id + '/stockFile';
                        child_process.execSync("tail -n +2 " + ricFilePath + '>>' + userFilePath);
                        console.log("tail -n +2 " + ricFilePath + '>>' + userFilePath);
                        return res.json(200,stock);
                    });
                }
            });
        });
    },

    find: function (req,res) {
        Stocks.find({userId: req.user.id}).exec(function (err,records) {
            if (err) res.negotiate(err);
            res.json(200,records);
        });
    },

    destroy: function (req,res) {
        Stocks.destroy({userId: req.user.id,id: req.param('id')}).exec(function (err,destroyed) {
            if (err) res.negotiate(err);
            res.json(200,destroyed);
        });
    }
};


/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var mkdirp = require('mkdirp');

module.exports = {
	create: function (req, res) {
        User.create(req.allParams(),function (err,user) {
            if (err) return res.negotiate(err);
            mkdirp(AppConstants.base_path + "/" + user.id,function (err) {
                if (err) return res.negotiate(err);
                //once we have made the folder, we make the event json file
                EventFileManager.create(user.id,function (err) {
                    if (err) res.negotate(err);
                    return res.send(200,user);
                });
            });

        })
    }
};


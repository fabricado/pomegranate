/**
 * AuthController
 *
 * @description :: Server-side logic for managing Auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var passport = require('passport');
module.exports = {

    login: function (req, res) {
        res.view();
    },
    process: function (req, res) {
        passport.authenticate('local', function (err, user, info) {
            if ((err) || (!user)) {
                return res.send(401,{
                    message: 'login failed',
                    error: err
                });
            }
            req.logIn(user, function (err) {
                if (err) res.send(err);
                return res.send({
                    message: 'login successful',
                    username: user.username
                });
            });
        })(req, res);
    },

    logout: function (req, res) {
        req.logout();
        res.send({message:'logout successful'});
    },

    loginStatus: function (req,res) {
        var response = {isLoggedIn: req.isAuthenticated()};
        if (response.isLoggedIn) {
            response['username'] = req.user.username;
        }
        return res.send(200,response);
    }
};

module.exports.blueprints = {
    actions: true,
    rest: true,

    shortcuts: true
};

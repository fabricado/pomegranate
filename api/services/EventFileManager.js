var fs = require('fs');
var _ = require('lodash');
var converter = require('json-2-csv');
module.exports = (function () {
    return {
        create: function (userId, cb) {
            fs.writeFile(AppConstants.base_path + "/" + userId + "/eventFile.json", JSON.stringify({
                eventVars: [],
                events: []
            }), 'utf-8', cb);
        },

        update: function (userId, givenEventData, cb) {
            fs.readFile(AppConstants.base_path + "/" + userId + "/eventFile.json", 'utf-8', function (err, currentEventData) {
                if (err) {
                    cb(err);
                } else {
                    currentEventData = JSON.parse(currentEventData);
                    var eventVarData = _.difference(_.keys(givenEventData[0]), ['#RIC', 'Event Date']);
                    var newEventVars = _.union(eventVarData, currentEventData.eventVars);
                    var newEvents = _.unionWith(currentEventData.events, givenEventData, _.isEqual);
                    fs.writeFile(AppConstants.base_path + "/" + userId + "/eventFile.json", JSON.stringify({
                        eventVars: newEventVars,
                        events: newEvents
                    }), 'utf-8', function (err) {
                        if (err) {
                            cb(err);
                        } else {
                            this.generateEventFile(userId, cb);
                        }
                    }.bind(this));
                }
            }.bind(this));
        },

        deleteEvents: function (userId, givenEventData, cb) {
            fs.readFile(AppConstants.base_path + "/" + userId + "/eventFile.json", 'utf-8', function (err, currentEventData) {
                if (err) {
                    cb(err);
                } else {
                    currentEventData = JSON.parse(currentEventData);

                    var eventVarData = _.difference(_.keys(givenEventData[0]), ['#RIC', 'Event Date']);

                    //remove event
                    var remainingEvents = _.differenceWith(currentEventData.events, givenEventData, function (e1,e2) {
                        return (e1['#RIC'] === e2['#RIC']) && (e1['Event Date'] === e2['Event Date']);
                    });

                    //keep only used event variables
                    var usedVars = [];
                    for (var v in currentEventData.eventVars) {
                        if (currentEventData.eventVars.hasOwnProperty(v)) {
                            //console.log(v);
                            var usedElsewhere = false;
                            for (var e in remainingEvents) {
                                if (remainingEvents.hasOwnProperty(e)) {
                                   // console.log("  ", e);
                                    if (remainingEvents[e][currentEventData.eventVars[v]]) {
                                        usedElsewhere = true;
                                        break;
                                    }
                                }
                            }
                            if (usedElsewhere) {
                                usedVars.push(currentEventData.eventVars[v]);
                            }
                        }
                    }

                    fs.writeFile(AppConstants.base_path + "/" + userId + "/eventFile.json", JSON.stringify({
                        eventVars: usedVars,
                        events: remainingEvents
                    }), 'utf-8', function (err) {
                        if (err) {
                            cb(err);
                        } else {
                            this.generateEventFile(userId, cb);
                        }
                    }.bind(this));

                }
            }.bind(this));
        },


        generateEventFile: function (userId, cb) {
            fs.readFile(AppConstants.base_path + "/" + userId + "/eventFile.json", 'utf-8', function (err, currentEventData) {
                if (err) {
                    cb(err);
                } else {
                    var json = JSON.parse(currentEventData);
                    converter.json2csv(json.events, function (err, csv) {
                        if (err) {
                            console.log(err);
                        } else {
                            fs.writeFile(AppConstants.base_path + "/" + userId + "/eventFile", csv, 'utf-8', cb);
                        }
                    }, {
                        checkSchemaDifferences: false,
                        emptyFieldValue: '0'
                    });
                }
            });
        }
    };
})();

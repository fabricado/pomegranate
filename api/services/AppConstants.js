module.exports = {
    base_path: sails.config.appPath + "/.tmp/uploads",
    getBinPath: function () {
        var bin_path = sails.config.appPath+"/bin/event_study_api_linux";
        if (process.platform == "darwin") {
            bin_path = sails.config.appPath+"/bin/event_study_api_mac";
        } else if (process.platform == "win32") {
            bin_path = sails.config.appPath+"/bin/event_study_api_windows.exe";
        }
        return bin_path;
    }
};

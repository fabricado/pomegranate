var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    bcrypt = require('bcrypt');
//helper functions
function findById(id, fn) {
    User.findOne(id, function (err, user) {
        if (err) {
            return fn(null, null);
        } else {
            return fn(null, user);
        }
    });
}

function findByEmail(e, fn) {
    User.findOne({
            email: e
        },
        function (err, user) {
            // Error handling
            if (err) {
                return fn(null, null);
                // The User was found successfully!
            } else {
                return fn(null, user);
            }
        });
}

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    findById(id, function (err, user) {
        done(err, user);
    });
});

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    }, function (email, password, done) {
        // asynchronous verification, for effect...
        process.nextTick(function () {
            findByEmail(email, function (err, user) {
                if (err)
                    return done(null, err);
                if (!user) {
                    return done(null, false, {
                        message: 'Unknown email ' + email
                    });
                }
                bcrypt.compare(password, user.password, function (err, res) {
                    if (!res)
                        return done(null, false, {
                            message: 'Invalid Password'
                        });
                    var returnUser = {
                        username: user.username,
                        email: user.email,
                        createdAt: user.createdAt,
                        id: user.id
                    };
                    console.log("Login SUCCESS");
                    return done(null, returnUser, {
                        message: 'Logged In Successfully'
                    });
                });
            })
        });
    }
));

var filesys = require('fs');

module.exports = (function () {

    return {
        // finds directories in /assets/releases and returns a json with
        //      the release number and the release notes
        findReleases: function () {

            var results = [];
            var releasesPath = __dirname + "/../../assets/releases/";
            filesys.readdirSync(releasesPath).forEach(function (file) {
                var fpath = releasesPath + file;
                var stat = filesys.statSync(fpath);

                if (stat && stat.isDirectory()) {
                    var curr = {};

                    var n = fpath.search(/eventstudy/i);
                    if (n > -1) {
                        var ver = fpath.match(/[0-9]+(?:\.[0-9]+)*$/i);

                        //var fs = require('fs');
                        var notes = filesys.readFileSync(fpath + '/RELEASE-NOTES.txt', 'utf8');
                        notes = notes.trim();
                        curr = {
                            api: "Event Study",
                            version: ver[0],
                            rNotes: notes,
                            winUrl: "/releases/eventStudy-v" + ver + "/eventStudy-v" + ver + "-win.zip",
                            osxUrl: "/releases/eventStudy-v" + ver + "/eventStudy-v" + ver + "-osx.zip",
                            linuxUrl: "/releases/eventStudy-v" + ver + "/eventStudy-v" + ver + "-linux.zip"
                        };

                        results.push(curr);
                    }
                }
            });
            return results;
        }
    }
})();

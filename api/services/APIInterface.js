/**
 * Interface between Event Study API (c++) and Sails-Node (JS)
 * Created by Aman Singh on 14/04/2016.
 * Collaborators: Aman Singh, Alan Allaf
 */

var child_process = require('child_process');
var crypto = require('crypto');
var fs = require('fs');

module.exports = (function () {
    var defaultEventFilePath = (sails.config.appPath + "/assets/defaultInputFiles/eventFile.csv");
    var defaultStockFilePath = (sails.config.appPath + "/assets/defaultInputFiles/farmingCompanies.csv");

    return {

        /**
         * Calls event_study_api with the necessary parameters
         * Will return JSON object containing API output
         */
        es_request: function (userID, mode, relFlag, singleEvent, RIC, buyInDate, randomDate, lb, ub, vars) {

            var bin_path = AppConstants.getBinPath();

            //console.log("INTERFACING: " + lb + " " + ub + ", VARS: " + vars);

            //check that both input files exist
            if (!fs.existsSync(sails.config.appPath+'/.tmp/uploads/' + userID + '/stockFile')) {
                //stock file does not exist
                return {
                    error: "Stock file not uplaoded."
                }
            }

            if (!fs.existsSync(sails.config.appPath+'/.tmp/uploads/' + userID + '/eventFile')) {
                //event file does not exist
                return {
                    error: "Event file not uplaoded."
                }
            }

            //execute program (from .tmp/uploads/<d>/ if possible)
            //console.log('old wd: ' + process.cwd());
            //console.log('trying: ' + sails.config.appPath+'/.tmp/uploads/' + token + '/');
            var oldDir = process.cwd();
            process.chdir(AppConstants.base_path + '/' + userID + "/");
            //console.log('new wd: ' + process.cwd());

            //determine api parameters depending on the mode of operation
            var params = "";
            if (relFlag) {
                console.log("adding relflag");
                params = " --rel";
            }

            var stockFile = 'stockFile';
            var eventFile = 'eventFile';

            var dateChangeParams = "";
            if (buyInDate != null) {
                dateChangeParams += "--buyin " + RIC + " " + buyInDate + " ";
            }
            if (randomDate != null) {
                dateChangeParams += "--random " + randomDate + " ";
                eventFile = ""; //no event file given with random flag
            }

            if (singleEvent) eventFile = 'fakeEventFile';
            switch(mode) {
                case 'analyse':
                    params += ' --analyse ' + stockFile + ' ' + eventFile;
                    break;
                case "eventStudyPercent":
                    params += ' --json --percent ' + dateChangeParams + stockFile + ' ' + eventFile + ' ' + lb + ' ' + ub + ' ' + vars.join(" ");
                    break;
                case "listEvents":
                    params += ' --json ' + stockFile + ' ' + eventFile;
                    break;
                case "getRIC":
                    params += ' --getvalues ' + RIC + ' ' +  stockFile + ' ' + eventFile;
                    break;
                case "all":
                    params = ' --all stockFile eventFile ' + lb + ' ' + ub + ' ' + vars.join(" ");
                    break;
                default: //"eventStudy"
                    params += ' --json ' + dateChangeParams+ stockFile + ' ' + eventFile + ' '  + lb + ' ' + ub + ' ' + vars.join(" ");
            }

            //run api, capture stdout
            console.log(bin_path + params);
            var output = child_process.execSync(bin_path + params).toString();
            //return to normal working dir
            process.chdir(oldDir);

            return {
                request: {
                    lowerWindow: lb,
                    upperWindow: ub,
                    variables: vars
                },

                results: JSON.parse(output)
            };
        },

        get_server_stock_lifetime: function (RIC) {
            var lifetimeFile = sails.config.appPath + '/assets/stockData/' + RIC + '/' + RIC + '.csv';
            var dummyEventFile = sails.config.appPath + '/assets/defaultInputFiles/eventFile.csv';

            var command = AppConstants.getBinPath() + ' --getvalues ' + RIC + ' ' + lifetimeFile + ' ' + dummyEventFile;
            //console.log(command);

            var output = child_process.execSync(command).toString();
            //console.log(output);

            return {results: JSON.parse(output)};
        }
    }
})();

/**
 * Interface between Stored Session Data and Sails-Node (JS)
 * Created by Aman Singh on 14/04/2016.
 * Collaborators: Aman Singh, Alan Allaf
 */

var child_process = require('child_process');
var crypto = require('crypto');
var fs = require('fs');

module.exports = (function () {

    return {

        /**
         * Returns a new token
         */
        session_generate_new: function (id) {
            //create a new id
            var date = new Date();
            var timeString = date.getTime().toString() + 'garbleSaltStringJustTryToReverseThis';
            var hash = crypto.createHash('md5');
            var token;
            do {
                hash.update(timeString);
                var digest = hash.digest('hex');

                //only use last five characters of the string
                token = digest.substr(digest.length - 5);
            } while (fs.existsSync(sails.config.appPath + "/.tmp/uploads/" + token + '/'));

            if (!fs.existsSync(sails.config.appPath + "/.tmp/uploads")) {
                fs.mkdirSync(sails.config.appPath + "/.tmp/uploads/");
            }

            if (!fs.existsSync(sails.config.appPath + "/.tmp/sessions")) {
                console.log("Making sessions folder");
                fs.mkdir(sails.config.appPath + "/.tmp/sessions/", function (err) {if (err) console.log(err);});
            }

            //console.log("mkdir...");
            fs.mkdir(sails.config.appPath + "/.tmp/uploads/" + token + '/', function (err) {if (err) console.log(err);});
            //console.log("mkdir done?");
            SessionInterface.session_set_active(id, token);
            //console.log("Generated new token: ", token);
            return token;
        },

        /**
         * Returns the token of the user's active session, if there is one
         */
        session_get_active: function(id) {
            var dir = sails.config.appPath + "/.tmp/sessions/" + id;
            if (!fs.existsSync(dir)) {
                return undefined;
            }
            return fs.readFileSync(dir, "utf-8");
        },

        /**
         * Sets the user's active session
         */
        session_set_active: function(id, session) {
            var dir = sails.config.appPath + "/.tmp/sessions/" + id;
            fs.writeFile(dir, session, function (err) {if (err) console.log(err);});
        },

        /**
         * Returns contents of the state.json file
         */
        session_get_state: function (token) {
            //console.log("trying to get state");
            var stateFileDir = sails.config.appPath + "/.tmp/uploads/" + token + "/state.json";
            //check that the state file exists
            if (!fs.existsSync(stateFileDir)) {
                //stock file does not exist
                return {
                    error: "State file does not exist."
                }
            }
            var stateFile = fs.readFileSync(stateFileDir, "utf-8");
            var state = JSON.parse(stateFile);
            //console.log(state);
            //console.log("Returning from session_get_state");
            return state;
        },

        /**
         * Returns contents of the state.json file
         */
        session_set_state: function (data) {
            var token = data.token;
            var stateFileDir = sails.config.appPath + "/.tmp/uploads/" + token + "/state.json";

            var state = JSON.stringify(data.state);
            fs.writeFile(stateFileDir, state, function (err) {
                    if (err) throw err;
                    //console.log('State Saved!');
                });
        },

    }
})();

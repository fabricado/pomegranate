/**
 * Module that has various helpful utility functions
 */
module.exports = (function () {
    return {
        /**
         * Implements a comparator for objects returned by FileHelper.findReleases()
         * The comparator will sort by version number
         * @param a An object containing release version info
         * @param b An object containing release version info
         * @returns {number}
         */
        versionComp: function (a, b) {
            var partsA = a.version.split(".").map(function (val) {
                return parseInt(val);
            });
            var partsB = b.version.split(".").map(function (val) {
                return parseInt(val);
            });
            var k = Math.min(partsA.length, partsB.length);
            var i = 0;
            while (i < k) {
                if (partsA[i] === partsB[i]) {
                    i++;
                } else if (partsA[i] < partsB[i]) {
                    return 1;
                } else {
                    return -1;
                }
            }
            if (partsA.length !== partsB.length) {
                if (partsA.length < partsB.length) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                return 0;
            }
        }
    }
})();
